using System;
using LaborHub.Domain.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace LaborHub.DataAccess.Identity;

public class LaborHubRoleStore : RoleStore<Role, LaborHubDbContext, Guid, UserRole, RoleClaim>
{
    public LaborHubRoleStore(LaborHubDbContext context, IdentityErrorDescriber describer)
        : base(context, describer) { }
}
