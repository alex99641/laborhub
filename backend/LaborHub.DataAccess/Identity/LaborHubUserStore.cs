using System;
using LaborHub.Domain.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace LaborHub.DataAccess.Identity
{
    public class LaborHubUserStore : UserStore<User, Role, LaborHubDbContext, Guid, UserClaim, UserRole, UserLogin, UserToken, RoleClaim>
    {
        public LaborHubUserStore(LaborHubDbContext context, IdentityErrorDescriber describer) : base(context, describer)
        {
        }
    }
}
