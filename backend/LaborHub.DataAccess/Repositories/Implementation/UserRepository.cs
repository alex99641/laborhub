using AutoMapper;
using DigitalSkynet.DotnetCore.DataAccess.Repository;
using DigitalSkynet.DotnetCore.DataStructures.Interfaces;
using AutoMapper.QueryableExtensions;
using DigitalSkynet.DotnetCore.DataAccess.Enums;
using DigitalSkynet.DotnetCore.DataStructures.Models.Paging;
using Microsoft.EntityFrameworkCore;
using LaborHub.DataAccess.Repositories.Interfaces;
using LaborHub.Domain.Entities;
using LaborHub.DataAccess;
using LaborHub.ViewModels.User;
using LaborHub.Foundation.Enums;
using LaborHub.Foundation.Constants;

namespace LaborHub.DataAccess.Repositories.Implementation
{
    public class UserRepository : GenericDeletableRepository<LaborHubDbContext, User, Guid>, IUserRepository
    {
        public UserRepository(LaborHubDbContext dbContext, IMapper mapper) : base(dbContext, mapper)
        {
        }

        public async Task<Paged<UserView>> GetUsers(string userRole, UserSearchStatus status, int pageNumber, int pageSize, string sortProperty, LaborHubSortDirections sortDirection, string filterString, CancellationToken ct)
        {
            var baseQuery = GetBaseQuery(DigitalSkynet.DotnetCore.DataAccess.Enums.FetchModes.NoTracking);
            baseQuery = baseQuery.Where(x => x.Id != ConstantIds.SystemUserId);
            if (!string.IsNullOrEmpty(userRole))
            {
                baseQuery = baseQuery.Where(x => x.UserRoles.Any(ur => ur.Role.Name == userRole));
            }

            if (!string.IsNullOrEmpty(filterString))
            {
                var loweredFilter = $"%{filterString.ToLower()}%";
                var phonFilter = loweredFilter.Replace("+", "").Replace("-", "").Replace("(", "").Replace(")", "").Replace(" ", "");
                baseQuery = baseQuery.Where(x => EF.Functions.Like((x.FirstName + " " + x.LastName).ToLower(), loweredFilter) ||
                    EF.Functions.Like(x.UserName!.ToLower(), loweredFilter) ||
                    EF.Functions.Like(x.Email!.ToLower(), loweredFilter) ||
                    EF.Functions.Like(x.PhoneNumber!.Replace(" ", "").Replace("+", "").Replace("-", "").Replace("(", "").Replace(")", "").ToLower(), phonFilter));
            }

            if (status != UserSearchStatus.Any)
            {
                bool mustHaveLockoutDate = status == UserSearchStatus.DisabledOnly;
                baseQuery = baseQuery.Where(x => x.LockoutEnd.HasValue == mustHaveLockoutDate);
            }

            var result = await baseQuery.Skip((pageNumber - 1) * pageSize)
                .Take(pageSize)
                .ProjectTo<UserView>(_mapper.ConfigurationProvider)
                .ToListAsync(ct);

            var totalCount = await baseQuery.CountAsync(ct);

            return new Paged<UserView>(result, totalCount, pageNumber, pageSize);
        }

        
    }
}
