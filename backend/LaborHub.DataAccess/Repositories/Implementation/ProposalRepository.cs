using AutoMapper;
using DigitalSkynet.DotnetCore.DataAccess.Enums;
using DigitalSkynet.DotnetCore.DataAccess.Repository;
using LaborHub.DataAccess.Repositories.Interfaces;
using LaborHub.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace LaborHub.DataAccess.Repositories.Implementation
{
    public class ProposalRepository
        : GenericDeletableRepository<LaborHubDbContext, Proposal, Guid>,
            IProposalRepository
    {
        public ProposalRepository(LaborHubDbContext dbContext, IMapper mapper)
            : base(dbContext, mapper) { }

        public Task<Proposal> GetByProposalIdAsync(Guid proposalId, CancellationToken ct)
        {
            return GetBaseQuery(FetchModes.NoTracking)
                .Include(x => x.Author)
                .Include(x => x.Advertisement)
                .Include(x => x.Advertisement.Author)
                .Where(x => x.Id == proposalId)
                .FirstAsync(ct);
        }
    }
}
