using AutoMapper;
using DigitalSkynet.DotnetCore.DataAccess.Repository;
using LaborHub.DataAccess.Repositories.Interfaces;
using LaborHub.Domain.Entities;

namespace LaborHub.DataAccess.Repositories.Implementation
{
    public class RefreshTokenRepository : GenericRepository<LaborHubDbContext, RefreshToken, int>, IRefreshTokenRepository
    {
        public RefreshTokenRepository(LaborHubDbContext dbContext, IMapper mapper) : base(dbContext, mapper)
        {
        }
    }
}
