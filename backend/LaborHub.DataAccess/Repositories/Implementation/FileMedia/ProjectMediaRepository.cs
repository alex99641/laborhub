using AutoMapper;
using DigitalSkynet.DotnetCore.DataAccess.Repository;
using LaborHub.DataAccess.Repositories.Interfaces.FileMedia;
using LaborHub.Domain.Entities;

namespace LaborHub.DataAccess.Repositories.Implementation.FileMedia
{
    public class ProjectMediaRepository
        : GenericDeletableRepository<LaborHubDbContext, ProjectMedia, Guid>,
            IProjectMediaRepository
    {
        public ProjectMediaRepository(LaborHubDbContext dbContext, IMapper mapper)
            : base(dbContext, mapper) { }        
    }
}
