using AutoMapper;
using DigitalSkynet.DotnetCore.DataAccess.Repository;
using LaborHub.DataAccess.Repositories.Interfaces.FileMedia;
using LaborHub.Domain.Entities;

namespace LaborHub.DataAccess.Repositories.Implementation.FileMedia
{
    public class ProposalFileRepository
        : GenericDeletableRepository<LaborHubDbContext, ProposalFile, Guid>,
            IProposalFileRepository
    {
        public ProposalFileRepository(LaborHubDbContext dbContext, IMapper mapper)
            : base(dbContext, mapper) { }        
    }
}
