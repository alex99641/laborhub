using AutoMapper;
using DigitalSkynet.DotnetCore.DataAccess.Repository;
using LaborHub.DataAccess.Repositories.Interfaces.FileMedia;
using LaborHub.Domain.Entities;

namespace LaborHub.DataAccess.Repositories.Implementation.FileMedia
{
    public class AdvertisementFileRepository
        : GenericDeletableRepository<LaborHubDbContext, AdvertisementFile, Guid>,
            IAdvertisementFileRepository
    {
        public AdvertisementFileRepository(LaborHubDbContext dbContext, IMapper mapper)
            : base(dbContext, mapper) { }

        
    }
}
