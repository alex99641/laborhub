using AutoMapper;
using DigitalSkynet.DotnetCore.DataAccess.Repository;
using LaborHub.DataAccess.Repositories.Interfaces.FileMedia;
using LaborHub.Domain.Entities;

namespace LaborHub.DataAccess.Repositories.Implementation.FileMedia
{
    public class ProposalMediaRepository
        : GenericDeletableRepository<LaborHubDbContext, ProposalMedia, Guid>,
            IProposalMediaRepository
    {
        public ProposalMediaRepository(LaborHubDbContext dbContext, IMapper mapper)
            : base(dbContext, mapper) { }        
    }
}
