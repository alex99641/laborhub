using AutoMapper;
using DigitalSkynet.DotnetCore.DataAccess.Repository;
using LaborHub.DataAccess.Repositories.Interfaces.FileMedia;
using LaborHub.Domain.Entities;

namespace LaborHub.DataAccess.Repositories.Implementation.FileMedia
{
    public class ProjectFileRepository
        : GenericDeletableRepository<LaborHubDbContext, ProjectFile, Guid>,
            IProjectFileRepository
    {
        public ProjectFileRepository(LaborHubDbContext dbContext, IMapper mapper)
            : base(dbContext, mapper) { }        
    }
}
