using AutoMapper;
using DigitalSkynet.DotnetCore.DataAccess.Repository;
using LaborHub.DataAccess.Repositories.Interfaces.FileMedia;
using LaborHub.Domain.Entities;

namespace LaborHub.DataAccess.Repositories.Implementation.FileMedia
{
    public class AdvertisementMediaRepository
        : GenericDeletableRepository<LaborHubDbContext, AdvertisementMedia, Guid>,
            IAdvertisementMediaRepository
    {
        public AdvertisementMediaRepository(LaborHubDbContext dbContext, IMapper mapper)
            : base(dbContext, mapper) { }

        
    }
}
