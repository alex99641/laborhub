using AutoMapper;
using DigitalSkynet.DotnetCore.DataAccess.Enums;
using DigitalSkynet.DotnetCore.DataAccess.Repository;
using LaborHub.DataAccess.Repositories.Interfaces;
using LaborHub.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace LaborHub.DataAccess.Repositories.Implementation
{
    public class ProjectRepository
        : GenericDeletableRepository<LaborHubDbContext, Project, Guid>,
            IProjectRepository
    {
        public ProjectRepository(LaborHubDbContext dbContext, IMapper mapper)
            : base(dbContext, mapper) { }

        public Task<Project> GetByProjectIdAsync(Guid projectId, CancellationToken ct)
        {
            return GetBaseQuery(FetchModes.NoTracking)
                .Include(x => x.Client)
                .Include(x => x.Master)
                .Include(x => x.Proposal)
                .Include(x => x.Advertisement)
                .Include(x => x.Proposal.Author)
                .Include(x => x.Proposal.Advertisement)
                .Include(x => x.Proposal.Advertisement.Author)
                .Include(x => x.Advertisement.Author)
                .Where(x => x.Id == projectId)
                .FirstAsync(ct);
        }
    }
}
