using AutoMapper;
using DigitalSkynet.DotnetCore.DataAccess.Enums;
using DigitalSkynet.DotnetCore.DataAccess.Repository;
using LaborHub.DataAccess.Repositories.Interfaces;
using LaborHub.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace LaborHub.DataAccess.Repositories.Implementation
{
    public class AdvertisementRepository
        : GenericDeletableRepository<LaborHubDbContext, Advertisement, Guid>,
            IAdvertisementRepository
    {
        public AdvertisementRepository(LaborHubDbContext dbContext, IMapper mapper)
            : base(dbContext, mapper) { }

        public Task<Advertisement> GetByAdvertisementIdAsync(Guid advertId, CancellationToken ct)
        {
            return GetBaseQuery(FetchModes.NoTracking)
                .Include(x => x.Author)
                .Where(x => x.Id == advertId)
                .FirstAsync(ct);
        }
    }
}
