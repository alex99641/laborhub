using DigitalSkynet.DotnetCore.DataAccess.Repository;
using LaborHub.Domain.Entities;

namespace LaborHub.DataAccess.Repositories.Interfaces
{
    public interface IRefreshTokenRepository : IGenericRepository<RefreshToken, int>
    {
    }
}
