using DigitalSkynet.DotnetCore.DataAccess.Repository;
using LaborHub.Domain.Entities;

namespace LaborHub.DataAccess.Repositories.Interfaces
{
    public interface IAdvertisementRepository : IGenericDeletableRepository<Advertisement, Guid>
    {
        Task<Advertisement> GetByAdvertisementIdAsync(Guid advertId, CancellationToken ct);
    }
}
