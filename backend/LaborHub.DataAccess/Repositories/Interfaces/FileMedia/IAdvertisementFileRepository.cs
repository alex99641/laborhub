using DigitalSkynet.DotnetCore.DataAccess.Repository;
using LaborHub.Domain.Entities;

namespace LaborHub.DataAccess.Repositories.Interfaces.FileMedia
{
    public interface IAdvertisementFileRepository
        : IGenericDeletableRepository<AdvertisementFile, Guid> { }
}
