using DigitalSkynet.DotnetCore.DataAccess.Repository;
using LaborHub.Domain.Entities;

namespace LaborHub.DataAccess.Repositories.Interfaces
{
    public interface IProposalRepository : IGenericDeletableRepository<Proposal, Guid>
    {
        Task<Proposal> GetByProposalIdAsync(Guid advertId, CancellationToken ct);
    }
}
