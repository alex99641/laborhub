using DigitalSkynet.DotnetCore.DataAccess.Repository;
using LaborHub.Domain.Entities;

namespace LaborHub.DataAccess.Repositories.Interfaces
{
    public interface IProjectRepository : IGenericDeletableRepository<Project, Guid>
    {
        Task<Project> GetByProjectIdAsync(Guid projectId, CancellationToken ct);
    }
}
