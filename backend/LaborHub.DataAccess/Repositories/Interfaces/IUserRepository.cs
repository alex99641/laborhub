using DigitalSkynet.DotnetCore.DataAccess.Repository;
using DigitalSkynet.DotnetCore.DataStructures.Models.Paging;
using LaborHub.Domain.Entities;
using LaborHub.Foundation.Enums;
using LaborHub.ViewModels.User;

namespace LaborHub.DataAccess.Repositories.Interfaces
{
    public interface IUserRepository : IGenericDeletableRepository<User, Guid>
    {
        Task<Paged<UserView>> GetUsers(string userRole, UserSearchStatus status, int pageNumber, int pageSize, string sortProperty, LaborHubSortDirections sortDirection, string filterString, CancellationToken ct);
    }
}
