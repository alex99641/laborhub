﻿using DigitalSkynet.DotnetCore.DataAccess.StaticData;
using DigitalSkynet.DotnetCore.DataStructures.Interfaces;
using LaborHub.Domain.Entities;
using LaborHub.DataAccess.StaticData;
using Microsoft.AspNetCore.DataProtection.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace LaborHub.DataAccess;

public class LaborHubDbContext
    : IdentityDbContext<User, Role, Guid, UserClaim, UserRole, UserLogin, RoleClaim, UserToken>,
        IDataProtectionKeyContext
{
    public DbSet<DataProtectionKey> DataProtectionKeys { get; set; }
    public DbSet<RefreshToken> RefreshTokens { get; set; }
    public DbSet<Advertisement> Advertisements { get; set; }
    public DbSet<AdvertisementMedia> AdvertisementMedias { get; set; }
    public DbSet<AdvertisementFile> AdvertisementFiles { get; set; }
    public DbSet<Proposal> Proposals { get; set; }
    public DbSet<ProposalFile> ProposalFiles { get; set; }
    public DbSet<ProposalMedia> ProposalMedias { get; set; }
    public DbSet<Project> Projects { get; set; }
    public DbSet<ProjectFile> ProjectFiles { get; set; }
    public DbSet<ProjectMedia> ProjectMedias { get; set; }

    public LaborHubDbContext(DbContextOptions<LaborHubDbContext> options)
        : base(options) { }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder); // adding Identity entities
        builder.HasPostgresExtension("postgis");

        var deletableEntities = typeof(User).Assembly
            .GetTypes()
            .Where(x => x.IsAssignableFrom(typeof(ISoftDeletable)) && !x.IsAbstract && x.IsClass);

        foreach (var entity in deletableEntities)
        {
            builder.Entity(entity).HasIndex("IsDeleted");
        }

        builder.Entity<User>().HasIndex(x => x.FirstName);
        builder.Entity<User>().HasIndex(x => x.LastName);

        builder.Entity<UserRole>().HasKey(ur => new { ur.UserId, ur.RoleId });

        builder
            .Entity<UserRole>()
            .HasOne(ur => ur.User)
            .WithMany(u => u.UserRoles)
            .HasForeignKey(ur => ur.UserId);

        builder
            .Entity<UserRole>()
            .HasOne(ur => ur.Role)
            .WithMany()
            .HasForeignKey(ur => ur.RoleId);

        builder.Entity<Role>().HasDataProvider<RoleData, Role>();
        builder.Entity<User>().HasDataProvider<UserData, User>();
        builder.Entity<UserRole>().HasDataProvider<UserRoleData, UserRole>();

        builder.Entity<RefreshToken>();
        builder.Entity<Advertisement>()
            .Property(l => l.Location)
            .HasColumnType("geography (point)");
    }

        #region Modification Tracking

    public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
    {
        OnBeforeSaving();
        return base.SaveChangesAsync(cancellationToken);
    }

    public override int SaveChanges(bool acceptAllChangesOnSuccess)
    {
        OnBeforeSaving();
        return base.SaveChanges(acceptAllChangesOnSuccess);
    }

    /// <summary> Track entites with modification dates </summary>
    private void OnBeforeSaving()
    {
        foreach (var entry in ChangeTracker.Entries())
        {
            if (entry.Entity is ITimestamped baseEntity)
            {
                var now = DateTime.UtcNow;

                switch (entry.State)
                {
                    case EntityState.Modified:
                        baseEntity.UpdatedDate = now;
                        break;

                    case EntityState.Added:
                        baseEntity.CreatedDate = now;
                        baseEntity.UpdatedDate = now;
                        break;
                }
            }
        }
    }

        #endregion
}
