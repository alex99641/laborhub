using System;
using System.Collections.Generic;
using DigitalSkynet.DotnetCore.DataAccess.StaticData;
using LaborHub.Domain.Entities;
using LaborHub.Foundation.Constants;

namespace LaborHub.DataAccess.StaticData;

public class UserData : StaticDataProvider<UserData, User>
{
    public static readonly Guid AdminUserId = new Guid("132f36ee-4f53-4043-a642-233fba6ee8c5");
    protected override IEnumerable<User> GenerateEntities()
    {
        yield return new User
        {
            Id = AdminUserId,
            UserName = "Admin",
            NormalizedUserName = "ADMIN",
            Email = "admin@example.com",
            NormalizedEmail = "ADMIN@EXAMPLE.COM",
            PasswordHash = "AQAAAAEAACcQAAAAENbWXnQm3ClTn60QQ6Rw3QwfjhImexUpVheChbbMxpbCrktychQLWQXwIt+QPXIJCw==",
            SecurityStamp = "OMHGPURXCVSFCHLL4IFMOIQDNZ7RIOXH",
            EmailConfirmed = true
        };
        yield return new User
        {
            Id = ConstantIds.SystemUserId,
            UserName = ConstantIds.SystemUserName,
            FirstName = ConstantIds.SystemUserFirstName,
            LastName = ConstantIds.SystemUserLastNameName,
            NormalizedUserName = "FLASH MEDIA",
            Email = "support@flashmedia.ru",
            NormalizedEmail = "SUPPORT@FLASHMEDIA.RU"
        };
    }
}
