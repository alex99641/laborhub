namespace LaborHub.DataAccess.StaticData;

public class JwtConstans
{
    public const string ISSUER = "MyAuthServer"; // token publisher
    public const string AUDIENCE = "MyAuthClient"; // token consumer
    const string KEY = "mysupersecret_secretkey!123";   // encryption
    public const int LIFETIME = 1; // token lifetime - 1 minute

}
