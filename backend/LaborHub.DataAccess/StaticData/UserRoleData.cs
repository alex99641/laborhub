using System;
using System.Collections.Generic;
using DigitalSkynet.DotnetCore.DataAccess.StaticData;
using LaborHub.Domain.Entities;

namespace LaborHub.DataAccess.StaticData
{
    public class UserRoleData : StaticDataProvider<UserRoleData, UserRole>
    {
        public static readonly Guid ADMIN_UID = new Guid("132f36ee-4f53-4043-a642-233fba6ee8c5");
        public static readonly Guid ROLE_UID = new Guid("132f36ee-4f53-4043-a642-233fba6ee8c5");
        protected override IEnumerable<UserRole> GenerateEntities()
        {
            yield return new UserRole
            {
                UserId = ADMIN_UID,
                RoleId = ROLE_UID
            };
        }
    }
}
