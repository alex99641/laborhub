using DigitalSkynet.DotnetCore.DataAccess.StaticData;
using LaborHub.Domain.Entities;

namespace LaborHub.DataAccess.StaticData;

public class RoleData : StaticDataProvider<RoleData, Role>
{
    public static readonly Guid ADMIN_UID = new Guid("132f36ee-4f53-4043-a642-233fba6ee8c5");
    public static readonly Guid CLIENT_UID = new Guid("232f36ee-4f53-4043-a647-233fba6ee8c6");
    public static readonly Guid MASTER_UID = new Guid("332f36ee-4f53-4043-a645-233fba6ee8c7");
    public static readonly Guid WORKER_UID = new Guid("432f36ee-4f53-4043-a647-233fba6ee8c8");
    protected override IEnumerable<Role> GenerateEntities()
    {
        yield return new Role
        {
            Id = ADMIN_UID,
            Name = "admin",
            NormalizedName = "ADMIN"
        };
        yield return new Role
        {
            Id = CLIENT_UID,
            Name = "client",
            NormalizedName = "CLIENT"
        };
        yield return new Role
        {
            Id = MASTER_UID,
            Name = "master",
            NormalizedName = "MASTER"
        };
        yield return new Role
        {
            Id = WORKER_UID,
            Name = "worker",
            NormalizedName = "WORKER"
        };
    }

}
