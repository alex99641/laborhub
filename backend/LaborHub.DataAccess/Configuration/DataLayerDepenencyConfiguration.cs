using DigitalSkynet.DotnetCore.DataAccess.UnitOfWork;
using LaborHub.DataAccess;
using LaborHub.DataAccess.Repositories.Implementation;
using LaborHub.DataAccess.Repositories.Implementation.FileMedia;
using LaborHub.DataAccess.Repositories.Interfaces;
using LaborHub.DataAccess.Repositories.Interfaces.FileMedia;
using Microsoft.Extensions.DependencyInjection;

namespace LaborHub.DataAccess.Configuration
{
    public static class DataLayerDependencyConfigurator
    {
        public static IServiceCollection AddRepositories(this IServiceCollection services)
        {
            services.AddScoped<IRefreshTokenRepository, RefreshTokenRepository>();
            services.AddScoped<IAdvertisementRepository, AdvertisementRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IAdvertisementFileRepository, AdvertisementFileRepository>();
            services.AddScoped<IAdvertisementMediaRepository, AdvertisementMediaRepository>();
            services.AddScoped<IProposalRepository, ProposalRepository>();
            services.AddScoped<IProposalFileRepository, ProposalFileRepository>();
            services.AddScoped<IProposalMediaRepository, ProposalMediaRepository>();
            services.AddScoped<IProjectRepository, ProjectRepository>();
            services.AddScoped<IProjectFileRepository, ProjectFileRepository>();
            services.AddScoped<IProjectMediaRepository, ProjectMediaRepository>();
            services.AddScoped<IUnitOfWork, UnitOfWork<LaborHubDbContext>>();

            return services;
        }
    }
}
