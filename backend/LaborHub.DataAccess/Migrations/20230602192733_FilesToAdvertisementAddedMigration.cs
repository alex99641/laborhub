﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace LaborHub.DataAccess.Migrations
{
    public partial class FilesToAdvertisementAddedMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AdvertisementFiles",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false),
                    AdvertisementId = table.Column<Guid>(type: "uuid", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    UpdatedDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    IsDeleted = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdvertisementFiles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AdvertisementFiles_Advertisements_AdvertisementId",
                        column: x => x.AdvertisementId,
                        principalTable: "Advertisements",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AdvertisementMedias",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false),
                    AdvertisementId = table.Column<Guid>(type: "uuid", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    UpdatedDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    IsDeleted = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdvertisementMedias", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AdvertisementMedias_Advertisements_AdvertisementId",
                        column: x => x.AdvertisementId,
                        principalTable: "Advertisements",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("132456ee-4f53-4943-a642-233fea6ee8e1"),
                column: "ConcurrencyStamp",
                value: "7647de95-1e10-4509-b9e7-a5ea7e5d5cff");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("132f36ee-4f53-4043-a642-233fba6ee8c5"),
                column: "ConcurrencyStamp",
                value: "9d381dc9-6fed-4593-86a7-fc7f153725ca");

            migrationBuilder.CreateIndex(
                name: "IX_AdvertisementFiles_AdvertisementId",
                table: "AdvertisementFiles",
                column: "AdvertisementId");

            migrationBuilder.CreateIndex(
                name: "IX_AdvertisementMedias_AdvertisementId",
                table: "AdvertisementMedias",
                column: "AdvertisementId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AdvertisementFiles");

            migrationBuilder.DropTable(
                name: "AdvertisementMedias");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("132456ee-4f53-4943-a642-233fea6ee8e1"),
                column: "ConcurrencyStamp",
                value: "3210f1f1-d78f-4557-b433-daf73c0440a8");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("132f36ee-4f53-4043-a642-233fba6ee8c5"),
                column: "ConcurrencyStamp",
                value: "d44d809d-f090-4411-b642-63e5b990c8bf");
        }
    }
}
