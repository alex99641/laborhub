﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using NetTopologySuite.Geometries;

#nullable disable

namespace LaborHub.DataAccess.Migrations
{
    public partial class AdvertisementAddedMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "AdvertisementId",
                table: "Advertisements",
                newName: "AuthorId");

            migrationBuilder.AlterDatabase()
                .Annotation("Npgsql:PostgresExtension:postgis", ",,");

            migrationBuilder.AddColumn<decimal>(
                name: "Latitude",
                table: "Advertisements",
                type: "numeric",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<Point>(
                name: "Location",
                table: "Advertisements",
                type: "geography (point)",
                nullable: false);

            migrationBuilder.AddColumn<decimal>(
                name: "Longitude",
                table: "Advertisements",
                type: "numeric",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<string>(
                name: "Title",
                table: "Advertisements",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("132456ee-4f53-4943-a642-233fea6ee8e1"),
                column: "ConcurrencyStamp",
                value: "3210f1f1-d78f-4557-b433-daf73c0440a8");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("132f36ee-4f53-4043-a642-233fba6ee8c5"),
                column: "ConcurrencyStamp",
                value: "d44d809d-f090-4411-b642-63e5b990c8bf");

            migrationBuilder.CreateIndex(
                name: "IX_Advertisements_AuthorId",
                table: "Advertisements",
                column: "AuthorId");

            migrationBuilder.AddForeignKey(
                name: "FK_Advertisements_AspNetUsers_AuthorId",
                table: "Advertisements",
                column: "AuthorId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Advertisements_AspNetUsers_AuthorId",
                table: "Advertisements");

            migrationBuilder.DropIndex(
                name: "IX_Advertisements_AuthorId",
                table: "Advertisements");

            migrationBuilder.DropColumn(
                name: "Latitude",
                table: "Advertisements");

            migrationBuilder.DropColumn(
                name: "Location",
                table: "Advertisements");

            migrationBuilder.DropColumn(
                name: "Longitude",
                table: "Advertisements");

            migrationBuilder.DropColumn(
                name: "Title",
                table: "Advertisements");

            migrationBuilder.RenameColumn(
                name: "AuthorId",
                table: "Advertisements",
                newName: "AdvertisementId");

            migrationBuilder.AlterDatabase()
                .OldAnnotation("Npgsql:PostgresExtension:postgis", ",,");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("132456ee-4f53-4943-a642-233fea6ee8e1"),
                column: "ConcurrencyStamp",
                value: "c252d0cb-13c7-40a7-a429-d89d2b52e726");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("132f36ee-4f53-4043-a642-233fba6ee8c5"),
                column: "ConcurrencyStamp",
                value: "c34079ac-866e-4562-a441-adcbc0004dbf");
        }
    }
}
