using LaborHub.Core.Implementation;
using LaborHub.Core.Interfaces;
using LaborHub.Core.Validation.Implementation;
using LaborHub.Core.Validation.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace LaborHub.Core.Configuration;

public static class CoreLayerDependencyConfigurator
{
    public static void AddServices(this IServiceCollection services) { 
        services.AddScoped<ISessionService, SessionService>();
        services.AddScoped<ITokenService, TokenService>();
        services.AddScoped<IFileService, FileService>();
        services.AddScoped<IAccountService, AccountService>();
        services.AddScoped<IAccountValidator, AccountValidator>();
        services.AddScoped<IAdvertisementService, AdvertisementService>();
        services.AddScoped<IProposalService, ProposalService>();
        services.AddScoped<IProjectService, ProjectService>();
    }
}
