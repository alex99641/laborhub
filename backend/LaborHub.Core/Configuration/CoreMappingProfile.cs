using AutoMapper;
using LaborHub.Domain.Entities;
using LaborHub.Domain.Infrastructure;
using LaborHub.Dtos.Advert;
using LaborHub.Dtos.Infrastructure;
using LaborHub.Dtos.Project;
using LaborHub.Dtos.Proposal;
using LaborHub.Dtos.User;
using LaborHub.ViewModels.Advert;
using LaborHub.ViewModels.Infrastructure;
using LaborHub.ViewModels.Project;
using LaborHub.ViewModels.Proposal;
using LaborHub.ViewModels.User;
using NetTopologySuite.Geometries;

namespace LaborHub.Core.Configuration;

public class CoreMappingProfile : Profile
{
    public CoreMappingProfile()
    {
        CreateMap<IInputModel, BaseGuidEntity>()
            .ForMember(x => x.Id, _ => _.Ignore())
            .ForMember(x => x.CreatedDate, _ => _.Ignore())
            .ForMember(x => x.UpdatedDate, _ => _.Ignore())
            .ForMember(x => x.IsDeleted, _ => _.Ignore());
        CreateMap<BaseGuidEntity, BaseGuidView>();

        CreateMap<RegistrationDto, User>()
            .ForMember(x => x.UserName, _ => _.MapFrom(x => x.UserName))
            .ForMember(x => x.PhoneNumber, _ => _.MapFrom(x => x.PhoneNumber))
            .ForMember(x => x.Email, _ => _.MapFrom(x => x.Email))
            .ForMember(x => x.FirstName, _ => _.MapFrom(x => x.FirstName))
            .ForMember(x => x.LastName, _ => _.MapFrom(x => x.LastName))
            .ForMember(x => x.Birthday, _ => _.MapFrom(x => x.Birthday));

        CreateMap<User, UserView>()
            .ForMember(x => x.UserName, _ => _.MapFrom(x => x.UserName))
            .ForMember(x => x.PhoneNumber, _ => _.MapFrom(x => x.PhoneNumber))
            .ForMember(x => x.Email, _ => _.MapFrom(x => x.Email))
            .ForMember(x => x.Birthday, _ => _.MapFrom(x => x.Birthday))
            .ForMember(x => x.FirstName, _ => _.MapFrom(x => x.FirstName))
            .ForMember(x => x.LastName, _ => _.MapFrom(x => x.LastName))
            .ForMember(x => x.Photo, _ => _.MapFrom(x => x.Photo));

        CreateMap<AdvertisementDto, Advertisement>()
            .IncludeBase<IInputModel, BaseGuidEntity>()
            .ForMember(x => x.AuthorId, _ => _.MapFrom(x => x.AuthorId))
            .ForMember(x => x.Description, _ => _.MapFrom(x => x.Description))
            .ForMember(x => x.Title, _ => _.MapFrom(x => x.Title))
            .ForMember(x => x.Latitude, _ => _.MapFrom(x => x.Latitude))
            .ForMember(x => x.Longitude, _ => _.MapFrom(x => x.Longitude))
            .ForMember(
                x => x.Location,
                _ => _.MapFrom(x => new Point(x.Longitude, x.Latitude) { SRID = 4326 })
            );

        CreateMap<Advertisement, AdvertisementView>()
            .IncludeBase<BaseGuidEntity, BaseGuidView>()
            .ForMember(x => x.Author, _ => _.MapFrom(x => x.Author))
            .ForMember(x => x.Description, _ => _.MapFrom(x => x.Description))
            .ForMember(x => x.Title, _ => _.MapFrom(x => x.Title))
            .ForMember(x => x.Latitude, _ => _.MapFrom(x => x.Latitude))
            .ForMember(x => x.Longitude, _ => _.MapFrom(x => x.Longitude));

        CreateMap<ProposalDto, Proposal>()
            .IncludeBase<IInputModel, BaseGuidEntity>()
            .ForMember(x => x.Title, _ => _.MapFrom(x => x.Title))
            .ForMember(x => x.Description, _ => _.MapFrom(x => x.Description))
            .ForMember(x => x.AuthorId, _ => _.MapFrom(x => x.AuthorId))
            .ForMember(x => x.AdvertisementId, _ => _.MapFrom(x => x.AdvertisementId))
            .ForMember(x => x.ProposalStatus, _ => _.MapFrom(x => x.ProposalStatus));

        CreateMap<Proposal, ProposalView>()
            .IncludeBase<BaseGuidEntity, BaseGuidView>()
            .ForMember(x => x.Title, _ => _.MapFrom(x => x.Title))
            .ForMember(x => x.Description, _ => _.MapFrom(x => x.Description))
            .ForMember(x => x.Author, _ => _.MapFrom(x => x.Author))
            .ForMember(x => x.Advertisement, _ => _.MapFrom(x => x.Advertisement))
            .ForMember(x => x.ProposalStatus, _ => _.MapFrom(x => x.ProposalStatus));

            CreateMap<ProjectDto, Project>()
            .IncludeBase<IInputModel, BaseGuidEntity>()
            .ForMember(x => x.Title, _ => _.MapFrom(x => x.Title))
            .ForMember(x => x.Description, _ => _.MapFrom(x => x.Description))
            .ForMember(x => x.AdvertisementId, _ => _.MapFrom(x => x.AdvertisementId))
            .ForMember(x => x.ProjectStatus, _ => _.MapFrom(x => x.ProjectStatus));

        CreateMap<Project, ProjectView>()
            .IncludeBase<BaseGuidEntity, BaseGuidView>()
            .ForMember(x => x.Title, _ => _.MapFrom(x => x.Title))
            .ForMember(x => x.Description, _ => _.MapFrom(x => x.Description))
            .ForMember(x => x.Master, _ => _.MapFrom(x => x.Master))
            .ForMember(x => x.Client, _ => _.MapFrom(x => x.Client))
            .ForMember(x => x.Advertisement, _ => _.MapFrom(x => x.Advertisement))
            .ForMember(x => x.Proposal, _ => _.MapFrom(x => x.Proposal))
            .ForMember(x => x.ProjectStatus, _ => _.MapFrom(x => x.ProjectStatus));
    }
}
