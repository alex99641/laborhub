using DigitalSkynet.DotnetCore.DataStructures.Exceptions.Api;
using LaborHub.Core.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;

namespace LaborHub.Core.Implementation;

public class FileService : IFileService
{
    private readonly string _photoStoragePath;

    public FileService(IConfiguration configuration)
    {
        _photoStoragePath = configuration.GetValue<string>("PhotoStoragePath")!;
    }

    public async Task<string> CreateAsync(IFormFile photo, CancellationToken ct)
    {
        if (photo == null)
        {
            throw new ApiInvalidOperationException(
                "_stringLocalizer[ErrorMessageCodes.FileIsEmpty].Value"
            );
        }

        var fileName = Guid.NewGuid().ToString() + Path.GetExtension(photo.FileName);
        var filePath = Path.Combine(_photoStoragePath, fileName);
        using (var stream = new FileStream(filePath, FileMode.Create))
        {
            await photo.CopyToAsync(stream);
        }
        return fileName;
    }

    public async Task<List<string>> CreateAsync(List<IFormFile>? files, CancellationToken ct)
    {
        var result = new List<string>();
        if (files == null)
        {
            return result;
        }
        foreach (var file in files)
        {
            result.Add(await CreateAsync(file, ct));
        }
        return result;
    }

    public string GetPhotoUrl(string fileName)
    {
        return Path.Combine(_photoStoragePath, fileName);
    }

    public byte[] GetPhotoByName(string fileName)
    {
        // Получаем фотографию из базы данных или файловой системы по id
        // Photo photo = _dbContext.Photos.FirstOrDefault(p => p.Id == id);

        var path = GetPhotoUrl(fileName);

        // Проверяем, что фотография найдена
        if (File.Exists(path))
        {
            // Читаем содержимое файла с изображением в буфер
            byte[] buffer = File.ReadAllBytes(path);

            return buffer;
        }
        else
            throw new ArgumentException($"Photo with name {fileName} not found");
    }
}
