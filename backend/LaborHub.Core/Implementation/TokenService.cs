using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using LaborHub.Core.Interfaces;
using LaborHub.Core.Options;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace LaborHub.Core.Implementation
{
    public class TokenService : ITokenService
    {
        public const string LanguageJwtClaimName = "lc";
        public const string CurrencyJwtClaimName = "preferred_currency_id";
        private readonly JwtOptions _jwtOptions;

        public TokenService(
            IOptions<JwtOptions> jwtOptions)
        {
            _jwtOptions = jwtOptions.Value;
        }

        /// <summary>
        /// Gets the access token lifetime
        /// </summary>
        /// <returns></returns>
        public TimeSpan AccessTokenLifeTime => TimeSpan.FromMinutes(_jwtOptions.AccessLifetimeMinutes);

        /// <summary>
        /// Gets the refresh token lifetime
        /// </summary>
        /// <returns></returns>
        public TimeSpan RefreshTokenLifeTime => TimeSpan.FromMinutes(_jwtOptions.RefreshLifetimeMinutes);

        /// <summary>
        /// Generates the access token with the given claims
        /// </summary>
        /// <param name="claims">IEnumerable of claims to put in the token's data</param>
        /// <param name="lifetime"></param>
        /// <returns></returns>
        public string GenerateToken(IEnumerable<Claim> claims, TimeSpan lifetime)
        {
            var now = DateTime.UtcNow;
            var jwt = new JwtSecurityToken(
                issuer: _jwtOptions.Issuer,
                audience: _jwtOptions.Audience,
                notBefore: now,
                claims: claims,
                expires: now.Add(lifetime),
                signingCredentials: new SigningCredentials(_jwtOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);
            return encodedJwt;
        }

        /// <summary>
        /// Gets the principal from string access token
        /// </summary>
        /// <param name="token">Access token</param>
        /// <throws>Security token exception</throws>
        /// <returns>Pricipal from token</returns>
        public ClaimsPrincipal GetPrincipalFromExpiredToken(string token)
        {
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateAudience = true, //you might want to validate the audience and issuer depending on your use case
                ValidateIssuer = true,
                ValidAudience = _jwtOptions.Audience,
                ValidIssuer = _jwtOptions.Issuer,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = _jwtOptions.GetSymmetricSecurityKey(),
                ValidateLifetime = false //here we are saying that we don't care about the token's expiration date
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            var principal = tokenHandler.ValidateToken(token, tokenValidationParameters, out var securityToken);
            if (securityToken is not JwtSecurityToken jwtSecurityToken || !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase))
                throw new SecurityTokenException(
                    "_stringLocalizer[ErrorMessageCodes.RefreshTokenNotFound].Value"
                );

            return principal;
        }

        /// <summary>
        /// Generates the refresh token
        /// </summary>
        /// <returns>Refresh token randomized string</returns>
        public string GenerateRefreshToken()
        {
            var refreshToken = GenerateToken(new List<Claim>(), RefreshTokenLifeTime);
            return refreshToken;
        }
    }
}
