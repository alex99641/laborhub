using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using DigitalSkynet.DotnetCore.DataAccess.UnitOfWork;
using DigitalSkynet.DotnetCore.DataStructures.Exceptions.Api;
using LaborHub.Core.Interfaces;
using LaborHub.DataAccess.Repositories.Interfaces;
using LaborHub.Domain.Entities;
using LaborHub.Dtos.User;
using LaborHub.ViewModels.User;
using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;

namespace LaborHub.Core.Implementation;
public class SessionService : ISessionService
    {
        #region Fields definition

        private readonly IUnitOfWork _unitOfWork;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        protected readonly IRefreshTokenRepository _refreshTokenRepository;
        protected readonly IAdvertisementRepository _advertisementRepository;
        private readonly ITokenService _tokenService;

        #endregion

        public SessionService(
            IUnitOfWork unitOfWork,
            UserManager<User> userManager,
            SignInManager<User> signInManager,
            IRefreshTokenRepository refreshTokenRepository,
            IAdvertisementRepository advertisementRepository,
            ITokenService tokenService
            )
        {
            _unitOfWork = unitOfWork;
            _userManager = userManager;
            _signInManager = signInManager;
            _refreshTokenRepository = refreshTokenRepository;
            _advertisementRepository = advertisementRepository;
            _tokenService = tokenService;
        }

        public async Task<JwtTokenView> LoginAsync(LoginDto model, CancellationToken ct)
        {
            // await ValidateInputModelAsync(model, ct);
            if (!model.Password.Any())
            {
                throw new ApiNotAuthenticatedException(
                    "_stringLocalizer[ErrorMessageCodes.LoginOrPasswordAreIncorrect].Value"
                );
            }

            var user = await GetUserAsync(model.UserName);
            var userIsDisabled = await _userManager.IsLockedOutAsync(user);
            var isPasswordValid = await _signInManager.UserManager.CheckPasswordAsync(user, model.Password);
            if (user.IsDeleted)
            {
                throw new ApiNotAuthenticatedException(
                   " _stringLocalizer[ErrorMessageCodes.UserIsDeleted].Value"
                );
            }
            if (userIsDisabled)
            {
                throw new ApiNotAuthenticatedException(
                    "_stringLocalizer[ErrorMessageCodes.UserIsDisabled].Value"
                );
            }
            if (!isPasswordValid)
            {
                throw new ApiNotAuthenticatedException(
                    "_stringLocalizer[ErrorMessageCodes.LoginOrPasswordAreIncorrect].Value"
                );
            }
            // else if (!user.EmailConfirmed)
            // {
            //     throw new ApiNotAuthenticatedException(
            //         "_stringLocalizer[ErrorMessageCodes.EmailNotConfirmed].Value"
            //     );
            // }

            var token = await SetTokenAsync(user, ct);
            SaveRefreshToken(user, token.RefreshToken);

            await _unitOfWork.SaveChangesAsync(ct);

            return token;
        }

        public async Task<JwtTokenView> RefreshAsync(string token, string refreshToken, CancellationToken ct)
        {
            var principal = _tokenService.GetPrincipalFromExpiredToken(token);
            var userIdStr = principal.FindFirstValue(ClaimTypes.NameIdentifier);
            var user = await _signInManager.UserManager.FindByIdAsync(userIdStr!);
            if (user == null)
            {
                throw new ApiNotAuthenticatedException(
                    "_stringLocalizer[ErrorMessageCodes.LoginOrPasswordAreIncorrect].Value"
                );
            }
            var userIsDisabled = await _userManager.IsLockedOutAsync(user);
            if (userIsDisabled)
            {
                throw new ApiNotAuthenticatedException(
                    "_stringLocalizer[ErrorMessageCodes.UserIsDisabled].Value"
                );
            }

            var hasToken = await _refreshTokenRepository.ExistsAsync(x => x.UserId == user.Id && x.Token == refreshToken);

            if (!hasToken)
                throw new SecurityTokenException(
                    "_stringLocalizer[ErrorMessageCodes.RefreshTokenNotFound].Value"
                );

            var newToken = await SetTokenAsync(user, ct);
            SaveRefreshToken(user, newToken.RefreshToken, refreshToken);
            await DeleteRefreshTokenAndExpiredTokens(user, refreshToken, ct);

            await _unitOfWork.SaveChangesAsync(ct);
            return newToken;
        }

        public async Task<bool> LogoutAsync(string token, string refreshToken, CancellationToken ct)
        {
            var principal = _tokenService.GetPrincipalFromExpiredToken(token);
            var username = principal.Identity?.Name;
            var user = await _userManager.GetUserAsync(principal);
            if (user == null)
                throw new ApiNotAuthenticatedException(
                    "_stringLocalizer[ErrorMessageCodes.LoginOrPasswordAreIncorrect].Value"
                );

            var hasToken = await _refreshTokenRepository.ExistsAsync(x => x.UserId == user.Id && x.Token == refreshToken);
            if (hasToken)
            {
                await DeleteRefreshTokenAndExpiredTokens(user, refreshToken, ct);
                await _unitOfWork.SaveChangesAsync(ct);
            }

            await _signInManager.SignOutAsync();
            return true;
        }

        protected async Task<User> GetUserAsync(string username)
        {
            var user = await _signInManager.UserManager.FindByNameAsync(username);
            if (user == null)
            {
                throw new ApiNotAuthenticatedException(
                    "User with this name does not exist"
                );
            }

            return user;
        }

        protected async Task<JwtTokenView> SetTokenAsync(User user, CancellationToken ct)
        {
            var identity = await GetIdentityAsync(user, ct);

            // TODO: why not using Options?
            var encodedJwt = _tokenService.GenerateToken(identity.Claims, _tokenService.AccessTokenLifeTime);
            //var encodedJwt = _tokenService.GenerateToken(identity.Claims, new TimeSpan(0, 10, 0));
            var refreshToken = _tokenService.GenerateRefreshToken();

            var response = new JwtTokenView
            {
                AccessToken = encodedJwt,
                RefreshToken = refreshToken
            };

            return response;
        }

        protected async Task<ClaimsIdentity> GetIdentityAsync(User user, CancellationToken ct)
        {
            var roles = await _signInManager.UserManager.GetRolesAsync(user);

            var userFirstNameClaim = string.IsNullOrEmpty(user.FirstName) ? string.Empty : user.FirstName;
            var userLastNameClaim = string.IsNullOrEmpty(user.LastName) ? string.Empty : user.LastName;

            var claims = new List<Claim>
            {
                new(JwtRegisteredClaimNames.Email, user!.Email! ),
                new(JwtRegisteredClaimNames.UniqueName, user!.UserName!),
                new(JwtRegisteredClaimNames.NameId, user.Id.ToString()),
                new(JwtRegisteredClaimNames.GivenName, userFirstNameClaim),
                new(JwtRegisteredClaimNames.FamilyName, userLastNameClaim),
            };

            var roleClaims = roles.Select(role => new Claim("role", role));
            claims.AddRange(roleClaims);

            await _signInManager.SignInAsync(user, false);

            ClaimsIdentity claimsIdentity = new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            return claimsIdentity;
        }

        protected void SaveRefreshToken(User user, string refreshToken, string? replacedBy = null)
        {
            var now = DateTime.UtcNow;
            var refreshTokenEntity = new RefreshToken
            {
                Token = refreshToken,
                Expires = now.Add(_tokenService.RefreshTokenLifeTime),
                Created = now,
                ReplacedByToken = replacedBy,
                UserId = user.Id
            };

            _refreshTokenRepository.Create(refreshTokenEntity);
        }

        protected async Task DeleteRefreshTokenAndExpiredTokens(User user, string refreshToken, CancellationToken ct)
        {
            var refreshTokenEntity = await _refreshTokenRepository.FindFirstAsync(x => x.UserId == user.Id && x.Token == refreshToken);
            if (refreshTokenEntity != null)
            {
                _refreshTokenRepository.DeleteHard(refreshTokenEntity);
            }
            await DeleteExpiredRefreshTokens(user, ct);
        }

        protected async Task DeleteExpiredRefreshTokens(User user, CancellationToken ct)
        {
            var expiredUserTokens = await _refreshTokenRepository.FilterAsync(x => x.UserId == user.Id && DateTime.UtcNow >= x.Expires, ct: ct);
            _refreshTokenRepository.DeleteHard(expiredUserTokens);
        }
    }