using AutoMapper;
using DigitalSkynet.DotnetCore.DataAccess.Enums;
using DigitalSkynet.DotnetCore.DataAccess.UnitOfWork;
using DigitalSkynet.DotnetCore.DataStructures.Enums.Api;
using DigitalSkynet.DotnetCore.DataStructures.Exceptions.Api;
using DigitalSkynet.DotnetCore.DataStructures.Models.Paging;
using LaborHub.Core.Interfaces;
using LaborHub.DataAccess.Repositories.Interfaces;
using LaborHub.DataAccess.Repositories.Interfaces.FileMedia;
using LaborHub.Domain.Entities;
using LaborHub.Dtos;
using LaborHub.Dtos.Advert;
using LaborHub.Foundation.Constans;
using LaborHub.Foundation.Enums;
using LaborHub.Foundation.Extensions;
using LaborHub.ViewModels.Advert;
using Microsoft.EntityFrameworkCore;

namespace LaborHub.Core.Implementation;

public class AdvertisementService : IAdvertisementService
{
    protected string EntityName => EntityNames.Advertisement;
    private readonly IAdvertisementRepository _advertisementRepository;
    private readonly IAdvertisementMediaRepository _advertisementMediaRepository;
    private readonly IAdvertisementFileRepository _advertisementFileRepository;
    private readonly IFileService _fileService;
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;

    public AdvertisementService(
        IAdvertisementRepository advertisementRepository,
        IAdvertisementMediaRepository advertisementMediaRepository,
        IAdvertisementFileRepository advertisementFileRepository,
        IFileService fileService,
        IMapper mapper,
        IUnitOfWork unitOfWork
    )
    {
        _advertisementRepository = advertisementRepository;
        _advertisementMediaRepository = advertisementMediaRepository;
        _advertisementFileRepository = advertisementFileRepository;
        _fileService = fileService;
        _mapper = mapper;
        _unitOfWork = unitOfWork;
    }

    public async Task<AdvertisementView> GetAsync(Guid id, CancellationToken ct)
    {
        var entity = await _advertisementRepository.GetByAdvertisementIdAsync(id, ct);
        var result = _mapper.Map<AdvertisementView>(entity);

        return await CompleteAdvertViewByFiles(result);
    }

    private async Task<AdvertisementView> CompleteAdvertViewByFiles(AdvertisementView view)
    {
        var medias = new List<string>();
        var files = new List<string>();

        foreach (
            var media in await _advertisementMediaRepository.FilterAsync(
                x => x.AdvertisementId == view.Id
            )
        )
        {
            medias.Add(media.Name);
        }

        foreach (
            var file in await _advertisementFileRepository.FilterAsync(
                x => x.AdvertisementId == view.Id
            )
        )
        {
            files.Add(file.Name);
        }

        view.Medias = medias;
        view.Files = files;

        return view;
    }

    public async Task<Paged<AdvertisementView>> GetAsync(PageAdvertDto model, CancellationToken ct)
    {
        var sorting = new List<SortModel>
        {
            new()
            {
                FieldName = !string.IsNullOrEmpty(model.SortProperty)
                    ? model.SortProperty
                    : nameof(Advertisement.CreatedDate),
                Direction =
                    model.SortDirection == LaborHubSortDirections.Asc
                        ? SortDirections.Asc
                        : SortDirections.Desc
            }
        };

        var predicate = PredicateBuilder.True<AdvertisementView>();

        if (!string.IsNullOrEmpty(model.FilterString))
        {
            predicate = predicate.And(
                x =>
                    x.Title.ToLower().Contains(model.FilterString.ToLower())
                    || x.Description.ToLower().Contains(model.FilterString.ToLower())
            );
        }

        if (model.StartDate.HasValue)
        {
            predicate = predicate.And(x => x.CreatedDate >= model.StartDate);
        }

        if (model.EndDate.HasValue)
        {
            predicate = predicate.And(x => x.CreatedDate <= model.EndDate);
        }

        var pagedView = await _advertisementRepository.ProjectPagedToAsync<AdvertisementView>(
            predicate,
            model.Page,
            model.PageSize,
            sorting,
            FetchModes.NoTracking,
            ct: ct
        );

        #region Complete views by file names
        var completedViews = new List<AdvertisementView>();

        foreach (var advertisementView in pagedView.Data)
        {
            completedViews.Add(await CompleteAdvertViewByFiles(advertisementView));
        }

        // var tempViews = pagedView.Data.Select(async x => await CompleteAdvertViewByFiles(x)).ToList();
        // var completedViews = await Task.WhenAll(tempViews);

        Paged<AdvertisementView> result = new Paged<AdvertisementView>(
            completedViews.ToList(),
            pagedView.Total,
            pagedView.PageNumber,
            pagedView.PageSize
        );
        #endregion


        return result;
    }

    public async Task<AdvertisementView> CreateAsync(
        AdvertisementDto model,
        FileMediaDto fileData,
        CancellationToken ct
    )
    {
        Advertisement entity;
        using (var transaction = await _unitOfWork.EnsureTransactionAsync(ct))
        {
            var fileNames = await _fileService.CreateAsync(fileData.Files, ct);
            var mediaNames = await _fileService.CreateAsync(fileData.Media, ct);

            entity = _advertisementRepository.Create<Advertisement>(
                _mapper.Map<Advertisement>(model)
            );

            foreach (var item in mediaNames)
            {
                _advertisementMediaRepository.Create(
                    new AdvertisementMedia { Name = item, AdvertisementId = entity.Id, }
                );
            }

            foreach (var item in fileNames)
            {
                _advertisementFileRepository.Create(
                    new AdvertisementFile { Name = item, AdvertisementId = entity.Id, }
                );
            }
            await _unitOfWork.SaveChangesAsync(ct);
            transaction.Commit();
        }

        // var result = _mapper.Map<AdvertisementView>(
        //     await _advertisementRepository.GetByAdvertisementIdAsync(entity.Id, ct)
        // );
        var result = await GetAsync(entity.Id, ct);
        return result;
    }

    public async Task<AdvertisementView> UpdateAsync(
        Guid advertId,
        AdvertisementDto model,
        CancellationToken ct
    )
    {
        await _advertisementRepository.UpdateAsync(
            advertId,
            _mapper.Map<Advertisement>(model),
            ct: ct
        );
        await _unitOfWork.SaveChangesAsync(ct);
        var result = _mapper.Map<AdvertisementView>(model);
        return result;
    }

    public async Task<bool> SoftDeleteAsync(Guid advertId, CancellationToken ct)
    {
        if (!await _advertisementRepository.ExistsAsync(advertId, ct: ct))
        {
            throw new ApiNotFoundException(EntityName, advertId.ToString());
        }

        await _advertisementRepository.Delete(advertId, ct);
        await _unitOfWork.SaveChangesAsync(ct);
        return true;
    }
}
