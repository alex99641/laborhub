using AutoMapper;
using DigitalSkynet.DotnetCore.DataAccess.Enums;
using DigitalSkynet.DotnetCore.DataAccess.UnitOfWork;
using DigitalSkynet.DotnetCore.DataStructures.Enums.Api;
using DigitalSkynet.DotnetCore.DataStructures.Exceptions.Api;
using DigitalSkynet.DotnetCore.DataStructures.Models.Paging;
using LaborHub.Core.Interfaces;
using LaborHub.DataAccess.Repositories.Interfaces;
using LaborHub.DataAccess.Repositories.Interfaces.FileMedia;
using LaborHub.Domain.Entities;
using LaborHub.Dtos;
using LaborHub.Dtos.Advert;
using LaborHub.Dtos.Proposal;
using LaborHub.Foundation.Constans;
using LaborHub.Foundation.Enums;
using LaborHub.Foundation.Extensions;
using LaborHub.ViewModels.Advert;
using LaborHub.ViewModels.Proposal;
using Microsoft.EntityFrameworkCore;

namespace LaborHub.Core.Implementation;

public class ProposalService : IProposalService
{
    protected string EntityName => EntityNames.Proposal;
    private readonly IProposalRepository _proposalRepository;
    private readonly IProjectRepository _projectRepository;
    private readonly IProposalFileRepository _proposalFileRepository;
    private readonly IProposalMediaRepository _proposalMediaRepository;
    private readonly IFileService _fileService;
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;

    public ProposalService(
        IProposalRepository proposalRepository,
        IProjectRepository projectRepository,
        IProposalFileRepository proposalFileRepository,
        IProposalMediaRepository proposalMediaRepository,
        IFileService fileService,
        IMapper mapper,
        IUnitOfWork unitOfWork
    )
    {
        _proposalRepository = proposalRepository;
        _projectRepository = projectRepository;
        _proposalFileRepository = proposalFileRepository;
        _proposalMediaRepository = proposalMediaRepository;
        _fileService = fileService;
        _mapper = mapper;
        _unitOfWork = unitOfWork;
    }

    public async Task<ProposalView> GetAsync(Guid id, CancellationToken ct)
    {
        var entity = await _proposalRepository.GetByProposalIdAsync(id, ct);
        var result = _mapper.Map<ProposalView>(entity);

        return await CompleteProposalViewByFiles(result);
    }

    private async Task<ProposalView> CompleteProposalViewByFiles(ProposalView view)
    {
        var medias = new List<string>();
        var files = new List<string>();

        foreach (
            var media in await _proposalMediaRepository.FilterAsync(x => x.ProposalId == view.Id)
        )
        {
            medias.Add(media.Name);
        }

        foreach (var file in await _proposalFileRepository.FilterAsync(x => x.ProposalId == view.Id))
        {
            files.Add(file.Name);
        }

        view.Medias = medias;
        view.Files = files;

        return view;
    }

    public async Task<Paged<ProposalView>> GetAsync(ProposalFilterDto model, CancellationToken ct)
    {
        var sorting = new List<SortModel>
        {
            new()
            {
                FieldName = !string.IsNullOrEmpty(model.SortProperty)
                    ? model.SortProperty
                    : nameof(Proposal.CreatedDate),
                Direction =
                    model.SortDirection == LaborHubSortDirections.Asc
                        ? SortDirections.Asc
                        : SortDirections.Desc
            }
        };

        var predicate = PredicateBuilder.True<ProposalView>();

        if (!string.IsNullOrEmpty(model.FilterString))
        {
            predicate = predicate.And(
                x =>
                    x.Title.ToLower().Contains(model.FilterString.ToLower())
                    || x.Description.ToLower().Contains(model.FilterString.ToLower())
            );
        }

        if (model.StartDate.HasValue)
        {
            predicate = predicate.And(x => x.CreatedDate >= model.StartDate);
        }

        if (model.EndDate.HasValue)
        {
            predicate = predicate.And(x => x.CreatedDate <= model.EndDate);
        }

        var pagedView = await _proposalRepository.ProjectPagedToAsync<ProposalView>(
            predicate,
            model.Page,
            model.PageSize,
            sorting,
            FetchModes.NoTracking,
            ct: ct
        );

        #region Complete views by file names
        var completedViews = new List<ProposalView>();

        foreach (var advertisementView in pagedView.Data)
        {
            completedViews.Add(await CompleteProposalViewByFiles(advertisementView));
        }

        Paged<ProposalView> result = new Paged<ProposalView>(
            completedViews.ToList(),
            pagedView.Total,
            pagedView.PageNumber,
            pagedView.PageSize
        );
        #endregion

        return result;
    }

    public async Task<ProposalView> CreateAsync(ProposalDto model, FileMediaDto fileData, CancellationToken ct)
    {
        // Proposal entity;
        // using (var transaction = await _unitOfWork.EnsureTransactionAsync(ct))
        // {
        var entity = _proposalRepository.Create<Proposal>(_mapper.Map<Proposal>(model));

        await _unitOfWork.SaveChangesAsync(ct);
        //     transaction.Commit();
        // }

        // var result = _mapper.Map<AdvertisementView>(
        //     await _advertisementRepository.GetByAdvertisementIdAsync(entity.Id, ct)
        // );
        var result = await GetAsync(entity.Id, ct);
        return result;
    }

    public async Task<ProposalView> UpdateAsync(
        Guid proposalId,
        ProposalDto model,
        CancellationToken ct
    )
    {
        if (!await _proposalRepository.ExistsAsync(proposalId, ct: ct))
        {
            throw new ApiNotFoundException(EntityName, proposalId.ToString());
        }

        await _proposalRepository.UpdateAsync(proposalId, _mapper.Map<Proposal>(model), ct: ct);
        await _unitOfWork.SaveChangesAsync(ct);
        var result = _mapper.Map<ProposalView>(model);
        return result;
    }

    public async Task<bool> SoftDeleteAsync(Guid proposalId, CancellationToken ct)
    {
        if (!await _proposalRepository.ExistsAsync(proposalId, ct: ct))
        {
            throw new ApiNotFoundException(EntityName, proposalId.ToString());
        }

        await _proposalRepository.Delete(proposalId, ct);
        await _unitOfWork.SaveChangesAsync(ct);
        return true;
    }
}
