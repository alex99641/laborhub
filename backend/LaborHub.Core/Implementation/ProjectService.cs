using AutoMapper;
using DigitalSkynet.DotnetCore.DataAccess.Enums;
using DigitalSkynet.DotnetCore.DataAccess.UnitOfWork;
using DigitalSkynet.DotnetCore.DataStructures.Enums.Api;
using DigitalSkynet.DotnetCore.DataStructures.Exceptions.Api;
using DigitalSkynet.DotnetCore.DataStructures.Models.Paging;
using LaborHub.Core.Interfaces;
using LaborHub.DataAccess.Repositories.Interfaces;
using LaborHub.DataAccess.Repositories.Interfaces.FileMedia;
using LaborHub.Domain.Entities;
using LaborHub.Dtos;
using LaborHub.Dtos.Project;
using LaborHub.Foundation.Constans;
using LaborHub.Foundation.Enums;
using LaborHub.Foundation.Extensions;
using LaborHub.ViewModels.Project;
using Microsoft.EntityFrameworkCore;

namespace LaborHub.Core.Implementation;

public class ProjectService : IProjectService
{
    protected string EntityName => EntityNames.Project;
    private readonly IProposalRepository _proposalRepository;
    private readonly IProjectRepository _projectRepository;
    private readonly IProjectFileRepository _projectFileRepository;
    private readonly IProjectMediaRepository _projectMediaRepository;
    private readonly IFileService _fileService;
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;

    public ProjectService(
        IProposalRepository proposalRepository,
        IProjectRepository projectRepository,
        IProjectFileRepository projectFileRepository,
        IProjectMediaRepository projectMediaRepository,
        IFileService fileService,
        IMapper mapper,
        IUnitOfWork unitOfWork
    )
    {
        _proposalRepository = proposalRepository;
        _projectRepository = projectRepository;
        _projectFileRepository = projectFileRepository;
        _projectMediaRepository = projectMediaRepository;
        _fileService = fileService;
        _mapper = mapper;
        _unitOfWork = unitOfWork;
    }

    public async Task<ProjectView> GetAsync(Guid id, CancellationToken ct)
    {
        var entity = await _projectRepository.GetByProjectIdAsync(id, ct);
        var result = _mapper.Map<ProjectView>(entity);

        return await CompleteProjectViewByFiles(result);
    }

    private async Task<ProjectView> CompleteProjectViewByFiles(ProjectView view)
    {
        var medias = new List<string>();
        var files = new List<string>();

        foreach (
            var media in await _projectMediaRepository.FilterAsync(x => x.ProjectId == view.Id)
        )
        {
            medias.Add(media.Name);
        }

        foreach (var file in await _projectFileRepository.FilterAsync(x => x.ProjectId == view.Id))
        {
            files.Add(file.Name);
        }

        view.Medias = medias;
        view.Files = files;

        return view;
    }

    public async Task<Paged<ProjectView>> GetAsync(ProjectFilterDto model, CancellationToken ct)
    {
        var sorting = new List<SortModel>
        {
            new()
            {
                FieldName = !string.IsNullOrEmpty(model.SortProperty)
                    ? model.SortProperty
                    : nameof(Project.CreatedDate),
                Direction =
                    model.SortDirection == LaborHubSortDirections.Asc
                        ? SortDirections.Asc
                        : SortDirections.Desc
            }
        };

        var predicate = PredicateBuilder.True<ProjectView>();

        if (!string.IsNullOrEmpty(model.FilterString))
        {
            predicate = predicate.And(
                x =>
                    x.Title.ToLower().Contains(model.FilterString.ToLower())
                    || x.Description.ToLower().Contains(model.FilterString.ToLower())
            );
        }

        if (model.StartDate.HasValue)
        {
            predicate = predicate.And(x => x.CreatedDate >= model.StartDate);
        }

        if (model.EndDate.HasValue)
        {
            predicate = predicate.And(x => x.CreatedDate <= model.EndDate);
        }

        var pagedView = await _proposalRepository.ProjectPagedToAsync<ProjectView>(
            predicate,
            model.Page,
            model.PageSize,
            sorting,
            FetchModes.NoTracking,
            ct: ct
        );
        #region Complete views by file names
        var completedViews = new List<ProjectView>();

        foreach (var projectView in pagedView.Data)
        {
            completedViews.Add(await CompleteProjectViewByFiles(projectView));
        }

        Paged<ProjectView> result = new Paged<ProjectView>(
            completedViews.ToList(),
            pagedView.Total,
            pagedView.PageNumber,
            pagedView.PageSize
        );
        #endregion

        return pagedView;
    }

    public async Task<ProjectView> CreateAsync(
        ProjectDto model,
        FileMediaDto fileData,
        CancellationToken ct
    )
    {
        Project entity;
        using (var transaction = await _unitOfWork.EnsureTransactionAsync(ct))
        {
            var fileNames = await _fileService.CreateAsync(fileData.Files, ct);
            var mediaNames = await _fileService.CreateAsync(fileData.Media, ct);

            entity = _projectRepository.Create<Project>(
                _mapper.Map<Project>(model)
            );

            foreach (var item in mediaNames)
            {
                _projectMediaRepository.Create(
                    new ProjectMedia { Name = item, ProjectId = entity.Id, }
                );
            }

            foreach (var item in fileNames)
            {
                _projectFileRepository.Create(
                    new ProjectFile { Name = item, ProjectId = entity.Id, }
                );
            }
            await _unitOfWork.SaveChangesAsync(ct);
            transaction.Commit();
        }

        var result = await GetAsync(entity.Id, ct);
        return result;
    }

    public async Task<ProjectView> UpdateAsync(
        Guid projectId,
        ProjectDto model,
        CancellationToken ct
    )
    {
        if (!await _projectRepository.ExistsAsync(projectId, ct: ct))
        {
            throw new ApiNotFoundException(EntityName, projectId.ToString());
        }

        await _projectRepository.UpdateAsync(projectId, _mapper.Map<Project>(model), ct: ct);
        await _unitOfWork.SaveChangesAsync(ct);
        var result = _mapper.Map<ProjectView>(model);
        return result;
    }

    public async Task<bool> SoftDeleteAsync(Guid projectId, CancellationToken ct)
    {
        if (!await _projectRepository.ExistsAsync(projectId, ct: ct))
        {
            throw new ApiNotFoundException(EntityName, projectId.ToString());
        }

        await _projectRepository.Delete(projectId, ct);
        await _unitOfWork.SaveChangesAsync(ct);
        return true;
    }
}
