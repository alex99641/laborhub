using AutoMapper;
using DigitalSkynet.DotnetCore.DataAccess.Repository;
using DigitalSkynet.DotnetCore.DataAccess.UnitOfWork;
using DigitalSkynet.DotnetCore.DataStructures.Interfaces;
using FluentValidation;
using LaborHub.Core.Extentions;

namespace LaborHub.Core.Implementation;

/// <summary>
/// Class. Represents validation service base. Has protected validator required.
/// Inherits base entity service.
/// </summary>
/// <typeparam name="TInputModel"></typeparam>
/// <typeparam name="TEntity"></typeparam>
/// <typeparam name="TKey"></typeparam>
/// <typeparam name="TRepository"></typeparam>
public abstract class ValidationServiceBase<TInputModel, TRepository, TEntity, TKey> : KeyEntityServiceBase<TRepository, TEntity, TKey>
    where TRepository : class, IGenericRepository<TEntity, TKey>
    where TEntity : class, IHasKey<TKey>
    where TKey : struct
{
    private readonly IValidator<TInputModel> _validator;
    // TODO Move to nuget
    protected ValidationServiceBase(IMapper mapper,
        IUnitOfWork unitOfWork,
        TRepository repository,
        IValidator<TInputModel> validator) : base(mapper, unitOfWork, repository)
    {
        _validator = validator;
    }

    /// <summary>
    /// Validates input model
    /// </summary>
    /// <param name="model"></param>
    /// <param name="ct"></param>
    /// <returns></returns>
    protected virtual async Task ValidateInputModelAsync(TInputModel model, CancellationToken ct)
    {
        var validationResult = await _validator.ValidateAsync(model, ct);
        validationResult.ThrowIfNotValid();
    }
}
