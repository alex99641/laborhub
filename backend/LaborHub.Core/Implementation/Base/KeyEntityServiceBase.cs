using System.Linq.Expressions;
using AutoMapper;
using DigitalSkynet.DotnetCore.DataAccess.Repository;
using DigitalSkynet.DotnetCore.DataAccess.UnitOfWork;
using DigitalSkynet.DotnetCore.DataStructures.Exceptions.Api;
using DigitalSkynet.DotnetCore.DataStructures.Interfaces;
using DigitalSkynet.DotnetCore.DataStructures.Models.Paging;

namespace LaborHub.Core.Implementation;

// TODO Move to nuget
public abstract class KeyEntityServiceBase<TRepository, TEntity, TKey>
    where TRepository : class, IGenericRepository<TEntity, TKey>
    where TEntity : class, IHasKey<TKey>
    where TKey : struct
{
    protected abstract string EntityName { get; }
    protected readonly IUnitOfWork _unitOfWork;
    protected readonly IMapper _mapper;
    protected readonly TRepository _repository;

    protected KeyEntityServiceBase(IMapper mapper,
        IUnitOfWork unitOfWork,
        TRepository repository)
    {
        _unitOfWork = unitOfWork;
        _mapper = mapper;
        _repository = repository;
    }

    protected void ValidateModelExists(object? dto)
    {
        if (dto == null)
        {
            throw new ApiNotFoundException($"{EntityName} has not been found");
        }
    }

    protected virtual async Task<Paged<TProjection>> GetPagedByPredicate<TProjection>(Expression<Func<TProjection, bool>> predicate, PagingModel pagingModel, CancellationToken ct)
    {
        var paged = await _repository.GetPagedProjectionAsync(pagingModel, predicate, ct: ct);
        return paged;
    }
}
