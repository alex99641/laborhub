using AutoMapper;
using DigitalSkynet.DotnetCore.DataAccess.UnitOfWork;
using DigitalSkynet.DotnetCore.DataStructures.Exceptions.Api;
using DigitalSkynet.DotnetCore.DataStructures.Validation;
using LaborHub.Core.Interfaces;
using LaborHub.Core.Validation.Interfaces;
using LaborHub.DataAccess.Repositories.Interfaces;
using LaborHub.Domain.Entities;
using LaborHub.Dtos.User;
using LaborHub.Foundation.Constans;
using LaborHub.ViewModels.User;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Options;

namespace LaborHub.Core.Implementation;

public class AccountService
    : ValidationServiceBase<RegistrationDto, IUserRepository, User, Guid>,
        IAccountService
{
    protected override string EntityName => EntityNames.User;
    private readonly UserManager<User> _userManager;
    private readonly RoleManager<Role> _roleManager;
    private readonly UserOptions _userOptions;
    private readonly ISessionService _sessionService;

    public AccountService(
        IOptions<UserOptions> userOptions,
        UserManager<User> userManager,
        RoleManager<Role> roleManager,
        IUserRepository repository,
        IMapper mapper,
        IUnitOfWork unitOfWork,
        IAccountValidator validator,
        ISessionService sessionService
    )
        : base(mapper, unitOfWork, repository, validator)
    {
        _userOptions = userOptions.Value;
        _userManager = userManager;
        _sessionService = sessionService;
        _roleManager = roleManager;
    }

    public async Task<JwtTokenView> Register(RegistrationDto model, CancellationToken ct)
    {
        await ValidateInputModelAsync(model, ct);

        var user = _mapper.Map<RegistrationDto, User>(model);
        var userByMail = await _userManager.Users
            .Where(x => x.NormalizedEmail == user.Email!.ToUpper() && !x.IsDeleted)
            .FirstOrDefaultAsync(ct);

        if (userByMail != null)
        {
            throw new ApiNotAuthenticatedException(
                "_stringLocalizer[ErrorMessageCodes.RequireUniqueEmail].Value"
            );
        }

        using (var transaction = await _unitOfWork.EnsureTransactionAsync(ct))
        {
            var creationResult = await _userManager.CreateAsync(user, model.Password);

            var validationResult = new ValidationResult();

            foreach (var error in creationResult.Errors)
            {
                validationResult.AddError(error.Description);
            }

            if (
                !creationResult.Succeeded
                && creationResult.Errors.FirstOrDefault()?.Code
                    == nameof(IdentityErrorDescriber.DuplicateUserName)
            )
            {
                throw new ApiNotAuthenticatedException(
                    "_stringLocalizer[ErrorMessageCodes.LoginNotConfirmed].Value"
                );
            }

            if (
                !creationResult.Succeeded
                && creationResult.Errors.FirstOrDefault()?.Code
                    == nameof(IdentityErrorDescriber.DuplicateEmail)
            )
            {
                throw new ApiNotAuthenticatedException(
                    "_stringLocalizer[ErrorMessageCodes.RequireUniqueEmail].Value"
                );
            }

            if (
                !creationResult.Succeeded
                && creationResult.Errors.FirstOrDefault()?.Code
                    == nameof(IdentityErrorDescriber.InvalidUserName)
            )
            {
                throw new ApiNotAuthenticatedException(
                    "_stringLocalizer[ErrorMessageCodes.InvalidUserName].Value"
                );
            }

            if (!creationResult.Succeeded)
            {
                throw new ApiValidationException(validationResult);
            }

            transaction.Commit();
        }

        var result = await _userManager.AddToRoleAsync(user, model.Role);

        if (result.Succeeded)
        {
            await _unitOfWork.SaveChangesAsync(ct);
        }

        return await _sessionService.LoginAsync(
            new LoginDto { UserName = user.UserName!, Password = model.Password },
            ct
        );
    }

    public async Task<UserView> Get(Guid id, CancellationToken ct)
    {
        var dto = await _userManager.FindByIdAsync(id.ToString());
        var result = _mapper.Map<UserView>(dto);
        return result;
    }
}
