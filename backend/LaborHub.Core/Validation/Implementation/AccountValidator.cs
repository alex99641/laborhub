using FluentValidation;
using LaborHub.Core.Implementation;
using LaborHub.Core.Validation.Interfaces;
using LaborHub.Dtos.User;
using LaborHub.Foundation.Constans;
using Microsoft.Extensions.Localization;

namespace LaborHub.Core.Validation.Implementation;

public class AccountValidator : AbstractValidator<RegistrationDto>, IAccountValidator
{
    public AccountValidator()
    {
        RuleFor(x => x)
            .Must(Confirmation)
            .WithMessage("stringLocalizer[ErrorMessageCodes.PasswordDontMatch].Value");
        RuleFor(x => x.UserName)
            .NotEmpty()
            .WithMessage("stringLocalizer[ErrorMessageCodes.LoginMustDontEmpty].Value");
        RuleFor(x => x.FirstName)
            .NotEmpty()
            .WithMessage("stringLocalizer[ErrorMessageCodes.FirstNameMustDontEmpty].Value");
        RuleFor(x => x.LastName)
            .NotEmpty()
            .WithMessage("stringLocalizer[ErrorMessageCodes.LastNameMustDontEmpty].Value");

        // TODO: clarify validation rules here
        RuleFor(x => x.Email)
            .EmailAddress()
            .WithMessage("stringLocalizer[ErrorMessageCodes.EmailAreIncorrect].Value");

        RuleFor(x => x.PhoneNumber)
            .Cascade(CascadeMode.Stop)
            .NotEmpty()
            .WithMessage("stringLocalizer[ErrorMessageCodes.PhoneMustDontEmpty].Value")
            .MaximumLength(18)
            .WithMessage("stringLocalizer[ErrorMessageCodes.PhoneIsIncorrect].Value")
            .Matches(@"^\+\d \(\d{3}\) \d{3}\-\d{2}\-\d{2}$")
            .WithMessage("stringLocalizer[ErrorMessageCodes.PhoneIsIncorrect].Value1");
        RuleFor(x => x)
            .Must(IsValidAge)
            .WithMessage("stringLocalizer[ErrorMessageCodes.AgeAreIncorrect].Value");
    }
    private bool Confirmation(RegistrationDto model)
    {
        return model.Password == model.PasswordConfirm;
    }
    private bool IsValidAge(RegistrationDto model)
    {
        var today = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day);
        var birthday18years =
            new DateTime(model.Birthday.Year + 18, model.Birthday.Month, model.Birthday.Day, 0, 0, 0);
        return birthday18years <= today;
    }
}
