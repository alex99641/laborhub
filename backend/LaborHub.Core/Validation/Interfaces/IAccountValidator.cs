using FluentValidation;
using LaborHub.Dtos.User;

namespace LaborHub.Core.Validation.Interfaces
{
    public interface IAccountValidator : IValidator<RegistrationDto>
    {
    }
}
