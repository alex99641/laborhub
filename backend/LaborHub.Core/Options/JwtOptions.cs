using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace LaborHub.Core.Options;

public class JwtOptions
{
    public string Issuer { get; set; } = string.Empty;
    public string Audience { get; set; } = string.Empty;
    public string Secret { get; set; } = string.Empty;
    public int AccessLifetimeMinutes { get; set; }
    public int RefreshLifetimeMinutes { get; set; }

    public SymmetricSecurityKey GetSymmetricSecurityKey()
    {
        return new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Secret));
    }
}
