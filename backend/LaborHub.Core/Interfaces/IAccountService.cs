using LaborHub.Dtos.User;
using LaborHub.ViewModels.User;

namespace LaborHub.Core.Interfaces
{
    public interface IAccountService
    {
        Task<JwtTokenView> Register(RegistrationDto model, CancellationToken ct);
    }
}
