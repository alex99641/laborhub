using Microsoft.AspNetCore.Http;

namespace LaborHub.Core.Interfaces;

public interface IFileService
{
    Task<string> CreateAsync(IFormFile photo, CancellationToken ct);
    Task<List<string>> CreateAsync(List<IFormFile>? files, CancellationToken ct);
    string GetPhotoUrl(string fileName);
    public byte[] GetPhotoByName(string fileName);
}
