using System;
using System.Collections.Generic;
using System.Security.Claims;

namespace LaborHub.Core.Interfaces
{
    public interface ITokenService
    {
        string GenerateToken(IEnumerable<Claim> claims, TimeSpan lifetime);
        ClaimsPrincipal GetPrincipalFromExpiredToken(string token);
        string GenerateRefreshToken();

        TimeSpan AccessTokenLifeTime { get; }
        TimeSpan RefreshTokenLifeTime { get; }
    }
}