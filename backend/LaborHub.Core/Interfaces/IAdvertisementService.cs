using DigitalSkynet.DotnetCore.DataStructures.Models.Paging;
using LaborHub.Dtos;
using LaborHub.Dtos.Advert;
using LaborHub.Dtos.User;
using LaborHub.ViewModels.Advert;
using LaborHub.ViewModels.User;

namespace LaborHub.Core.Interfaces
{
    public interface IAdvertisementService
    {
        Task<AdvertisementView> GetAsync(Guid id, CancellationToken ct);
        Task<Paged<AdvertisementView>> GetAsync(PageAdvertDto model, CancellationToken ct);
        Task<AdvertisementView> CreateAsync(AdvertisementDto model, FileMediaDto fileData, CancellationToken ct);
        Task<AdvertisementView> UpdateAsync(Guid advertId, AdvertisementDto model, CancellationToken ct);
        Task<bool> SoftDeleteAsync(Guid advertId, CancellationToken ct);
    }
}
