using LaborHub.Dtos.User;
using LaborHub.Foundation.Enums;
using LaborHub.ViewModels.User;

namespace LaborHub.Core.Interfaces;

public interface ISessionService
{
    Task<JwtTokenView> LoginAsync(LoginDto model, CancellationToken ct);
    Task<JwtTokenView> RefreshAsync(string token, string refreshToken, CancellationToken ct);
    Task<bool> LogoutAsync(string token, string refreshToken, CancellationToken ct);
}
