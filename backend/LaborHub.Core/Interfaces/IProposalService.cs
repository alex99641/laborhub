using DigitalSkynet.DotnetCore.DataStructures.Models.Paging;
using LaborHub.Dtos;
using LaborHub.Dtos.Advert;
using LaborHub.Dtos.Proposal;
using LaborHub.Dtos.User;
using LaborHub.ViewModels.Advert;
using LaborHub.ViewModels.Proposal;
using LaborHub.ViewModels.User;

namespace LaborHub.Core.Interfaces
{
    public interface IProposalService
    {
        Task<ProposalView> GetAsync(Guid id, CancellationToken ct);
        Task<Paged<ProposalView>> GetAsync(ProposalFilterDto model, CancellationToken ct);
        Task<ProposalView> CreateAsync(ProposalDto model, FileMediaDto fileData, CancellationToken ct);
        Task<ProposalView> UpdateAsync(Guid advertId, ProposalDto model, CancellationToken ct);
        Task<bool> SoftDeleteAsync(Guid advertId, CancellationToken ct);
    }
}
