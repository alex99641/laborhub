using DigitalSkynet.DotnetCore.DataStructures.Models.Paging;
using LaborHub.Dtos;
using LaborHub.Dtos.Project;
using LaborHub.ViewModels.Project;

namespace LaborHub.Core.Interfaces
{
    public interface IProjectService
    {
        Task<ProjectView> GetAsync(Guid id, CancellationToken ct);
        Task<Paged<ProjectView>> GetAsync(ProjectFilterDto model, CancellationToken ct);
        Task<ProjectView> CreateAsync(ProjectDto model, FileMediaDto fileData, CancellationToken ct);
        Task<ProjectView> UpdateAsync(Guid projectId, ProjectDto model, CancellationToken ct);
        Task<bool> SoftDeleteAsync(Guid projectId, CancellationToken ct);
    }
}
