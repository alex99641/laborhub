﻿using DigitalSkynet.DotnetCore.DataStructures.Models.Response;
using LaborHub.Core.Interfaces;
using LaborHub.Dtos;
using LaborHub.Dtos.Project;
using LaborHub.ViewModels.Project;
using Microsoft.AspNetCore.Mvc;

namespace LaborHub.Controllers;

[ApiController]
[Route("api/project")]
public class ProjectController : LaborHubBaseController<IProjectService>
{
    public ProjectController(IProjectService projectService)
        : base(projectService) { }

    [HttpPost("page")]
    public async Task<ActionResult<ApiPagedResponseEnvelope<ProjectView>>> GetAsync(
        [FromBody] ProjectFilterDto model,
        CancellationToken ct
    )
    {
        var result = await _service.GetAsync(model, ct);
        return PagedCollectionResponse(result);
    }

    [HttpPost]
    public async Task<ActionResult<ApiResponseEnvelope<ProjectView>>> CreateAsync(
        [FromForm] FileMediaDto fileData,
        [FromQuery] ProjectDto model,
        CancellationToken ct
    )
    {
        var result = await _service.CreateAsync(model, fileData, ct);

        return ResponseModel(result);
    }

    [HttpPut("{id:guid}")]
    public async Task<ActionResult<ApiResponseEnvelope<ProjectView>>> UpdateAsync(
        Guid id,
        [FromBody] ProjectDto model,
        CancellationToken ct
    )
    {
        var result = await _service.UpdateAsync(id, model, ct);
        return ResponseModel(result);
    }

    [HttpDelete("{id:guid}")]
    public async Task<ActionResult<ApiResponseEnvelope<bool>>> DeleteAsync(
        Guid id,
        CancellationToken ct
    )
    {
        var result = await _service.SoftDeleteAsync(id, ct);
        return ResponseModel(result);
    }
}
