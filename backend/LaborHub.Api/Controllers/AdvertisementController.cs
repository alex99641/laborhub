﻿using DigitalSkynet.DotnetCore.DataStructures.Models.Response;
using LaborHub.Core.Interfaces;
using LaborHub.Dtos;
using LaborHub.Dtos.Advert;
using LaborHub.ViewModels.Advert;
using Microsoft.AspNetCore.Mvc;

namespace LaborHub.Controllers;

[ApiController]
[Route("api/advertisement")]
public class AdvertisementController : LaborHubBaseController<IAdvertisementService>
{
    public AdvertisementController(IAdvertisementService advertisementService)
        : base(advertisementService) { }

    [HttpPost("page")]
    public async Task<ActionResult<ApiPagedResponseEnvelope<AdvertisementView>>> GetAsync(
        [FromBody] PageAdvertDto model,
        CancellationToken ct
    )
    {
        var result = await _service.GetAsync(model, ct);
        return PagedCollectionResponse(result);
    }

    [HttpPost]
    public async Task<ActionResult<ApiResponseEnvelope<AdvertisementView>>> CreateAsync(
        [FromForm] FileMediaDto fileData,
        [FromQuery] AdvertisementDto model,
        CancellationToken ct
    )
    {
        var result = await _service.CreateAsync(model, fileData, ct);

        return ResponseModel(result);
    }

    [HttpPut("{id:guid}")]
    public async Task<ActionResult<ApiResponseEnvelope<AdvertisementView>>> UpdateAsync(
        Guid id,
        [FromBody] AdvertisementDto model,
        CancellationToken ct
    )
    {
        var result = await _service.UpdateAsync(id, model, ct);
        return ResponseModel(result);
    }

    [HttpDelete("{id:guid}")]
    public async Task<ActionResult<ApiResponseEnvelope<bool>>> DeleteAsync(
        Guid id,
        CancellationToken ct
    )
    {
        var result = await _service.SoftDeleteAsync(id, ct);
        return ResponseModel(result);
    }
}
