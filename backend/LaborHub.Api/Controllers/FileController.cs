﻿using DigitalSkynet.DotnetCore.DataStructures.Models.Response;
using LaborHub.Core.Interfaces;
using LaborHub.Foundation.Constants;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace LaborHub.Controllers;


[ApiController]
[Route("api/file")]
public class FileController : LaborHubBaseController<IFileService>
{
    public FileController(IFileService fileService) : base(fileService)
    {
    }

    [Authorize]
    [Authorize]
    [HttpPost]
    [RequestSizeLimit(RequestConstraints.MaxRequestLength)]
    public async Task<ActionResult<ApiResponseEnvelope<int>>> Create([FromForm] IFormFile media, CancellationToken ct = default)
    {
        await _service.CreateAsync(media, ct);
        return ResponseModel(StatusCodes.Status201Created);
    }

    [HttpGet("{name}")]
    public IActionResult Get(String name)
    {
        byte[] photoBytes = _service.GetPhotoByName(name);

        // Определяем тип контента как изображение
        string contentType = "application/octet-stream";

        // Возвращаем файл с изображением и указываем его тип контента
        return File(photoBytes, contentType, name);
    }
}
