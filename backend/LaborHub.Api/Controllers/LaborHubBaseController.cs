﻿using Microsoft.AspNetCore.Authorization;
using DigitalSkynet.DotnetCore.Api.Controllers;
using Microsoft.AspNetCore.Mvc;

namespace LaborHub.Controllers
{
    public class LaborHubBaseController<TService> : BaseServiceController<TService, Guid>
    {
        public LaborHubBaseController(TService service)
            : base(service) { }
    }
}
