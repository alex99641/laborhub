﻿using DigitalSkynet.DotnetCore.DataStructures.Models.Response;
using LaborHub.Core.Interfaces;
using LaborHub.Dtos;
using LaborHub.Dtos.Proposal;
using LaborHub.ViewModels.Proposal;
using Microsoft.AspNetCore.Mvc;

namespace LaborHub.Controllers;

[ApiController]
[Route("api/proposal")]
public class ProposalController : LaborHubBaseController<IProposalService>
{
    public ProposalController(IProposalService proposalService)
        : base(proposalService) { }

    [HttpPost("page")]
    public async Task<ActionResult<ApiPagedResponseEnvelope<ProposalView>>> GetAsync(
        [FromBody] ProposalFilterDto model,
        CancellationToken ct
    )
    {
        var result = await _service.GetAsync(model, ct);
        return PagedCollectionResponse(result);
    }

    [HttpPost]
    public async Task<ActionResult<ApiResponseEnvelope<ProposalView>>> CreateAsync(
        [FromForm] FileMediaDto fileData,
        [FromQuery] ProposalDto model,
        CancellationToken ct
    )
    {
        var result = await _service.CreateAsync(model, fileData, ct);

        return ResponseModel(result);
    }

    [HttpPut("{id:guid}")]
    public async Task<ActionResult<ApiResponseEnvelope<ProposalView>>> UpdateAsync(
        Guid id,
        [FromBody] ProposalDto model,
        CancellationToken ct
    )
    {
        var result = await _service.UpdateAsync(id, model, ct);
        return ResponseModel(result);
    }

    [HttpDelete("{id:guid}")]
    public async Task<ActionResult<ApiResponseEnvelope<bool>>> DeleteAsync(
        Guid id,
        CancellationToken ct
    )
    {
        var result = await _service.SoftDeleteAsync(id, ct);
        return ResponseModel(result);
    }
}
