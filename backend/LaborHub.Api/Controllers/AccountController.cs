using DigitalSkynet.DotnetCore.DataStructures.Models.Response;
using LaborHub.Controllers;
using LaborHub.Core.Interfaces;
using LaborHub.Dtos.User;
using LaborHub.ViewModels.User;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace LaborHub.Api.Controllers
{
    /// <summary>
    /// Class. Represents account controller
    /// </summary>
    [Route("api/account")]
    [AllowAnonymous]
    public class AccountController : LaborHubBaseController<IAccountService>
    {
        /// <summary>
        /// Creates the controller
        /// </summary>
        /// <param name="accountService">The service to work with accounts</param>
        public AccountController(IAccountService accountService)
            : base(accountService)
        {
        }

        /// <summary>
        /// Registers the user
        /// </summary>
        /// <param name="model"></param>
        /// <param name="ct"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<ApiResponseEnvelope<JwtTokenView>>> Register([FromBody] RegistrationDto model, CancellationToken ct)
        {
            var result = await _service.Register(model, ct);
            return ResponseModel(result);
        }
    }
}
