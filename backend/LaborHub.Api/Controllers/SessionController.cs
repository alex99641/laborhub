﻿using DigitalSkynet.DotnetCore.DataStructures.Models.Response;
using LaborHub.Core.Interfaces;
using LaborHub.Dtos.User;
using LaborHub.ViewModels.User;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace LaborHub.Controllers
{
    [ApiController]
    [Route("api/session")]
    public class SessionController : LaborHubBaseController<ISessionService>
    {
        public SessionController(ISessionService sessionService) : base(sessionService)
        {
        }

        [HttpPost]
        public async Task<ActionResult<ApiResponseEnvelope<JwtTokenView>>> Login([FromBody] LoginDto model, CancellationToken ct)
        {
            var result = await _service.LoginAsync(model, ct);
            return ResponseModel(result);
        }

        [HttpPost("refresh")]
        public async Task<ActionResult<ApiResponseEnvelope<JwtTokenView>>> Refresh([FromBody] JwtTokenDto model, CancellationToken ct)
        {
            var result = await _service.RefreshAsync(model.AccessToken, model.RefreshToken, ct);
            return ResponseModel(result);
        }

        [HttpPost("logout")]
        public async Task<ActionResult<ApiResponseEnvelope<bool>>> Logout([FromBody] JwtTokenDto model, CancellationToken ct)
        {
            var result = await _service.LogoutAsync(model.AccessToken, model.RefreshToken, ct);
            return ResponseModel(result);
        }
    }
}
