using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.IO;
using System.Threading.Tasks;

namespace LaborHub.Api.Middleware;

public class RequestLoggingMiddleware
{
    private readonly RequestDelegate _next;
    private readonly ILogger _logger;

    public RequestLoggingMiddleware(RequestDelegate next, ILogger<RequestLoggingMiddleware> logger)
    {
        _next = next;
        _logger = logger;
    }

    public async Task Invoke(HttpContext context)
    {
        // // // Чтение тела запроса
        // using (StreamReader reader = new StreamReader(context.Request.Body))
        // {
        //     string requestBody = await reader.ReadToEndAsync();

        //     // Логирование входящего запроса
        //     _logger.LogInformation("Incoming Request: {Method} {Path} {QueryString} {Body}",
        //         context.Request.Method,
        //         context.Request.Path,
        //         context.Request.QueryString,
        //         requestBody);
        // }

        // Продолжение обработки запроса
        //await _next(context);
        try
        {
            await _next(context);
        }
        finally
        {
            _logger.LogInformation(
                "Request {method} {Path} {QueryString} => {statusCode}",
                context.Request?.Method,
                context.Request?.Path.Value,
                context.Request?.QueryString,
                context.Response?.StatusCode
            );
            // using (StreamReader reader = new StreamReader(context.Request.Body))
            // {
            //     string requestBody = await reader.ReadToEndAsync();

            //     // Логирование входящего запроса
            //     _logger.LogInformation(
            //         "Request {method} {Path} {QueryString} {Body} => {statusCode}",
            //         context.Request?.Method,
            //         context.Request?.Path.Value,
            //         context.Request.QueryString,
            //         requestBody,
            //         context.Response?.StatusCode
            //     );
            // }
        }
    }
}

public static class RequestLoggingExtensions
{
    public static IApplicationBuilder UseRequestLogging(this IApplicationBuilder app)
    {
        app.UseMiddleware<RequestLoggingMiddleware>();
        return app;
    }
}
