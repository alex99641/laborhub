using System;
using System.Text.Json;
using System.Threading.Tasks;
using DigitalSkynet.DotnetCore.DataStructures.Enums.Api;
using DigitalSkynet.DotnetCore.DataStructures.Exceptions.Api;
using DigitalSkynet.DotnetCore.DataStructures.Models.Response;
using LaborHub.Api.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace LaborHub.Api.Middleware;

/// <summary>
/// Class. Represents the exception middleware
/// </summary>
public class ExceptionMiddleware
{
    private readonly RequestDelegate _next;

    /// <summary>
    /// Constructor. Initializes the middleware
    /// </summary>
    /// <param name="next"></param>
    public ExceptionMiddleware(RequestDelegate next)
    {
        _next = next;
    }

    /// <summary>
    /// Invokes a request. Logs exceptions to the standard ILogger.
    /// </summary>
    public async Task Invoke(HttpContext httpContext, ILoggerFactory loggerFactory)
    {
        try
        {
            await _next(httpContext);
        }
        catch (Exception ex)
        {
            var logger = loggerFactory.CreateLogger<ExceptionMiddleware>();


            if (ex is ApiException apiException)
            {
                logger.LogApiException(apiException);
                await OverrideResponse(new ApiResponseEnvelope(apiException), httpContext);
            }
            else
            {
                logger.LogError(ex, "Unexpected exception");
                await OverrideResponse(new ApiResponseEnvelope(ex), httpContext);
            }
        }
    }

    /// <summary>
    /// Overrides the response. Sets the content type to json
    /// </summary>
    /// <param name="response"></param>
    /// <param name="httpContext"></param>
    /// <returns></returns>
    protected async Task OverrideResponse(ApiResponseEnvelope response, HttpContext httpContext)
    {
        if (!httpContext.Response.HasStarted)
        {
            httpContext.Response.Clear();
            httpContext.Response.ContentType = "application/json";
            httpContext.Response.StatusCode = (int)response.Status.ToHttpStatusCode();

            await httpContext.Response.WriteAsync(JsonSerializer.Serialize(response, options: new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                DictionaryKeyPolicy = JsonNamingPolicy.CamelCase,
                PropertyNameCaseInsensitive = true
            }));
        }
        else
            throw new ApiInvalidOperationException(response.UserMessage,
                "The response stream has already started. Cannot override it with the exception message");
    }
}

/// <summary>
/// Class. Represents the exception middleware extentions
/// </summary>
public static class ExceptionMiddlewareExtentions
{
    /// <summary>
    /// Makes the app to use global exception handler
    /// </summary>
    /// <param name="builder"></param>
    /// <param name="includeDebugInfo"></param>
    /// <returns></returns>
    public static IApplicationBuilder UseGlobalExceptionHandler(this IApplicationBuilder builder, bool includeDebugInfo = true)
    {
        ApiResponseEnvelope.IncludeDebugInfo = includeDebugInfo;
        return builder.UseMiddleware<ExceptionMiddleware>();
    }
}
