using AutoMapper;
using FluentValidation.AspNetCore;
using LaborHub.Api.Configuration;
using LaborHub.Api.Extensions;
using LaborHub.Api.Healthchecks;
using LaborHub.Api.Middleware;
using LaborHub.Core.Configuration;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.EntityFrameworkCore;
using LaborHub.DataAccess.Configuration;

namespace LaborHub;

public class Startup
{
    public Startup(IConfiguration configuration)
    {
        Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {
        services.AddAutoMapper(typeof(CoreMappingProfile));

        services.AddControllers().AddNewtonsoftJson();

        services.AddSwaggerGenConfiguration();

        services.AddOptionsConfiguration(Configuration);

        services.RegisterDBContext(Configuration);

        services.ConfigureIdentity();

        services.AddAuthenticationConfiguration(Configuration);

        services.AddAuthorizationConfiguration();

        services
            .AddHealthChecks()
            .AddCheck<LivenessCheck>("liveness", tags: new[] { "liveness" })
            .AddCheck<ReadinessCheck>("readiness", tags: new[] { "readiness" });

        services.AddRepositories();
        services.AddServices();

        services
            .AddMvc(options =>
            {
                options.AllowEmptyInputInBodyModelBinding = true;
                options.EnableEndpointRouting = false;
            })
            .AddFluentValidation(fv =>
            {
                fv.LocalizationEnabled = false;
            })
            .AddControllersAsServices();
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        var forwardedHeaderOptions = new ForwardedHeadersOptions
        {
            ForwardedHeaders = ForwardedHeaders.All
        };
        forwardedHeaderOptions.KnownNetworks.Clear();
        forwardedHeaderOptions.KnownProxies.Clear();
        app.UseForwardedHeaders(forwardedHeaderOptions);

        app.UseGlobalExceptionHandler();
        app.UseRequestLogging();

        // This is a hack for extracting forwarded protocol, but it works:
        app.Use(
            async (context, next) =>
            {
                if (
                    !context.Request.Headers.ContainsKey("Authorization")
                    && context.Request.Query.TryGetValue("access_token", out var accessToken)
                )
                {
                    context.Request.Headers.Add("Authorization", $"Bearer {accessToken}");
                }
                await next();
            }
        );

        app.UseCookiePolicy();

        app.UseRouting();

        // Am I sure?
        app.UseDefaultFiles();
        app.UseStaticFiles();
        
        app.UseMvc();

        if (env.IsDevelopment() || true)
        {
            app.UseDeveloperExceptionPage();
            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "LaborHub v1"));
        }

        app.UseHttpsRedirection();

        app.UseAuthentication();
        app.UseAuthorization();

        app.UseEndpoints(endpoints =>
        {
            endpoints.MapHealthChecks(
                "/health/liveness",
                new HealthCheckOptions { Predicate = x => x.Name == "liveness" }
            );
            endpoints.MapHealthChecks(
                "/health/readiness",
                new HealthCheckOptions { Predicate = x => x.Name == "readiness" }
            );

            endpoints.MapControllers();
        });

        app.Migrate(Configuration);
    }
}
