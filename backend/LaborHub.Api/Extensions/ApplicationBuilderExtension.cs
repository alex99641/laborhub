using System;
using LaborHub.DataAccess;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace LaborHub.Api.Extensions
{
    /// <summary>
    /// Class. Represents the application builder extentions
    /// </summary>
    public static class ApplicationBuilderExtensions
    {
        /// <summary>
        /// Migrates the database
        /// </summary>
        /// <param name="app">The application</param>
        /// <param name="configuration">Configuration</param>
        public static void Migrate(this IApplicationBuilder app, IConfiguration configuration)
        {
            if (configuration["migrate"] == "up")
            {
                var serviceScope = app.ApplicationServices.CreateScope();

                if (configuration.GetValue<bool>("EnabledFeatures:AutoMigrations"))
                {
                    using var context = serviceScope.ServiceProvider.GetRequiredService<LaborHubDbContext>();
                    var logger = serviceScope.ServiceProvider
                        .GetRequiredService<ILogger<Startup>>();
                    try
                    {
                        context.Database.Migrate();
                    }
                    catch (Exception e)
                    {
                        logger.LogCritical($"Cannot migrate database: {e.Message}");
                        logger.LogWarning(e.StackTrace);
                        throw;
                    }
                }
            }
            else
            {
                var serviceScope = app.ApplicationServices.CreateScope();
                var logger = serviceScope.ServiceProvider
                        .GetRequiredService<ILogger<Startup>>();
                logger.LogCritical($"In ApplicationBuilderExtension.cs on row 23 configuration['migrate'] != 'up'. It's custom message.");
            }
        }
    }
}
