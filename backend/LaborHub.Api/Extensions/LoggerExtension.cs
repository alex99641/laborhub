using DigitalSkynet.DotnetCore.DataStructures.Exceptions.Api;
using Microsoft.Extensions.Logging;

namespace LaborHub.Api.Extensions
{
    /// <summary>
    /// Class. Represents the logger extensions
    /// </summary>
    public static class LoggerExtensions
    {

        /// <summary>
        /// Logs the exceptions using the configured logger
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="exception"></param>
        public static void LogApiException(this ILogger logger, ApiException exception)
        {
            logger.LogError(exception, exception.SystemMessage ?? exception.Message);
        }
    }
}
