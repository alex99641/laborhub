using Microsoft.Extensions.Diagnostics.HealthChecks;
using System.Threading;
using System.Threading.Tasks;

namespace LaborHub.Api.Healthchecks
{
    /// <summary>
    /// Class. Represents the liveness check 
    /// </summary>
    public class LivenessCheck : IHealthCheck
    {
        /// <summary>
        /// Checks if the app is alive
        /// </summary>
        /// <param name="context"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = default)
        {
            return Task.FromResult(HealthCheckResult.Healthy("Healthy"));
        }
    }
}
