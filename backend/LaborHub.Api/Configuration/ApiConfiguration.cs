using System.Security.Claims;
using System.Text;
using LaborHub.Core.Options;
using LaborHub.DataAccess;
using LaborHub.DataAccess.Identity;
using LaborHub.Domain.Entities;
using LaborHub.Domain.Identity;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;

namespace LaborHub.Api.Configuration;

public static class ApiConfiguration
{
    public static void AddSwaggerGenConfiguration(this IServiceCollection services)
    {
        services.AddSwaggerGen(c =>
        {
            c.SwaggerDoc("v1", new OpenApiInfo { Title = "LaborHub.Api", Version = "v1" });
            c.AddSecurityDefinition(
                "Bearer",
                new OpenApiSecurityScheme
                {
                    Description =
                        @"JWT Authorization header using the Bearer scheme. \r\n\r\n
                                            Enter 'Bearer' [space] and then your token in the text input below.
                                            \r\n\r\nExample: 'Bearer 12345abcdef'",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer"
                }
            );
            c.DescribeAllParametersInCamelCase();

            c.AddSecurityRequirement(
                new OpenApiSecurityRequirement()
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Name = "Bearer",
                            In = ParameterLocation.Header,
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            },
                        },
                        new List<string>()
                    }
                }
            );
        });

        services.AddSwaggerGenNewtonsoftSupport();
    }

    public static void AddOptionsConfiguration(
        this IServiceCollection services,
        IConfiguration configuration
    )
    {
        services.Configure<JwtOptions>(configuration.GetSection("Jwt"));
        services.AddSingleton(configuration);
    }

    public static void AddAuthenticationConfiguration(
        this IServiceCollection services,
        IConfiguration configuration
    )
    {
        var jwtOptions = configuration.GetSection("Jwt").Get<JwtOptions>()!;
        services
            .AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(cfg =>
            {
                cfg.RequireHttpsMetadata = false;
                cfg.TokenValidationParameters = new TokenValidationParameters()
                {
                    RequireExpirationTime = true,
                    ValidateLifetime = false,
                    ValidateIssuer = true,
                    ValidIssuer = jwtOptions.Issuer,
                    ValidateAudience = true,
                    ValidAudience = jwtOptions.Audience,
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(
                        Encoding.Default.GetBytes(jwtOptions.Secret)
                    )
                };
                cfg.Events = new JwtBearerEvents
                {
                    OnTokenValidated = async context =>
                    {
                        var userManager = context.HttpContext.RequestServices.GetRequiredService<
                            UserManager<User>
                        >();
                        var userName = context.Principal?.Identity?.Name;
                        if (userName == null)
                            return;
                        var user = await userManager.FindByNameAsync(userName);
                        if (user == null)
                            return;
                        var photoPathClaim = new Claim("pic", user.Photo); // добавление пути к фото профиля
                        ((ClaimsIdentity)context.Principal!.Identity!).AddClaim(photoPathClaim); // добавление пути к фото профиля в токен
                        return;
                    }
                };
            });
    }

    public static void AddAuthorizationConfiguration(this IServiceCollection services)
    {
        services.AddAuthorizationCore(cfg =>
        {
            var defaultPolicy = new AuthorizationPolicyBuilder()
                .AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme)
                .RequireAuthenticatedUser()
                .Build();
            cfg.AddPolicy(
                "admin",
                policy =>
                    policy
                        .RequireRole(new List<string> { "admin" })
                        .AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme)
            );
            cfg.AddPolicy(
                "client",
                policy =>
                    policy
                        .RequireRole(new List<string> { "client" })
                        .AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme)
            );
            cfg.AddPolicy(
                "master",
                policy =>
                    policy
                        .RequireRole(new List<string> { "master" })
                        .AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme)
            );
            cfg.AddPolicy(
                "worker",
                policy =>
                    policy
                        .RequireRole(new List<string> { "worker" })
                        .AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme)
            );

            cfg.AddPolicy("default", defaultPolicy);
            cfg.DefaultPolicy = defaultPolicy;
        });
    }

    public static void RegisterDBContext(
        this IServiceCollection services,
        IConfiguration configuration
    )
    {
        services.AddDbContext<LaborHubDbContext>(options =>
        {
            options.UseNpgsql(
                configuration.GetConnectionString("DefaultConnection")!,
                o => o.UseNetTopologySuite()
            );
            options.EnableSensitiveDataLogging();
        });

        services.AddDataProtection().PersistKeysToDbContext<LaborHubDbContext>();
    }

    public static void ConfigureIdentity(this IServiceCollection services)
    {
        services
            .AddIdentity<User, Role>(opts =>
            {
                opts.Password.RequiredLength = 5;
                opts.Password.RequireNonAlphanumeric = false;
                opts.Password.RequireLowercase = false;
                opts.Password.RequireUppercase = false;
                opts.Password.RequireDigit = false;
                opts.SignIn.RequireConfirmedEmail = false;
                //opts.User.RequireUniqueEmail = true;
            })
            .AddEntityFrameworkStores<LaborHubDbContext>()
            // .AddUserManager<LaborHubUserManager>()
            // .AddUserStore<LaborHubUserStore>()
            // .AddRoleStore<LaborHubRoleStore>()
            // .AddRoleManager<LaborHubRoleManager>()
            // .AddSignInManager<LaborHubSignInManager>()
            .AddDefaultTokenProviders();
    }
}
