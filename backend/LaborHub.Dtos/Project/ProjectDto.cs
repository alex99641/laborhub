using LaborHub.Dtos.Infrastructure;
using LaborHub.Foundation.Enums;

namespace LaborHub.Dtos.Project;

public class ProjectDto : IInputModel
{
    public string Title { get; set; }
    public string Description { get; set; }
    public ProjectStatus ProjectStatus { get; set; }
    public Guid AdvertisementId { get; set; }
    public Guid ProposalId { get; set; }
    public Guid ClientId { get; set; }
    public Guid MasterId { get; set; }
}
