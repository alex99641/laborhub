using LaborHub.Dtos.Infrastructure;
using LaborHub.Foundation.Enums;

namespace LaborHub.Dtos.Proposal;

public class ProposalDto : IInputModel
{
    public string Title { get; set; }
    public string Description { get; set; }
    public ProposalStatus ProposalStatus { get; set; }
    public Guid AuthorId { get; set; } 
    public Guid AdvertisementId { get; set; }
}
