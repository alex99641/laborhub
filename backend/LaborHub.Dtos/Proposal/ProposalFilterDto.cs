using LaborHub.Dtos.Infrastructure;
using LaborHub.Foundation.Enums;

namespace LaborHub.Dtos.Proposal;

public class ProposalFilterDto : IPagedModel
{
    public int Page { get; set; }
    public Guid? AdvertisementId { get; set; }
    public Guid? ClientId { get; set; }
    public Guid? MasterId { get; set; }
    public string SortProperty { get; set; } = string.Empty;
    public LaborHubSortDirections SortDirection { get; set; }
    public string FilterString { get; set; } = string.Empty;
    public int PageSize { get; set; }
    public DateTime? StartDate { get; set; }
    public DateTime? EndDate { get; set; }
}
