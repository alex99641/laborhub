


using LaborHub.Foundation.Enums;

namespace LaborHub.Dtos.Infrastructure;

public interface IPagedModel : IInputModel
{
    int Page { get; set; }
    int PageSize { get; set; }
    string SortProperty { get; set; }
    LaborHubSortDirections SortDirection { get; set; }
}
