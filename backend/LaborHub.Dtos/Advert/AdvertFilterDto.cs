

using LaborHub.Dtos.Infrastructure;
using LaborHub.Foundation.Enums;

namespace LaborHub.Dtos.Advert;

public class PageAdvertDto : IPagedModel
{
    public int Page { get; set; }
    public string SortProperty { get; set; } = string.Empty;
    public LaborHubSortDirections SortDirection { get; set; }
    public string FilterString { get; set; } = string.Empty;
    public int PageSize { get; set; }
    public decimal? Latitude { get; set; }
    public decimal? Longitude { get; set; }
    public DateTime? StartDate { get; set; }
    public DateTime? EndDate { get; set; }
}
