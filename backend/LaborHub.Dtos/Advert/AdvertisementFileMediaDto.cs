using LaborHub.Dtos.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace LaborHub.Dtos.Advert;

public class AdvertisementFileMediaDto : IInputModel
{
    [FromForm(Name = "Files")]
    public List<IFormFile>? Files { get; set; }
    [FromForm(Name = "Media")]
    public List<IFormFile>? Media { get; set; }
}
