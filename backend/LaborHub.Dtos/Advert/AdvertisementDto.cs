using LaborHub.Dtos.Infrastructure;

namespace LaborHub.Dtos.Advert;

public class AdvertisementDto : IInputModel
{
    public string Title { get; set; }
    public string Description { get; set; }
    public double Latitude { get; set; }
    public double Longitude { get; set; }
    public string AuthorId { get; set; }
}
