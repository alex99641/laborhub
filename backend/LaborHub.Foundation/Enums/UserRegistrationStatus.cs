﻿namespace LaborHub.Foundation.Enums
{
    public enum UserRegistrationStatus
    {
        ExistingUser,
        NewUser
    }
}
