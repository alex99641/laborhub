namespace LaborHub.Foundation.Enums
{
    public enum UserSearchStatus
    {
        Any = 0,
        DisabledOnly = 1,
        EnabledOnly = 2
    }
}
