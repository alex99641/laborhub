﻿namespace LaborHub.Foundation.Enums
{
    public enum ProposalStatus
    {
        Waiting,
        Declined,
        DeclinedByAcceptingOther,
        Cancelled,
        Accepted
    }
}
