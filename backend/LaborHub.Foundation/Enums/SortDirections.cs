namespace LaborHub.Foundation.Enums;

public enum LaborHubSortDirections
{
    Asc = 1,
    Desc = 2
}
