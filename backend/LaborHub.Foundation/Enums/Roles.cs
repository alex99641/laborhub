namespace LaborHub.Foundation.Enums
{
    public enum Roles
    {
        Admin,
        Client,
        Master,
        Worker
    }
}
