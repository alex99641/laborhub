﻿namespace LaborHub.Foundation.Enums
{
    public enum ProjectStatus
    {
        InProgress,
        Completed
    }
}
