namespace LaborHub.Foundation.Constants;

public class ConstantIds
{
    public static readonly Guid SystemUserId = new Guid("132456ee-4f53-4943-a642-233fea6ee8e1");
    public const string SystemUserName = "Flash Media";
    public const string SystemUserFirstName = "Flash";
    public const string SystemUserLastNameName = "Media";
}
