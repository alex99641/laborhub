namespace LaborHub.Foundation.Constans;

public class EntityNames
{
    public const string User = "User";
    public const string Advertisement = "Advertisement";
    public const string Proposal = "Proposal";
    public const string Project = "Project";
    public const string MediaSpace = "Media Space";
    public const string Price = "Price";
    public const string PriceGroup = "PriceGroup";
    public const string ChatMessage = "ChatMessage";
    public const string Schedule = "Schedule";
    public const string Order = "Order";
    public const string Video = "Video";
    public const string Photo = "Photo";
    public const string Device = "Device";
    public const string PlayedFile = "Played File";
    public const string PlayLog = "Play Log";
    public const string Currency = "Currency";
    public const string Scheduler = "Scheduler";
}
