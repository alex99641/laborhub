namespace LaborHub.Foundation.Constants
{
    public class RequestConstraints
    {
        public const int MaxRequestLength = 1024 * 1024 * 500; // 500MB
    }
}
