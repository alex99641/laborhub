using LaborHub.ViewModels.Advert;
using LaborHub.ViewModels.Infrastructure;
using LaborHub.ViewModels.User;

namespace LaborHub.ViewModels.Proposal;

public class ProposalView : BaseGuidView
{
    public string Title { get; set; }
    public string Description { get; set; }
    public string ProposalStatus { get; set; }
    public List<string> Medias { get; set; }
    public List<string> Files { get; set; }
    public virtual UserView Author { get; set; }
    public virtual AdvertisementView Advertisement { get; set; }
}
