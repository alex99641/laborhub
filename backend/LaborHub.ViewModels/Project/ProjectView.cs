using LaborHub.Foundation.Enums;
using LaborHub.ViewModels.Advert;
using LaborHub.ViewModels.Infrastructure;
using LaborHub.ViewModels.Proposal;
using LaborHub.ViewModels.User;

namespace LaborHub.ViewModels.Project;

public class ProjectView : BaseGuidView
{
    public string Title { get; set; }
    public string Description { get; set; }
    public ProjectStatus ProjectStatus { get; set; }
    public List<string> Medias { get; set; }
    public List<string> Files { get; set; }
    public virtual UserView Master { get; set; }
    public virtual UserView Client { get; set; }
    public virtual AdvertisementView Advertisement { get; set; }
    public virtual ProposalView Proposal { get; set; }
}
