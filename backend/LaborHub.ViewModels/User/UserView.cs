﻿using LaborHub.ViewModels.Infrastructure;

namespace LaborHub.ViewModels.User;

public class UserView : BaseGuidView
    {
        public string UserName { get; set; } = string.Empty;
        public string Email { get; set; } = string.Empty;
        public string PhoneNumber { get; set; } = string.Empty;
        public string? Role { get; set; }
        public DateTime? Birthday { get; set; }
        public bool LockoutEnabled { get; set; }
        public string FirstName { get; set; } = string.Empty;
        public string LastName { get; set; } = string.Empty;
        public string Photo { get; set; } = string.Empty;
        public DateTimeOffset? LockoutEnd { get; set; }
        public bool IsLockedOut => LockoutEnd.HasValue && DateTime.UtcNow < LockoutEnd.Value.Date;
        public string Locale { get; set; } = string.Empty;
    }
