﻿namespace LaborHub.ViewModels.User;

public class JwtTokenView
{
    public string AccessToken { get; set; } = string.Empty;
    public string RefreshToken { get; set; } = string.Empty;
}
