using LaborHub.ViewModels.Infrastructure;
using LaborHub.ViewModels.User;

namespace LaborHub.ViewModels.Advert;

public class AdvertisementView : BaseGuidView
{
    public string Title { get; set; }
    public string Description { get; set; }
    public decimal Latitude { get; set; }
    public decimal Longitude { get; set; }
    public List<string> Medias { get; set; }
    public List<string> Files { get; set; }
    public virtual UserView Author { get; set; }
}
