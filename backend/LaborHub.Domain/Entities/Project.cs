
using System.ComponentModel.DataAnnotations.Schema;
using LaborHub.Domain.Infrastructure;
using LaborHub.Foundation.Enums;

namespace LaborHub.Domain.Entities;

public class Project : BaseGuidEntity
{
    public string Title { get; set; }
    public string Description { get; set; }
    public ProjectStatus ProjectStatus { get; set; }

    [ForeignKey(nameof(User))]
    public Guid ClientId { get; set; }
    
    [ForeignKey(nameof(User))]
    public Guid MasterId { get; set; }
    
    [ForeignKey(nameof(Advertisement))]
    public Guid AdvertisementId { get; set; }
    [ForeignKey(nameof(Proposal))]
    public Guid ProposalId { get; set; }

    #region Virtual
    public virtual Advertisement Advertisement { get; set; }
    public virtual User Client { get; set; }
    public virtual User Master { get; set; }
    public virtual Proposal Proposal { get; set; }
    #endregion
}
