
using System.ComponentModel.DataAnnotations.Schema;
using LaborHub.Domain.Infrastructure;

namespace LaborHub.Domain.Entities;

public class ProjectMedia : BaseGuidEntity
{
    public string Name { get; set; }

    [ForeignKey(nameof(Project))]
    public Guid ProjectId { get; set; }

    #region Virtual

    public virtual Project Project { get; set; }

    #endregion
}
