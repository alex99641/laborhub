
using System.ComponentModel.DataAnnotations.Schema;
using LaborHub.Domain.Infrastructure;

namespace LaborHub.Domain.Entities;

public class AdvertisementMedia : BaseGuidEntity
{
    public string Name { get; set; }

    [ForeignKey(nameof(Advertisement))]
    public Guid AdvertisementId { get; set; }

    #region Virtual

    public virtual Advertisement Advertisement { get; set; }

    #endregion
}
