
using System.ComponentModel.DataAnnotations.Schema;
using LaborHub.Domain.Infrastructure;

namespace LaborHub.Domain.Entities;

public class ProposalMedia : BaseGuidEntity
{
    public string Name { get; set; }

    [ForeignKey(nameof(Proposal))]
    public Guid ProposalId { get; set; }

    #region Virtual

    public virtual Proposal Proposal { get; set; }

    #endregion
}
