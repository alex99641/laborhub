
using System.ComponentModel.DataAnnotations.Schema;
using LaborHub.Domain.Infrastructure;
using LaborHub.Foundation.Enums;

namespace LaborHub.Domain.Entities;

public class Proposal : BaseGuidEntity
{
    public string Title { get; set; }
    public string Description { get; set; }
    public ProposalStatus ProposalStatus { get; set; }

    [ForeignKey(nameof(User))]
    public Guid AuthorId { get; set; }    

    [ForeignKey(nameof(Advertisement))]
    public Guid AdvertisementId { get; set; }

    #region Virtual
    public virtual User Author { get; set; }
    public virtual Advertisement Advertisement { get; set; }
    #endregion
}
