using DigitalSkynet.DotnetCore.DataStructures.Interfaces;
using Microsoft.AspNetCore.Identity;

namespace LaborHub.Domain.Entities;

public class User : IdentityUser<Guid>, IHasKey<Guid>, ISoftDeletable, ITimestamped
{
    public string FirstName { get; set; } = String.Empty;
    public string LastName { get; set; } = String.Empty;
    public DateTime? Birthday { get; set; }
    public virtual string Photo { get; set; } = String.Empty;
    public virtual List<UserRole> UserRoles { get; set; }
    public DateTime CreatedDate { get; set; }
    public DateTime UpdatedDate { get; set; }
    public bool IsDeleted { get; set; }
}
