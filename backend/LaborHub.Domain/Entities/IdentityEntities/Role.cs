using DigitalSkynet.DotnetCore.DataStructures.Interfaces;
using Microsoft.AspNetCore.Identity;

namespace LaborHub.Domain.Entities;

public class Role : IdentityRole<Guid>, IHasKey<Guid>, ISoftDeletable, ITimestamped
{
    public DateTime CreatedDate { get; set; }
    public DateTime UpdatedDate { get; set; }
    public bool IsDeleted { get; set; }
}
