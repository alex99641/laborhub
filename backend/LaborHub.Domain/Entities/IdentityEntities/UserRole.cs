using System.ComponentModel.DataAnnotations.Schema;
using DigitalSkynet.DotnetCore.DataStructures.Interfaces;
using Microsoft.AspNetCore.Identity;


namespace LaborHub.Domain.Entities
{
    public class UserRole : IdentityUserRole<Guid>, ITimestamped
    {
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }

        public virtual User User {get;set;}
        public virtual Role Role {get;set;}
    }
}
