using DigitalSkynet.DotnetCore.DataStructures.Interfaces;
using LaborHub.Domain.Infrastructure;
using NetTopologySuite.Geometries;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace LaborHub.Domain.Entities;

public class Advertisement : BaseGuidEntity
{
    public string Title { get; set; }
    public string Description { get; set; }
    public decimal Latitude { get; set; }
    public decimal Longitude { get; set; }
    
    [Column(TypeName="geometry (point)")]
    public Point Location { get; set; }

    [ForeignKey(nameof(User))]
    public Guid AuthorId { get; set; }
    public virtual User Author { get; set; }
}
