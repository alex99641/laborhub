import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:get/route_manager.dart';
import 'package:laborhub/core/services/locator_service.dart';
import 'package:laborhub/features/bussiness_logic/blocs/authentication/authentication_bloc.dart';
import 'package:laborhub/features/bussiness_logic/enums/roles.dart';
import 'package:laborhub/features/presentation/home_page_admin.dart';
import 'package:laborhub/features/presentation/home_page_client.dart';
import 'package:laborhub/features/presentation/home_page_master.dart';
import 'package:laborhub/features/presentation/home_page_worker.dart';
import 'package:laborhub/features/presentation/login_page.dart';

/// This widget is the root of application.
class App extends StatefulWidget {
  const App({super.key});

  @override
  State<App> createState() => _AppState();
}

class _AppState extends State<App> {
  final _navigatorKey = GlobalKey<NavigatorState>();

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<AuthBloc>(
          create: (_) => sl<AuthBloc>(),
        ),
      ],
      child: GetMaterialApp(
        title: 'Flutter Demo',
        locale: context.locale,
        supportedLocales: context.supportedLocales,
        localizationsDelegates: [
          ...context.localizationDelegates,
          FormBuilderLocalizations.delegate,
        ],
        theme: ThemeData(
          useMaterial3: true,
          primarySwatch: Colors.blue,
        ),
        darkTheme: ThemeData.dark(),
        navigatorKey: _navigatorKey,
        builder: (context, child) {
          return BlocListener<AuthBloc, AuthenticationState>(
            listener: (context, state) {
              if (state is AuthenticatedState) {
                switch (state.profile.role) {
                  case Roles.admin:
                    Get.offAll(() => HomePageAdmin(title: 'Hello, ${state.profile.role.name}'));
                    break;
                  case Roles.client:
                    Get.offAll(() => HomePageClient(title: 'Hello, ${state.profile.role.name}'));
                    break;
                  case Roles.master:
                    Get.offAll(() => HomePageMaster(title: 'Hello, ${state.profile.role.name}'));
                    break;
                  case Roles.worker:
                    Get.offAll(() => HomePageWorker(title: 'Hello, ${state.profile.role.name}'));
                    break;
                  default:
                }
              } else {
                Get.to(LoginPage());
              }
            },
            child: child,
          );
        },
        home: LoginPage(),
      ),
    );
  }
}
