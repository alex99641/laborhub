import 'package:laborhub/features/data/dtos/signup_dto.dart';
import 'package:laborhub/features/data/models/tokens_model.dart';
import 'package:laborhub/features/data/sources/account_datasource.dart';

class AccountRepository {
  final AccountDatasource _accountDatasource;

  AccountRepository({required AccountDatasource accountDatasource})
      : _accountDatasource = accountDatasource;

  Future<TokensModel> signUp({required SignUpDto signUpDto}) async {
    return await _accountDatasource.signUp(signUpDto);
  }
}
