import 'package:laborhub/features/data/models/tokens_model.dart';
import 'package:laborhub/features/data/sources/token_local_datasource.dart';

class TokenRepository {
  final TokenLocalDatasource _tokenLocalDatasource;

  TokenRepository({required TokenLocalDatasource tokenLocalDatasource})
      : _tokenLocalDatasource = tokenLocalDatasource;

  void setTokens(TokensModel tokensModel) {
    _tokenLocalDatasource.setTokens(tokensModel);
  }

  /// Получение текущих токенов из памяти
  TokensModel? getTokens() {
    return _tokenLocalDatasource.getTokens();
  }

  /// Получение валидного access token
  Future<String?> getAccessToken() async {
    return _tokenLocalDatasource.getAccessToken();
  }

  void removeTokens() {
    _tokenLocalDatasource.removeTokens();
  }
}
