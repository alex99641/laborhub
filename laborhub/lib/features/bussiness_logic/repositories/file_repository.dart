import 'package:laborhub/features/data/dtos/signup_dto.dart';
import 'package:laborhub/features/data/models/tokens_model.dart';
import 'package:laborhub/features/data/sources/file_datasource.dart';

class FileRepository {
  final FileDatasource _fileDatasource;

  FileRepository({required FileDatasource fileDatasource}) : _fileDatasource = fileDatasource;

  Future<void> loadFile({required String name}) async {
    return await _fileDatasource.loadFile(name);
  }
}
