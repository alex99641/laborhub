import 'package:laborhub/features/data/models/tokens_model.dart';
import 'package:laborhub/features/data/sources/auth_datasource.dart';

class AuthenticationRepository {
  final AuthenticationDatasource _authenticationDatasource;

  AuthenticationRepository({required AuthenticationDatasource authenticationDatasource})
      : _authenticationDatasource = authenticationDatasource;

  Future<TokensModel> login({required String userName, required String password}) async {
    return await _authenticationDatasource.login(userName, password);
  }

  Future<void> logout(TokensModel tokensModel) async {
    return _authenticationDatasource.logout(tokensModel);
  }

  Future<TokensModel> refresh({required TokensModel oldTokens}) async {
    return await _authenticationDatasource.refresh(oldTokens);
  }
}
