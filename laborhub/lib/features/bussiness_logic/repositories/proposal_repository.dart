import 'package:laborhub/features/bussiness_logic/internal_models/paged_internal_model.dart';
import 'package:laborhub/features/data/dtos/proposal_dto.dart';
import 'package:laborhub/features/data/dtos/proposal_filter_dto.dart';
import 'package:laborhub/features/data/models/proposal_model.dart';
import 'package:laborhub/features/data/sources/proposal_datasource.dart';

class ProposalRepository {
  final ProposalDatasource _proposalDatasource;

  ProposalRepository({required ProposalDatasource datasource}) : _proposalDatasource = datasource;

  Future<PagedInternalModel<ProposalModel>> getProposals(ProposalFilterDto filterDto) async {
    return await _proposalDatasource.getProposals(filterDto);
  }

  Future<ProposalModel> createProposal(ProposalDto proposal) async {
    return await _proposalDatasource.createProposal(proposal);
  }

  Future<ProposalModel> updateProposal(String id, ProposalDto proposal) async {
    return await _proposalDatasource.updateProposal(id, proposal);
  }

  Future<bool> deleteProposal(String id) async {
    return await _proposalDatasource.deleteProposal(id);
  }
}
