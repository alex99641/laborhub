import 'package:laborhub/features/bussiness_logic/internal_models/paged_internal_model.dart';
import 'package:laborhub/features/data/dtos/advertisement_dto.dart';
import 'package:laborhub/features/data/dtos/page_advert_dto.dart';
import 'package:laborhub/features/data/models/advertisement_model.dart';
import 'package:laborhub/features/data/sources/advertisement_datasource.dart';

class AdvertisementRepository {
  final AdvertisementDatasource _advertisementDatasource;

  AdvertisementRepository({required AdvertisementDatasource datasource})
      : _advertisementDatasource = datasource;

  Future<PagedInternalModel<AdvertisementModel>> getAdvertisements(
      PageAdvertDto pageAdvertDto) async {
    return await _advertisementDatasource.getAdvertisements(pageAdvertDto);
  }

  Future<AdvertisementModel> createAdvertisement(AdvertisementDto advertisement) async {
    return await _advertisementDatasource.createAdvertisement(advertisement);
  }

  Future<AdvertisementModel> updateAdvertisement(String id, AdvertisementDto advertisement) async {
    return await _advertisementDatasource.updateAdvertisement(id, advertisement);
  }

  Future<bool> deleteAdvertisement(String id) async {
    return await _advertisementDatasource.deleteAdvertisement(id);
  }
}
