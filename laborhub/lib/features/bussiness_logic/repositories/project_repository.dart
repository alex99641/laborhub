import 'package:laborhub/features/bussiness_logic/internal_models/paged_internal_model.dart';
import 'package:laborhub/features/data/dtos/project_dto.dart';
import 'package:laborhub/features/data/dtos/project_filter_dto.dart';
import 'package:laborhub/features/data/models/project_model.dart';
import 'package:laborhub/features/data/sources/project_datasource.dart';

class ProjectRepository {
  final ProjectDatasource _projectDatasource;

  ProjectRepository({required ProjectDatasource datasource}) : _projectDatasource = datasource;

  Future<PagedInternalModel<ProjectModel>> getProjects(ProjectFilterDto filterDto) async {
    return await _projectDatasource.getProjects(filterDto);
  }

  Future<ProjectModel> createProject(ProjectDto project) async {
    return await _projectDatasource.createProject(project);
  }

  Future<ProjectModel> updateProject(String id, ProjectDto project) async {
    return await _projectDatasource.updateProject(id, project);
  }

  Future<bool> deleteProject(String id) async {
    return await _projectDatasource.deleteProject(id);
  }
}
