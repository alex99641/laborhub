import 'dart:convert';

import 'package:enum_to_string/enum_to_string.dart';
import 'package:laborhub/features/bussiness_logic/enums/roles.dart';

class ProfileEntity {
  String email;
  String uniqueName;
  String nameid;
  String givenName;
  String familyName;
  String lc;
  String hasSignedInOnce;
  String pic;
  Roles role;
  int nbf;
  int exp;
  String iss;
  String aud;

  ProfileEntity({
    required this.email,
    required this.uniqueName,
    required this.nameid,
    required this.givenName,
    required this.familyName,
    required this.lc,
    required this.hasSignedInOnce,
    required this.pic,
    required this.role,
    required this.nbf,
    required this.exp,
    required this.iss,
    required this.aud,
  });

  Map<String, dynamic> toMap() {
    return {
      'email': email,
      'unique_name': uniqueName,
      'nameid': nameid,
      'given_name': givenName,
      'family_name': familyName,
      'lc': lc,
      'hasSignedInOnce': hasSignedInOnce,
      'pic': pic,
      'role': role.name,
      'nbf': nbf,
      'exp': exp,
      'iss': iss,
      'aud': aud,
    };
  }

  factory ProfileEntity.fromMap(Map<String, dynamic> map) {
    return ProfileEntity(
      email: map['email'] ?? '',
      uniqueName: map['unique_name'] ?? '',
      nameid: map['nameid'] ?? '',
      givenName: map['given_name'] ?? '',
      familyName: map['family_name'] ?? '',
      lc: map['lc'] ?? '',
      hasSignedInOnce: map['hasSignedInOnce'] ?? '',
      pic: map['pic'] ?? '',
      role: EnumToString.fromString(Roles.values, map['role'])!,
      nbf: map['nbf']?.toInt() ?? 0,
      exp: map['exp']?.toInt() ?? 0,
      iss: map['iss'] ?? '',
      aud: map['aud'] ?? '',
    );
  }

  String toJson() => json.encode(toMap());

  factory ProfileEntity.fromJson(String source) => ProfileEntity.fromMap(json.decode(source));
}
