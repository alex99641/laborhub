import 'package:yandex_mapkit/yandex_mapkit.dart';

abstract class MapCubit {}

abstract class MapUpdatableCubit extends MapCubit {
  void setPoint({required Point point});
}

abstract class MapUnupdatableCubit extends MapCubit {}
