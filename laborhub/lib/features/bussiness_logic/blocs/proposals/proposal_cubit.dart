import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:laborhub/features/bussiness_logic/blocs/states.dart';
import 'package:laborhub/features/bussiness_logic/repositories/project_repository.dart';
import 'package:laborhub/features/bussiness_logic/repositories/proposal_repository.dart';
import 'package:laborhub/features/data/dtos/project_dto.dart';
import 'package:laborhub/features/data/dtos/proposal_dto.dart';
import 'package:laborhub/features/data/models/project_model.dart';

class ProposalCubit extends Cubit<LaborHubBlocState> {
  final ProposalRepository _proposalRepository;
  final ProjectRepository _projectRepository;

  ProposalCubit(
      {required ProjectRepository projectRepository,
      required ProposalRepository proposalRepository})
      : _proposalRepository = proposalRepository,
        _projectRepository = projectRepository,
        super(LaborHubInitialState<ProposalDto>()..model = ProposalDto());

  @override
  Future<void> close() {
    // TODO: implement close
    return super.close();
  }

  // void getProposal({required ProposalDto proposalDto}) async {
  //   var savedPreviousState = state;
  //   emit(LaborHubLoadingState());

  //   try {
  //     final model = await _proposalRepository.createProposal(proposalDto);
  //     emit(LaborHubSucceedState(model: model));
  //   } on LaborHubException catch (e) {
  //     emit(LaborHubFailureState(message: e.message));
  //     emit(savedPreviousState);
  //   } catch (e) {
  //     emit(LaborHubFailureState(message: 'Failed to create. Please try again.'));
  //     emit(savedPreviousState);
  //   }
  // }

  Future<ProjectModel> createProject({required ProjectDto projectDto}) async {
    return await _projectRepository.createProject(projectDto);
  }
}
