import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:laborhub/core/errors/exception.dart';
import 'package:laborhub/features/bussiness_logic/blocs/states.dart';
import 'package:laborhub/features/bussiness_logic/repositories/proposal_repository.dart';
import 'package:laborhub/features/data/dtos/proposal_filter_dto.dart';

class ProposalListCubit extends Cubit<LaborHubBlocState> {
  final ProposalRepository _proposalRepository;

  ProposalListCubit({required ProposalRepository proposalRepository})
      : _proposalRepository = proposalRepository,
        super(LaborHubInitialState());

  @override
  Future<void> close() {
    // TODO: implement close
    return super.close();
  }

  void getProposalList({required ProposalFilterDto proposalFilterDto}) async {
    emit(LaborHubLoadingState());
    try {
      final model = await _proposalRepository.getProposals(proposalFilterDto);
      emit(LaborHubSucceedState(model: model));
    } on LaborHubException catch (e) {
      emit(LaborHubFailureState(message: e.message));
    } catch (e) {
      emit(LaborHubFailureState(message: 'Failed to create. Please try again.'));
    }
  }
}
