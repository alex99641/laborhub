import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:laborhub/core/errors/exception.dart';
import 'package:laborhub/features/bussiness_logic/blocs/file_media_cubit.dart';
import 'package:laborhub/features/bussiness_logic/blocs/states.dart';
import 'package:laborhub/features/bussiness_logic/repositories/proposal_repository.dart';
import 'package:laborhub/features/data/dtos/proposal_dto.dart';
import 'package:wechat_assets_picker/wechat_assets_picker.dart';

class CreateProposalCubit extends Cubit<LaborHubBlocState> implements FileMediaCubit {
  final ProposalRepository _proposalRepository;

  CreateProposalCubit({required ProposalRepository proposalRepository})
      : _proposalRepository = proposalRepository,
        super(LaborHubInitialState<ProposalDto>());

  @override
  Future<void> close() {
    // TODO: implement close
    return super.close();
  }

  void createProposal({required ProposalDto proposalDto}) async {
    var savedPreviousState = state;
    emit(LaborHubLoadingState());

    try {
      final model = await _proposalRepository.createProposal(proposalDto);
      emit(LaborHubSucceedState(model: model));
    } on LaborHubException catch (e) {
      emit(LaborHubFailureState(message: e.message));
      emit(savedPreviousState);
    } catch (e) {
      emit(LaborHubFailureState(message: 'Failed to create. Please try again.'));
      emit(savedPreviousState);
    }
  }

  @override
  void setMediaAssets({required List<AssetEntity> media}) async {
    var currentState = state as LaborHubInitialState<ProposalDto>;

    List<File> mediaList = await Future.wait(
      media.map((asset) async {
        File? file = await asset.file;
        return file!;
      }),
    );

    if (currentState.model == null) {
      currentState.model = ProposalDto(media: mediaList);
    } else {
      currentState.model!.files = mediaList;
    }
    emit(currentState.copyWith(model: currentState.model));
  }

  @override
  void setFiles({required List<PlatformFile> files}) async {
    var currentState = state as LaborHubInitialState<ProposalDto>;

    List<File> fileList = files.map((platformFile) {
      File? file = File(platformFile.path!);
      return file;
    }).toList();

    if (currentState.model == null) {
      currentState.model = ProposalDto(files: fileList);
    } else {
      currentState.model!.files = fileList;
    }

    emit(currentState.copyWith(model: currentState.model));
  }
}
