// ignore_for_file: invalid_use_of_visible_for_testing_member

import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:jwt_decoder/jwt_decoder.dart';
import 'package:laborhub/features/bussiness_logic/entities/profile_entity.dart';
import 'package:laborhub/features/bussiness_logic/enums/authentication_status.dart';
import 'package:laborhub/features/bussiness_logic/repositories/authentication_repository.dart';
import 'package:laborhub/features/bussiness_logic/repositories/token_repository.dart';
import 'package:laborhub/features/data/models/tokens_model.dart';

part 'authentication_event.dart';
part 'authentication_state.dart';

class AuthBloc extends Bloc<AuthenticationEvent, AuthenticationState> {
  final AuthenticationRepository _authenticationRepository;
  final TokenRepository _tokenRepository;

  AuthBloc({
    required AuthenticationRepository authenticationRepository,
    required TokenRepository tokenRepository,
  })  : _authenticationRepository = authenticationRepository,
        _tokenRepository = tokenRepository,
        super(UnknownAuthState()) {
    on<AuthenticationLoginEvent>(_onAuthenticationLoginEvent);
    on<AuthenticationLogoutEvent>(_onAuthenticationLogoutEvent);
    _tokenRepository.getAccessToken().then((value) {
      if (value == null) {
        emit(UnauthenticatedState());
      } else {
        emit(AuthenticatedState(ProfileEntity.fromMap(JwtDecoder.decode(value))));
      }
    });
  }

  void _onAuthenticationLoginEvent(
    AuthenticationLoginEvent event,
    Emitter<AuthenticationState> emit,
  ) async {
    var tokens =
        await _authenticationRepository.login(userName: event.userName, password: event.password);
    setAuthorizedByToken(tokens);
  }

  void _onAuthenticationLogoutEvent(
    AuthenticationLogoutEvent event,
    Emitter<AuthenticationState> emit,
  ) {
    // Извлечение access и refresh токенов из хранилища
    final tokenCouple = _tokenRepository.getTokens();
    if (tokenCouple != null) {
      _authenticationRepository.logout(tokenCouple);
      _tokenRepository.removeTokens();
    }

    return emit(UnauthenticatedState());
  }

  void setAuthorizedByToken(TokensModel tokensModel) {
    _tokenRepository.setTokens(tokensModel);
    emit(AuthenticatedState(ProfileEntity.fromMap(JwtDecoder.decode(tokensModel.accessToken))));
  }
}
