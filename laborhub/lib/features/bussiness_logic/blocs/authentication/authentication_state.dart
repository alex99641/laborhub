part of 'authentication_bloc.dart';

abstract class AuthenticationState {}

class AuthenticatedState implements AuthenticationState {
  final ProfileEntity profile;

  AuthenticatedState(this.profile);
}

class UnauthenticatedState implements AuthenticationState {}

class UnknownAuthState implements AuthenticationState {}
