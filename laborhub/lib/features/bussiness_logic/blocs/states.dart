import 'package:file_picker/src/platform_file.dart';

abstract class LaborHubBlocState {}

class LaborHubInitialState<T> implements LaborHubBlocState {
  T? model;
  LaborHubInitialState({
    this.model,
  });

  LaborHubInitialState<T> copyWith({
    T? model,
  }) {
    return LaborHubInitialState<T>(
      model: model ?? this.model,
    );
  }
}

class LaborHubLoadingState implements LaborHubBlocState {}

class LaborHubSucceedState<T> implements LaborHubBlocState {
  T model;
  LaborHubSucceedState({
    required this.model,
  });
}

class LaborHubFailureState implements LaborHubBlocState {
  String message;
  LaborHubFailureState({
    required this.message,
  });
}
