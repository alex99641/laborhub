part of 'create_advertisement_cubit.dart';

abstract class CreateAdvertisementState {}

class CreateAdvertisementInitialState implements CreateAdvertisementState {
  List<PlatformFile>? files;
  List<AssetEntity>? media;

  CreateAdvertisementInitialState({
    this.files,
    this.media,
  });

  attachMedia(List<AssetEntity> assets) {
    media = assets;
  }

  attachFiles(List<PlatformFile> files) {
    files = files;
  }

  CreateAdvertisementInitialState copyWith({
    List<PlatformFile>? files,
    List<AssetEntity>? media,
  }) {
    return CreateAdvertisementInitialState(
      files: files ?? this.files,
      media: media ?? this.media,
    );
  }
}

class CreateAdvertisementLoadingState implements CreateAdvertisementState {}

class CreateAdvertisementSucceedState implements CreateAdvertisementState {
  AdvertisementModel model;
  CreateAdvertisementSucceedState({
    required this.model,
  });
}

class CreateAdvertisementFailureState implements CreateAdvertisementState {
  String message;
  CreateAdvertisementFailureState({
    required this.message,
  });
}
