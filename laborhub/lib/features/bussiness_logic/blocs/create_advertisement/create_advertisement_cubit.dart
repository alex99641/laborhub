import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:laborhub/core/errors/exception.dart';
import 'package:laborhub/features/bussiness_logic/blocs/file_media_cubit.dart';
import 'package:laborhub/features/bussiness_logic/blocs/map_updatable_cubit.dart';
import 'package:laborhub/features/bussiness_logic/blocs/states.dart';
import 'package:laborhub/features/bussiness_logic/repositories/advertisement_repository.dart';
import 'package:laborhub/features/data/dtos/advertisement_dto.dart';
import 'package:laborhub/features/data/models/advertisement_model.dart';
import 'package:wechat_assets_picker/wechat_assets_picker.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

part 'create_advertisement_state.dart';

class CreateAdvertisementCubit extends Cubit<LaborHubBlocState>
    implements FileMediaCubit, MapUpdatableCubit {
  final AdvertisementRepository _advertisementRepository;

  CreateAdvertisementCubit({required AdvertisementRepository advertisementRepository})
      : _advertisementRepository = advertisementRepository,
        super(LaborHubInitialState<AdvertisementDto>());

  @override
  Future<void> close() {
    // TODO: implement close
    return super.close();
  }

  void createAdvertisement({required AdvertisementDto advertisementDto}) async {
    var savedPreviousState = state;
    // var copyState = state as LaborHubInitialState<AdvertisementDto>;
    // if (copyState.media != null && copyState.media!.isNotEmpty) {
    //   List<File> mediaList = await Future.wait(
    //     copyState.media!.map((asset) async {
    //       File? file = await asset.file;
    //       return file!;
    //     }),
    //   );
    //   advertisementDto.media = mediaList;
    // }
    // if (copyState.files != null && copyState.files!.isNotEmpty) {
    //   List<File> fileList = copyState.files!.map((platformFile) {
    //     File? file = File(platformFile.path!);
    //     return file;
    //   }).toList();
    //   advertisementDto.files = fileList;
    // }
    emit(LaborHubLoadingState());

    try {
      final model = await _advertisementRepository.createAdvertisement(advertisementDto);
      emit(LaborHubSucceedState(model: model));
    } on LaborHubException catch (e) {
      emit(LaborHubFailureState(message: e.message));
      emit(savedPreviousState);
    } catch (e) {
      emit(LaborHubFailureState(message: 'Failed to create. Please try again.'));
      emit(savedPreviousState);
    }
  }

  @override
  void setMediaAssets({required List<AssetEntity> media}) async {
    var currentState = state as LaborHubInitialState<AdvertisementDto>;

    List<File> mediaList = await Future.wait(
      media.map((asset) async {
        File? file = await asset.file;
        return file!;
      }),
    );

    if (currentState.model == null) {
      currentState.model = AdvertisementDto(media: mediaList);
    } else {
      currentState.model!.media = mediaList;
    }
    emit(currentState.copyWith(model: currentState.model));
  }

  @override
  void setFiles({required List<PlatformFile> files}) async {
    var currentState = state as LaborHubInitialState<AdvertisementDto>;

    List<File> fileList = files.map((platformFile) {
      File? file = File(platformFile.path!);
      return file;
    }).toList();

    if (currentState.model == null) {
      currentState.model = AdvertisementDto(files: fileList);
    } else {
      currentState.model!.files = fileList;
    }

    emit(currentState.copyWith(model: currentState.model));
  }

  @override
  void setPoint({required Point point}) {
    var currentState = state as LaborHubInitialState<AdvertisementDto>;

    if (currentState.model == null) {
      currentState.model = AdvertisementDto(
        latitude: point.latitude,
        longitude: point.longitude,
      );
    } else {
      currentState.model!.latitude = point.latitude;
      currentState.model!.longitude = point.longitude;
    }

    emit(currentState.copyWith(model: currentState.model));
  }
}
