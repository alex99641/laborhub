part of 'signup_cubit.dart';

abstract class SignUpState {}

class SignUpInitialState implements SignUpState {}

class SignUpLoadingState implements SignUpState {}

class SignUpSucceedState implements SignUpState {
  TokensModel tokens;
  SignUpSucceedState({
    required this.tokens,
  });
}

class SignUpFailureState implements SignUpState {
  String message;
  SignUpFailureState({
    required this.message,
  });
}
