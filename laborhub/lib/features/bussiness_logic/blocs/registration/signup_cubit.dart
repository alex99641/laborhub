import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:laborhub/core/errors/exception.dart';
import 'package:laborhub/features/bussiness_logic/repositories/account_repository.dart';
import 'package:laborhub/features/data/dtos/signup_dto.dart';
import 'package:laborhub/features/data/models/tokens_model.dart';

part 'signup_state.dart';

class SignUpCubit extends Cubit<SignUpState> {
  final AccountRepository _accountRepository;

  SignUpCubit({required AccountRepository accountRepository})
      : _accountRepository = accountRepository,
        super(SignUpInitialState());

  @override
  Future<void> close() {
    // TODO: implement close
    return super.close();
  }

  void register({required SignUpDto signUpDto}) async {
    emit(SignUpLoadingState());
    try {
      final tokens = await _accountRepository.signUp(signUpDto: signUpDto);
      emit(SignUpSucceedState(tokens: tokens));
    } on LaborHubException catch (e) {
      emit(SignUpFailureState(message: e.message));
    } catch (e) {
      emit(SignUpFailureState(message: 'Failed to register. Please try again.'));
    }
  }
}
