
import 'package:file_picker/file_picker.dart';
import 'package:wechat_assets_picker/wechat_assets_picker.dart';

abstract class FileMediaCubit {
  void setMediaAssets({required List<AssetEntity> media});
  void setFiles({required List<PlatformFile> files});
}
