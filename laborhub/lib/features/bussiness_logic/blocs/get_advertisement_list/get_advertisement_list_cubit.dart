import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:laborhub/core/errors/exception.dart';
import 'package:laborhub/features/bussiness_logic/blocs/states.dart';
import 'package:laborhub/features/bussiness_logic/repositories/advertisement_repository.dart';
import 'package:laborhub/features/data/dtos/page_advert_dto.dart';

class LoadAdvertisementListCubit extends Cubit<LaborHubBlocState> {
  final AdvertisementRepository _advertisementRepository;

  LoadAdvertisementListCubit({required AdvertisementRepository advertisementRepository})
      : _advertisementRepository = advertisementRepository,
        super(LaborHubInitialState());

  @override
  Future<void> close() {
    // TODO: implement close
    return super.close();
  }

  void getAdvertisementList({required PageAdvertDto pageAdvertDto}) async {
    emit(LaborHubLoadingState());
    try {
      final model = await _advertisementRepository.getAdvertisements(pageAdvertDto);
      emit(LaborHubSucceedState(model: model));
    } on LaborHubException catch (e) {
      emit(LaborHubFailureState(message: e.message));
    } catch (e) {
      emit(LaborHubFailureState(message: 'Failed to create. Please try again.'));
    }
  }
}
