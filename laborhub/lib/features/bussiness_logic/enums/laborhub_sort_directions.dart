enum LaborHubSortDirections {
  asc(1),
  desc(2);

  const LaborHubSortDirections(this.value);
  final num value;

  static LaborHubSortDirections getByValue(num i) {
    return LaborHubSortDirections.values.firstWhere((x) => x.value == i);
  }
}
