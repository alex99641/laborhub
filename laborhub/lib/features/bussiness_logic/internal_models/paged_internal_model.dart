import 'dart:convert';

import 'package:laborhub/features/data/models/paged_response_model.dart';

class PagedInternalModel<T> {
  List<T> data;
  int total;
  int pageNumber;
  int pageSize;
  int totalReturned;

  PagedInternalModel({
    required this.data,
    required this.totalReturned,
    required this.total,
    required this.pageNumber,
    required this.pageSize,
  });

  @override
  Map<String, dynamic> toMap() {
    return {
      'total': total,
      'pageNumber': pageNumber,
      'pageSize': pageSize,
      'totalReturned': totalReturned,
      'data': data,
    };
  }

  factory PagedInternalModel.fromMap(
      Map<String, dynamic> map, T Function(Map<String, dynamic>) factoryConstructor) {
    return PagedInternalModel<T>(
      total: map['total']?.toInt() ?? 0,
      pageNumber: map['pageNumber']?.toInt() ?? 0,
      pageSize: map['pageSize']?.toInt() ?? 0,
      data: (map['data'] as List).map((entry) => factoryConstructor(entry)).toList(),
      totalReturned: map['totalReturned'],
    );
  }

  String toJson() => json.encode(toMap());

  factory PagedInternalModel.fromJson(String source, factoryConstructor) =>
      PagedInternalModel.fromMap(json.decode(source), factoryConstructor);

  factory PagedInternalModel.fromPagedResponseModel(PagedResponseModel<T> model) {
    return PagedInternalModel<T>(
      total: model.total,
      pageNumber: model.pageNumber,
      pageSize: model.pageSize,
      data: model.data as List<T>,
      totalReturned: model.totalReturned,
    );
  }
}
