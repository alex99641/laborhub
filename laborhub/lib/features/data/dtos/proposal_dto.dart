import 'dart:convert';
import 'dart:io';

import 'package:lil_guid/lil_guid.dart';

class ProposalDto {
  String? title;
  String? description;
  Guid? authorId;
  Guid? advertisementId;
  List<File>? media;
  List<File>? files;

  ProposalDto({
    this.title,
    this.description,
    this.authorId,
    this.advertisementId,
    this.media,
    this.files,
  });

  Map<String, dynamic> toMap() {
    return {
      'title': title,
      'description': description,
      'authorId': authorId,
      'advertisementId': advertisementId,
    };
  }

  factory ProposalDto.fromMap(Map<String, dynamic> map) {
    return ProposalDto(
      title: map['title'] ?? '',
      description: map['description'] ?? '',
      authorId: Guid.parseString(map['authorId'].toString()),
      advertisementId: Guid.parseString(map['advertisementId'].toString()),
      media: map['media'] != null ? List<File>.from(map['media']) : null,
      files: map['files'] != null ? List<File>.from(map['files']) : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory ProposalDto.fromJson(String source) => ProposalDto.fromMap(json.decode(source));

  ProposalDto copyWith({required ProposalDto proposalDto}) {
    return ProposalDto(
      title: proposalDto.title ?? title,
      description: proposalDto.description ?? description,
      authorId: proposalDto.authorId ?? authorId,
      advertisementId: proposalDto.advertisementId ?? advertisementId,
      media: proposalDto.media ?? media,
      files: proposalDto.files ?? files,
    );
  }
}
