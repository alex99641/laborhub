import 'dart:convert';

import 'package:enum_to_string/enum_to_string.dart';
import 'package:laborhub/features/bussiness_logic/enums/laborhub_sort_directions.dart';
import 'package:lil_guid/lil_guid.dart';

class ProjectFilterDto {
  int page;
  String sortProperty;
  LaborHubSortDirections sortDirection;
  String filterString;
  int pageSize;
  Guid? advertisementId;
  Guid? proposalId;
  Guid? clientId;
  Guid? masterId;
  DateTime? startDate;
  DateTime? endDate;

  ProjectFilterDto({
    required this.page,
    this.sortProperty = '',
    this.sortDirection = LaborHubSortDirections.desc,
    this.filterString = '',
    required this.pageSize,
    this.advertisementId,
    this.proposalId,
    this.clientId,
    this.masterId,
    this.startDate,
    this.endDate,
  });

  Map<String, dynamic> toMap() {
    return {
      'page': page,
      'sortProperty': sortProperty,
      'sortDirection': EnumToString.convertToString(sortDirection),
      'filterString': filterString,
      'pageSize': pageSize,
      'advertisementId': advertisementId?.toString(),
      'proposalId': proposalId?.toString(),
      'clientId': clientId?.toString(),
      'masterId': masterId?.toString(),
      'startDate': startDate?.toIso8601String(),
      'endDate': endDate?.toIso8601String(),
    };
  }

  factory ProjectFilterDto.fromMap(Map<String, dynamic> map) {
    return ProjectFilterDto(
      page: map['page']?.toInt() ?? 0,
      sortProperty: map['sortProperty'] ?? '',
      sortDirection: EnumToString.fromString(LaborHubSortDirections.values, map['sortDirection']) ??
          LaborHubSortDirections.desc,
      filterString: map['filterString'] ?? '',
      pageSize: map['pageSize']?.toInt() ?? 0,
      advertisementId:
          map['advertisementId'] != null ? Guid.parseString(map['advertisementId']) : null,
      proposalId: map['proposalId'] != null ? Guid.parseString(map['proposalId']) : null,
      clientId: map['clientId'] != null ? Guid.parseString(map['clientId']) : null,
      masterId: map['masterId'] != null ? Guid.parseString(map['masterId']) : null,
      startDate: map['startDate'] != null ? DateTime.parse(map['startDate']) : null,
      endDate: map['endDate'] != null ? DateTime.parse(map['endDate']) : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory ProjectFilterDto.fromJson(String source) => ProjectFilterDto.fromMap(json.decode(source));

  factory ProjectFilterDto.defaultModel({int? page}) {
    return ProjectFilterDto(
      page: page ?? 1,
      sortProperty: '',
      sortDirection: LaborHubSortDirections.desc,
      filterString: '',
      pageSize: 10,
      advertisementId: null,
      proposalId: null,
      clientId: null,
      masterId: null,
      startDate: null,
      endDate: null,
    );
  }
}
