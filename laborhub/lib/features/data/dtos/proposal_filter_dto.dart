import 'dart:convert';

import 'package:enum_to_string/enum_to_string.dart';
import 'package:laborhub/features/bussiness_logic/enums/laborhub_sort_directions.dart';
import 'package:lil_guid/lil_guid.dart';

class ProposalFilterDto {
  int page;
  String sortProperty;
  LaborHubSortDirections sortDirection;
  String filterString;
  int pageSize;
  Guid? advertisementId;
  Guid? clientId;
  Guid? masterId;
  DateTime? startDate;
  DateTime? endDate;

  ProposalFilterDto({
    required this.page,
    this.sortProperty = '',
    this.sortDirection = LaborHubSortDirections.desc,
    this.filterString = '',
    required this.pageSize,
    this.advertisementId,
    this.clientId,
    this.masterId,
    this.startDate,
    this.endDate,
  });

  Map<String, dynamic> toMap() {
    return {
      'page': page,
      'sortProperty': sortProperty,
      'sortDirection': sortDirection.value,
      'filterString': filterString,
      'pageSize': pageSize,
      'advertisementId': advertisementId,
      'clientId': clientId,
      'masterId': masterId,
      'startDate': startDate?.toIso8601String(),
      'endDate': endDate?.toIso8601String(),
    };
  }

  factory ProposalFilterDto.fromMap(Map<String, dynamic> map) {
    return ProposalFilterDto(
      page: map['page']?.toInt() ?? 0,
      sortProperty: map['sortProperty'] ?? '',
      sortDirection: LaborHubSortDirections.getByValue(map['sortDirection']),
      filterString: map['filterString'] ?? '',
      pageSize: map['pageSize']?.toInt() ?? 0,
      advertisementId:
          map['advertisementId'] != null ? Guid.parseString(map['advertisementId']) : null,
      clientId: map['clientId'] != null ? Guid.parseString(map['clientId']) : null,
      masterId: map['masterId'] != null ? Guid.parseString(map['masterId']) : null,
      startDate:
          map['startDate'] != null ? DateTime.fromMillisecondsSinceEpoch(map['startDate']) : null,
      endDate: map['endDate'] != null ? DateTime.fromMillisecondsSinceEpoch(map['endDate']) : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory ProposalFilterDto.fromJson(String source) =>
      ProposalFilterDto.fromMap(json.decode(source));

  factory ProposalFilterDto.defaultModel({int? page}) {
    return ProposalFilterDto(
      page: page ?? 1,
      sortProperty: '',
      sortDirection: LaborHubSortDirections.desc,
      filterString: '',
      pageSize: 10,
      advertisementId: null,
      clientId: null,
      masterId: null,
      startDate: null,
      endDate: null,
    );
  }
}
