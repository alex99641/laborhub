import 'dart:convert';
import 'dart:io';

import 'package:lil_guid/lil_guid.dart';

class AdvertisementDto {
  String? title;
  String? description;
  double? latitude;
  double? longitude;
  Guid? authorId;
  List<File>? media;
  List<File>? files;

  AdvertisementDto({
    this.title,
    this.description,
    this.latitude,
    this.longitude,
    this.authorId,
    this.media,
    this.files,
  });

  AdvertisementDto copyWith({required AdvertisementDto advertisementDto}) {
    return AdvertisementDto(
      title: advertisementDto.title ?? title,
      description: advertisementDto.description ?? description,
      latitude: advertisementDto.latitude ?? latitude,
      longitude: advertisementDto.longitude ?? longitude,
      authorId: advertisementDto.authorId ?? authorId,
      media: advertisementDto.media ?? media,
      files: advertisementDto.files ?? files,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'title': title,
      'description': description,
      'latitude': latitude.toString(),
      'longitude': longitude.toString(),
      'authorId': authorId
      // 'media': media?.toList(),
      // 'files': files?.map((x) => x).toList(),
    };
  }

  factory AdvertisementDto.fromMap(Map<String, dynamic> map) {
    return AdvertisementDto(
      title: map['title'],
      description: map['description'],
      latitude: map['latitude']?.toDouble(),
      longitude: map['longitude']?.toDouble(),
      authorId: map['authorId'] != null ? Guid.tryParseString(map['authorId']) : null,
      media: map['media'] != null ? List<File>.from(map['media']) : null,
      files: map['files'] != null ? List<File>.from(map['files']) : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory AdvertisementDto.fromJson(String source) => AdvertisementDto.fromMap(json.decode(source));
}
