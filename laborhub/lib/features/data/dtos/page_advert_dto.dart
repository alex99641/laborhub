import 'dart:convert';

import 'package:enum_to_string/enum_to_string.dart';
import 'package:laborhub/features/bussiness_logic/enums/laborhub_sort_directions.dart';

class PageAdvertDto {
  int page;
  String sortProperty;
  LaborHubSortDirections sortDirection;
  String filterString;
  int pageSize;
  double? latitude;
  double? longitude;
  DateTime? startDate;
  DateTime? endDate;

  PageAdvertDto({
    required this.page,
    this.sortProperty = '',
    this.sortDirection = LaborHubSortDirections.desc,
    this.filterString = '',
    required this.pageSize,
    this.latitude,
    this.longitude,
    this.startDate,
    this.endDate,
  });

  Map<String, dynamic> toMap() {
    return {
      'page': page,
      'sortProperty': sortProperty,
      'sortDirection': sortDirection.value,
      'filterString': filterString,
      'pageSize': pageSize,
      'latitude': latitude,
      'longitude': longitude,
      'startDate': startDate?.toIso8601String(),
      'endDate': endDate?.toIso8601String(),
    };
  }

  factory PageAdvertDto.fromMap(Map<String, dynamic> map) {
    return PageAdvertDto(
      page: map['page']?.toInt() ?? 0,
      sortProperty: map['sortProperty'] ?? '',
      sortDirection: LaborHubSortDirections.getByValue(map['sortDirection']),
      filterString: map['filterString'] ?? '',
      pageSize: map['pageSize']?.toInt() ?? 0,
      latitude: map['latitude']?.toDouble(),
      longitude: map['longitude']?.toDouble(),
      startDate:
          map['startDate'] != null ? DateTime.fromMillisecondsSinceEpoch(map['startDate']) : null,
      endDate: map['endDate'] != null ? DateTime.fromMillisecondsSinceEpoch(map['endDate']) : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory PageAdvertDto.fromJson(String source) => PageAdvertDto.fromMap(json.decode(source));

  factory PageAdvertDto.defaultModel({int? page}) {
    return PageAdvertDto(
      page: page ?? 1,
      sortProperty: '',
      sortDirection: LaborHubSortDirections.desc,
      filterString: '',
      pageSize: 10,
      latitude: 0,
      longitude: 0,
      startDate: null,
      endDate: null,
    );
  }
}
