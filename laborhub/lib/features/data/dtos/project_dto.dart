import 'dart:convert';
import 'dart:io';

import 'package:enum_to_string/enum_to_string.dart';
import 'package:laborhub/features/bussiness_logic/enums/project_status.dart';
import 'package:lil_guid/lil_guid.dart';

class ProjectDto {
  String? title;
  String? description;
  ProjectStatus? projectStatus;
  Guid? advertisementId;
  Guid? proposalId;
  Guid? masterId;
  Guid? clientId;
  List<File>? media;
  List<File>? files;

  ProjectDto({
    required this.title,
    required this.description,
    this.projectStatus,
    this.advertisementId,
    this.proposalId,
    this.clientId,
    this.masterId,
    this.media,
    this.files,
  });

  Map<String, dynamic> toMap() {
    return {
      'title': title,
      'description': description,
      'projectStatus': projectStatus?.index,
      'advertisementId': advertisementId,
      'proposalId': proposalId,
      'masterId': masterId,
      'clientId': clientId,
    };
  }

  factory ProjectDto.fromMap(Map<String, dynamic> map) {
    return ProjectDto(
      title: map['title'] ?? '',
      description: map['description'] ?? '',
      projectStatus: map['projectStatus'] != null
          ? EnumToString.fromString(ProjectStatus.values, map['projectStatus'])
          : null,
      advertisementId:
          map['advertisementId'] != null ? Guid.parseString(map['advertisementId']) : null,
      proposalId: map['proposalId'] != null ? Guid.parseString(map['proposalId']) : null,
      clientId: map['clientId'] != null ? Guid.parseString(map['clientId']) : null,
      masterId: map['masterId'] != null ? Guid.parseString(map['masterId']) : null,
      media: map['media'] != null ? List<File>.from(map['media']) : null,
      files: map['files'] != null ? List<File>.from(map['files']) : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory ProjectDto.fromJson(String source) => ProjectDto.fromMap(json.decode(source));
}
