import 'dart:convert';

import 'package:laborhub/features/bussiness_logic/enums/roles.dart';

class SignUpDto {
  String userName;
  String email;
  String phoneNumber;
  String password;
  String passwordConfirm;
  String firstName;
  String lastName;
  Roles role;
  DateTime birthday;

  SignUpDto({
    required this.userName,
    required this.email,
    required this.phoneNumber,
    required this.password,
    required this.passwordConfirm,
    required this.firstName,
    required this.lastName,
    required this.birthday,
    required this.role,
  });

  Map<String, dynamic> toMap() {
    return {
      'userName': userName,
      'email': email,
      'phoneNumber': phoneNumber,
      'password': password,
      'passwordConfirm': passwordConfirm,
      'firstName': firstName,
      'lastName': lastName,
      'role': role.name,
      'birthday': birthday.toIso8601String(),
    };
  }

  factory SignUpDto.fromMap(Map<String, dynamic> map) {
    return SignUpDto(
      userName: map['userName'] ?? '',
      email: map['email'] ?? '',
      phoneNumber: map['phoneNumber'] ?? '',
      password: map['password'] ?? '',
      passwordConfirm: map['passwordConfirm'] ?? '',
      firstName: map['firstName'] ?? '',
      lastName: map['lastName'] ?? '',
      role: map['role'],
      birthday: map['birthday'].toUtc(),
    );
  }

  String toJson() => json.encode(toMap());

  factory SignUpDto.fromJson(String source) => SignUpDto.fromMap(json.decode(source));
}
