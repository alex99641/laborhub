import 'dart:convert';

import 'package:laborhub/features/data/models/collection_response_model.dart';

class PagedResponseModel<T> extends CollectionResponseModel<T> {
  int total;
  int pageNumber;
  int pageSize;

  PagedResponseModel({
    required List<T> data,
    required int status,
    required String userMessage,
    required String systemMessage,
    required String stackTrace,
    required DateTime generatedAtUtc,
    required int totalReturned,
    required this.total,
    required this.pageNumber,
    required this.pageSize,
  }) : super(
            data: data,
            status: status,
            userMessage: userMessage,
            systemMessage: systemMessage,
            stackTrace: stackTrace,
            generatedAtUtc: generatedAtUtc,
            totalReturned: totalReturned);

  @override
  Map<String, dynamic> toMap() {
    return {
      'total': total,
      'pageNumber': pageNumber,
      'pageSize': pageSize,
      'totalReturned': totalReturned,
      'data': data,
      'status': status,
      'userMessage': userMessage,
      'systemMessage': systemMessage,
      'stackTrace': stackTrace,
      'generatedAtUtc': generatedAtUtc.toIso8601String(),
    };
  }

  factory PagedResponseModel.fromMap(
      Map<String, dynamic> map, T Function(Map<String, dynamic>) factoryConstructor) {
    return PagedResponseModel<T>(
      total: map['total']?.toInt() ?? 0,
      pageNumber: map['pageNumber']?.toInt() ?? 0,
      pageSize: map['pageSize']?.toInt() ?? 0,
      data: (map['data'] as List).map((entry) => factoryConstructor(entry)).toList(),
      status: map['status']?.toInt() ?? 0,
      userMessage: map['userMessage'] ?? '',
      systemMessage: map['systemMessage'] ?? '',
      stackTrace: map['stackTrace'] ?? '',
      generatedAtUtc: DateTime.parse(map['generatedAtUtc']),
      totalReturned: map['totalReturned'],
    );
  }

  @override
  String toJson() => json.encode(toMap());

  factory PagedResponseModel.fromJson(String source, factoryConstructor) =>
      PagedResponseModel.fromMap(json.decode(source), factoryConstructor);
}
