abstract class FileMediaModel {
  List<String> getMedias();
  List<String> getFiles();
}
