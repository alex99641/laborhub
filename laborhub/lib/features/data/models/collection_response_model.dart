import 'dart:convert';

import 'package:laborhub/features/data/models/response_model.dart';

class CollectionResponseModel<T> extends ResponseModel {
  int totalReturned;

  CollectionResponseModel({
    required List<T> data,
    required int status,
    required String userMessage,
    required String systemMessage,
    required String stackTrace,
    required DateTime generatedAtUtc,
    required this.totalReturned,
  }) : super(
          data: data,
          status: status,
          userMessage: userMessage,
          systemMessage: systemMessage,
          stackTrace: stackTrace,
          generatedAtUtc: generatedAtUtc,
        );

  @override
  Map<String, dynamic> toMap() {
    return {
      'totalReturned': totalReturned,
      'data': data,
      'status': status,
      'userMessage': userMessage,
      'systemMessage': systemMessage,
      'stackTrace': stackTrace,
      'generatedAtUtc': generatedAtUtc.toIso8601String(),
    };
  }

  factory CollectionResponseModel.fromMap(
      Map<String, dynamic> map, T Function(Map<String, dynamic>) factoryConstructor) {
    return CollectionResponseModel<T>(
      data: (map['data'] as List).map((entry) => factoryConstructor(entry)).toList(),
      status: map['status']?.toInt() ?? 0,
      userMessage: map['userMessage'] ?? '',
      systemMessage: map['systemMessage'] ?? '',
      stackTrace: map['stackTrace'] ?? '',
      generatedAtUtc: DateTime.parse(map['generatedAtUtc']),
      totalReturned: map['totalReturned'],
    );
  }

  @override
  String toJson() => json.encode(toMap());

  factory CollectionResponseModel.fromJson(String source, factoryConstructor) =>
      CollectionResponseModel.fromMap(json.decode(source), factoryConstructor);
}
