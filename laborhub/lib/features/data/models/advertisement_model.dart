import 'dart:convert';

import 'package:laborhub/features/data/models/file_media_model.dart';
import 'package:laborhub/features/data/models/user_model.dart';

class AdvertisementModel implements FileMediaModel {
  String id;
  String title;
  String description;
  double latitude;
  double longitude;
  List<String> medias;
  List<String> files;
  UserModel author;
  DateTime createdDate;

  AdvertisementModel({
    required this.id,
    required this.title,
    required this.description,
    required this.latitude,
    required this.longitude,
    required this.medias,
    required this.files,
    required this.author,
    required this.createdDate,
  });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'title': title,
      'description': description,
      'latitude': latitude,
      'longitude': longitude,
      'medias': medias,
      'files': files,
      'author': author.toMap(),
      'createdDate': createdDate.toIso8601String(),
    };
  }

  factory AdvertisementModel.fromMap(Map<String, dynamic> map) {
    return AdvertisementModel(
      id: map['id'] ?? '',
      title: map['title'] ?? '',
      description: map['description'] ?? '',
      latitude: map['latitude']?.toDouble() ?? 0.0,
      longitude: map['longitude']?.toDouble() ?? 0.0,
      medias: map['medias'] != null ? List<String>.from(map['medias']) : [],
      files: map['files'] != null ? List<String>.from(map['files']) : [],
      author: UserModel.fromMap(map['author']),
      createdDate: DateTime.parse(map['createdDate']),
    );
  }

  String toJson() => json.encode(toMap());

  factory AdvertisementModel.fromJson(String source) =>
      AdvertisementModel.fromMap(json.decode(source));

  @override
  List<String> getFiles() {
    return files;
  }

  @override
  List<String> getMedias() {
    return medias;
  }
}
