import 'dart:convert';

import 'package:laborhub/features/data/models/file_media_model.dart';
import 'package:laborhub/features/data/models/user_model.dart';
import 'package:laborhub/features/data/models/advertisement_model.dart';
import 'package:lil_guid/lil_guid.dart';

class ProposalModel implements FileMediaModel {
  Guid id;
  String title;
  String description;
  String proposalStatus;
  List<String> medias;
  List<String> files;
  UserModel author;
  AdvertisementModel advertisement;

  ProposalModel({
    required this.id,
    required this.title,
    required this.description,
    required this.proposalStatus,
    required this.medias,
    required this.files,
    required this.author,
    required this.advertisement,
  });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'title': title,
      'description': description,
      'proposalStatus': proposalStatus,
      'medias': medias,
      'files': files,
      'author': author.toMap(),
      'advertisement': advertisement.toMap(),
    };
  }

  factory ProposalModel.fromMap(Map<String, dynamic> map) {
    print(map);
    return ProposalModel(
      id: Guid.parseString(map['id']),
      title: map['title'] ?? '',
      description: map['description'] ?? '',
      proposalStatus: map['proposalStatus'] ?? '',
      medias: map['medias'] != null ? List<String>.from(map['medias']) : [],
      files: map['files'] != null ? List<String>.from(map['files']) : [],
      author: UserModel.fromMap(map['author']),
      advertisement: AdvertisementModel.fromMap(map['advertisement']),
    );
  }

  String toJson() => json.encode(toMap());

  factory ProposalModel.fromJson(String source) => ProposalModel.fromMap(json.decode(source));

  @override
  List<String> getFiles() {
    return files;
  }

  @override
  List<String> getMedias() {
    return medias;
  }
}
