import 'dart:convert';

import 'package:enum_to_string/enum_to_string.dart';
import 'package:laborhub/features/bussiness_logic/enums/project_status.dart';
import 'package:laborhub/features/data/models/advertisement_model.dart';
import 'package:laborhub/features/data/models/file_media_model.dart';
import 'package:laborhub/features/data/models/proposal_model.dart';
import 'package:laborhub/features/data/models/user_model.dart';
import 'package:lil_guid/lil_guid.dart';

class ProjectModel implements FileMediaModel {
  Guid id;
  String title;
  String description;
  ProjectStatus projectStatus;
  List<String> medias;
  List<String> files;
  UserModel master;
  UserModel client;
  AdvertisementModel advertisement;
  ProposalModel proposal;

  ProjectModel({
    required this.id,
    required this.title,
    required this.description,
    required this.projectStatus,
    required this.medias,
    required this.files,
    required this.master,
    required this.client,
    required this.advertisement,
    required this.proposal,
  });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'title': title,
      'description': description,
      'projectStatus': projectStatus.toString(),
      'medias': medias,
      'files': files,
      'master': master.toMap(),
      'client': client.toMap(),
      'advertisement': advertisement.toMap(),
      'proposal': proposal.toMap(),
    };
  }

  factory ProjectModel.fromMap(Map<String, dynamic> map) {
    return ProjectModel(
      id: Guid.parseString(map['id'].toString()),
      title: map['title'] ?? '',
      description: map['description'] ?? '',
      projectStatus: ProjectStatus.values[map['projectStatus']],
      medias: map['medias'] != null ? List<String>.from(map['medias']) : [],
      files: map['files'] != null ? List<String>.from(map['files']) : [],
      master: UserModel.fromMap(map['master']),
      client: UserModel.fromMap(map['client']),
      advertisement: AdvertisementModel.fromMap(map['advertisement']),
      proposal: ProposalModel.fromMap(map['proposal']),
    );
  }

  String toJson() => json.encode(toMap());

  factory ProjectModel.fromJson(String source) => ProjectModel.fromMap(json.decode(source));

  @override
  List<String> getFiles() {
    return files;
  }

  @override
  List<String> getMedias() {
    return medias;
  }
}
