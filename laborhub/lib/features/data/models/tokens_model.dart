import 'dart:convert';

class TokensModel {
  String accessToken;
  String refreshToken;

  TokensModel({required this.accessToken, required this.refreshToken});

  Map<String, dynamic> toMap() {
    return {
      'accessToken': accessToken,
      'refreshToken': refreshToken,
    };
  }

  factory TokensModel.fromMap(Map<String, dynamic> map) {
    return TokensModel(
      accessToken: map['accessToken'] ?? '',
      refreshToken: map['refreshToken'] ?? '',
    );
  }

  String toJson() => json.encode(toMap());

  factory TokensModel.fromJson(String source) => TokensModel.fromMap(json.decode(source));
}
