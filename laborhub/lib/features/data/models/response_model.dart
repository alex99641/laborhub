import 'dart:convert';

class ResponseModel<T> {
  T data;
  int status;
  String userMessage;
  String systemMessage;
  String stackTrace;
  DateTime generatedAtUtc;
  ResponseModel({
    required this.data,
    required this.status,
    required this.userMessage,
    required this.systemMessage,
    required this.stackTrace,
    required this.generatedAtUtc,
  });

  Map<String, dynamic> toMap() {
    return {
      'data': data,
      'status': status,
      'userMessage': userMessage,
      'systemMessage': systemMessage,
      'stackTrace': stackTrace,
      'generatedAtUtc': generatedAtUtc.toIso8601String(),
    };
  }

  factory ResponseModel.fromMap(Map<String, dynamic> map) {
    return ResponseModel<T>(
      data: map['data'],
      status: map['status']?.toInt() ?? 0,
      userMessage: map['userMessage'] ?? '',
      systemMessage: map['systemMessage'] ?? '',
      stackTrace: map['stackTrace'] ?? '',
      generatedAtUtc: DateTime.parse(map['generatedAtUtc']),
    );
  }

  String toJson() => json.encode(toMap());

  factory ResponseModel.fromJson(String source) => ResponseModel.fromMap(json.decode(source));
}
