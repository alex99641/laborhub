import 'package:dio/dio.dart';
import 'package:laborhub/core/errors/exception.dart';
import 'package:laborhub/core/services/base_datasource.dart';
import 'package:laborhub/features/bussiness_logic/internal_models/paged_internal_model.dart';
import 'package:laborhub/features/data/dtos/project_dto.dart';
import 'package:laborhub/features/data/dtos/project_filter_dto.dart';
import 'package:laborhub/features/data/models/paged_response_model.dart';
import 'package:laborhub/features/data/models/project_model.dart';
import 'package:laborhub/features/data/models/response_model.dart';

class ProjectDatasource extends BaseDatasource {
  ProjectDatasource();

  Future<PagedInternalModel<ProjectModel>> getProjects(ProjectFilterDto filterDto) async {
    final response = await post('/api/project/page', body: filterDto.toJson());

    if (response.statusCode == 200) {
      return PagedInternalModel.fromPagedResponseModel(
          PagedResponseModel<ProjectModel>.fromMap(response.data, ProjectModel.fromMap));
    } else {
      throw ServerException('${response.statusCode} ${response.statusMessage}');
    }
  }

  Future<ProjectModel> createProject(ProjectDto project) async {
    var filePaths = project.files?.map((e) => e.path).toList();
    var mediaPaths = project.media?.map((e) => e.path).toList();

    List<MultipartFile> files = [];
    List<MultipartFile> media = [];

    if (filePaths != null && filePaths.isNotEmpty) {
      for (String filePath in filePaths) {
        String fileName = filePath.split('/').last;
        files.add(await MultipartFile.fromFile(filePath, filename: fileName));
      }
    }

    if (mediaPaths != null && mediaPaths.isNotEmpty) {
      for (String mediaPath in mediaPaths) {
        String fileName = mediaPath.split('/').last;
        media.add(await MultipartFile.fromFile(mediaPath, filename: fileName));
      }
    }

    FormData formData = FormData.fromMap({'Files': files, 'Media': media});

    final response = await post(
      '/api/project',
      body: formData,
      queryParameters: project.toMap(),
    );

    var responseModel = ResponseModel.fromMap(response.data);

    if (response.statusCode == 200) {
      return ProjectModel.fromMap(responseModel.data);
    } else {
      throw ServerException(
          '${response.statusCode} ${response.statusMessage} ${responseModel.userMessage}');
    }
  }

  Future<ProjectModel> updateProject(String id, ProjectDto project) async {
    final response = await put(
      '/api/project/$id',
      body: project.toMap(),
    );

    var responseModel = ResponseModel.fromMap(response.data);

    if (response.statusCode == 200) {
      return ProjectModel.fromMap(responseModel.data);
    } else {
      throw ServerException(
          '${response.statusCode} ${response.statusMessage} ${responseModel.userMessage}');
    }
  }

  Future<bool> deleteProject(String id) async {
    final response = await delete('/api/project/$id');
    var responseModel = ResponseModel.fromMap(response.data);

    if (response.statusCode == 200) {
      return responseModel.data as bool;
    } else {
      throw ServerException(
          '${response.statusCode} ${response.statusMessage} ${responseModel.userMessage}');
    }
  }
}
