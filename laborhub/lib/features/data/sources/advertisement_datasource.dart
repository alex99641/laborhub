import 'package:dio/dio.dart';
import 'package:laborhub/core/errors/exception.dart';
import 'package:laborhub/core/services/base_datasource.dart';
import 'package:laborhub/features/bussiness_logic/internal_models/paged_internal_model.dart';
import 'package:laborhub/features/data/dtos/advertisement_dto.dart';
import 'package:laborhub/features/data/dtos/page_advert_dto.dart';
import 'package:laborhub/features/data/models/advertisement_model.dart';
import 'package:laborhub/features/data/models/paged_response_model.dart';
import 'package:laborhub/features/data/models/response_model.dart';

class AdvertisementDatasource extends BaseDatasource {
  AdvertisementDatasource();

  Future<PagedInternalModel<AdvertisementModel>> getAdvertisements(
      PageAdvertDto pageAdvertDto) async {
    final response = await post('/api/advertisement/page/', body: pageAdvertDto.toJson());

    if (response.statusCode == 200) {
      return PagedInternalModel.fromPagedResponseModel(
          PagedResponseModel<AdvertisementModel>.fromMap(
              response.data, AdvertisementModel.fromMap));
    } else {
      throw ServerException('${response.statusCode} ${response.statusMessage}');
    }
  }

  Future<AdvertisementModel> createAdvertisement(AdvertisementDto advertisement) async {
    var filePaths = advertisement.files?.map((e) => e.path).toList();
    var mediaPaths = advertisement.media?.map((e) => e.path).toList();

    List<MultipartFile> files = [];
    List<MultipartFile> media = [];

    if (filePaths != null && filePaths.isNotEmpty) {
      for (String filePath in filePaths) {
        String fileName = filePath.split('/').last;
        files.add(await MultipartFile.fromFile(filePath, filename: fileName));
      }
    }

    if (mediaPaths != null && mediaPaths.isNotEmpty) {
      for (String mediaPath in mediaPaths) {
        String fileName = mediaPath.split('/').last;
        media.add(await MultipartFile.fromFile(mediaPath, filename: fileName));
      }
    }

    FormData formData = FormData.fromMap({'Files': files, 'Media': media});

    final response =
        await post('/api/advertisement/', body: formData, queryParameters: advertisement.toMap());
    var responseModel = ResponseModel.fromMap(response.data);

    if (response.statusCode == 200) {
      return AdvertisementModel.fromMap(responseModel.data);
    } else {
      throw ServerException(
          '${response.statusCode} ${response.statusMessage} ${responseModel.userMessage}');
    }
  }

  Future<AdvertisementModel> updateAdvertisement(String id, AdvertisementDto advertisement) async {
    final response =
        await put('/api/advertisement/', body: advertisement.toMap(), queryParameters: {'id': id});
    var responseModel = ResponseModel.fromMap(response.data);

    if (response.statusCode == 200) {
      return AdvertisementModel.fromMap(responseModel.data);
    } else {
      throw ServerException(
          '${response.statusCode} ${response.statusMessage} ${responseModel.userMessage}');
    }
  }

  Future<bool> deleteAdvertisement(String id) async {
    final response = await delete('/api/advertisement/', queryParameters: {'id': id});
    var responseModel = ResponseModel.fromMap(response.data);

    if (response.statusCode == 200) {
      return responseModel.data as bool;
    } else {
      throw ServerException(
          '${response.statusCode} ${response.statusMessage} ${responseModel.userMessage}');
    }
  }
}
