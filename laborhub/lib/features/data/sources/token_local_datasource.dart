import 'package:get_storage/get_storage.dart';
import 'package:jwt_decoder/jwt_decoder.dart';
import 'package:laborhub/core/services/locator_service.dart';
import 'package:laborhub/features/data/models/tokens_model.dart';
import 'package:laborhub/features/data/sources/auth_datasource.dart';

class TokenLocalDatasource {
  final _storage = GetStorage();

  // Установка access token и refresh token
  void setTokens(TokensModel tokensModel) {
    _storage.write('accessToken', tokensModel.accessToken);
    _storage.write('refreshToken', tokensModel.refreshToken);
  }

  // Получение текущих токенов из памяти
  TokensModel? getTokens() {
    var accessToken = _storage.read('accessToken');
    var refreshToken = _storage.read('refreshToken');
    return (accessToken == null || refreshToken == null)
        ? null
        : TokensModel(accessToken: accessToken, refreshToken: refreshToken);
  }

  // Получение валидного access token
  Future<String?> getAccessToken() async {
    var currentTokens = getTokens();
    if (currentTokens == null) {
      return null;
    }
    if (JwtDecoder.isExpired(currentTokens.accessToken)) {
      var newTokens = await _refreshToken(currentTokens);
      return newTokens.accessToken;
    }
    return currentTokens.accessToken;
  }

  // Метод для выполнения запроса на получение обновленных токенов
  Future<TokensModel> _refreshToken(TokensModel oldTokens) async {
    var newTokens = await _callRefreshTokenApi(oldTokens);
    setTokens(newTokens);
    return newTokens;
  }

  // Метод для вызова API для обновления токена
  Future<TokensModel> _callRefreshTokenApi(TokensModel tokensModel) async {
    final authDatasource = sl<AuthenticationDatasource>();
    return await authDatasource.refresh(tokensModel);
  }

  // Установка access token и refresh token
  void removeTokens() {
    _storage.remove('accessToken');
    _storage.remove('refreshToken');
  }
}
