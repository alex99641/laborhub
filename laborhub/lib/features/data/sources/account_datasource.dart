import 'package:laborhub/core/errors/exception.dart';
import 'package:laborhub/core/services/base_datasource.dart';
import 'package:laborhub/features/data/dtos/signup_dto.dart';
import 'package:laborhub/features/data/models/response_model.dart';
import 'package:laborhub/features/data/models/tokens_model.dart';

class AccountDatasource extends BaseDatasource {
  AccountDatasource();

  // Метод для выполнения запроса на авторизацию
  Future<TokensModel> signUp(SignUpDto signUpDto) async {
    try {
      final response = await post('/api/account/', body: signUpDto.toJson());
      var responseModel = ResponseModel.fromMap(response.data);
      // final userData = response.data as Map<String, dynamic>;
      // final user = UserModel.fromJson(userData);
      if (response.statusCode == 200) {
        return TokensModel.fromMap(responseModel.data);
      } else {
        throw ServerException(
            '${response.statusCode} ${response.statusMessage} ${responseModel.userMessage}');
      }
    } catch (error) {
      // Обработка ошибок
      throw Exception('Ошибка при регистрации: $error');
    }
  }
}
