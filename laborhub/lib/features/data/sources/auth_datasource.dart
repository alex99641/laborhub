import 'dart:convert';

import 'package:laborhub/core/errors/exception.dart';
import 'package:laborhub/core/services/base_datasource.dart';
import 'package:laborhub/features/data/models/response_model.dart';
import 'package:laborhub/features/data/models/tokens_model.dart';

class AuthenticationDatasource extends BaseDatasource {
  AuthenticationDatasource();

  // Метод для выполнения запроса на авторизацию
  Future<TokensModel> login(String userName, String password) async {
    try {
      final response =
          await post('/api/session/', body: {'userName': userName, 'password': password});
      var responseModel = ResponseModel.fromMap(response.data);
      // final userData = response.data as Map<String, dynamic>;
      // final user = UserModel.fromJson(userData);
      if (response.statusCode == 200) {
        return TokensModel.fromMap(responseModel.data);
      } else {
        throw ServerException(
            '${response.statusCode} ${response.statusMessage} ${responseModel.userMessage}');
      }
    } catch (error) {
      // Обработка ошибок
      throw Exception('Ошибка при авторизации: $error');
    }
  }

  Future<void> logout(TokensModel tokenCouple) async {
    try {
      final data = {
        'accessToken': tokenCouple.accessToken,
        'refreshToken': tokenCouple.refreshToken
      };
      await post('/api/session/logout', body: json.encode(data));
    } catch (error) {
      // Обработка ошибок
      throw Exception('Ошибка при выходе из аккаунта: $error');
    }
  }

  Future<TokensModel> refresh(TokensModel oldTokens) async {
    final response = await post('/api/session/refresh', body: oldTokens.toJson());
    var responseModel = ResponseModel.fromMap(response.data);
    if (response.statusCode == 200) {
      return TokensModel.fromMap(responseModel.data);
    } else {
      throw ServerException(
          '${response.statusCode} ${response.statusMessage} ${responseModel.userMessage}');
    }
  }
}
