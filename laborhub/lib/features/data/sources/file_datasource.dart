import 'dart:io';

import 'package:dio/dio.dart';
import 'package:laborhub/core/errors/exception.dart';
import 'package:laborhub/core/services/base_datasource.dart';
import 'package:path_provider/path_provider.dart';

class FileDatasource extends BaseDatasource {
  FileDatasource();

  Future<void> loadFile(String name) async {
    enableAuthInterceptor = true;

    final response = await get(
      '/api/file/$name',
      options: Options(responseType: ResponseType.bytes),
    );

    if (response.statusCode == 200) {
      final bytes = response.data;
      Directory directory = await getApplicationDocumentsDirectory();
      // Directory directory = Platform.isAndroid
      //     ? (await getExternalStorageDirectory())! //FOR ANDROID
      //     : await getApplicationSupportDirectory(); //FOR iOS
      var file = File('${directory.absolute.path}/$name');
      try {
        await file.writeAsBytes(bytes);
        print('File written successfully');
      } catch (e) {
        print('Error writing file: $e');
      }
    } else {
      throw ServerException('${response.statusCode} ${response.statusMessage}');
    }
  }
}
