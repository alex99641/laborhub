import 'package:dio/dio.dart';
import 'package:laborhub/core/errors/exception.dart';
import 'package:laborhub/core/services/base_datasource.dart';
import 'package:laborhub/features/bussiness_logic/internal_models/paged_internal_model.dart';
import 'package:laborhub/features/data/dtos/proposal_dto.dart';
import 'package:laborhub/features/data/dtos/proposal_filter_dto.dart';
import 'package:laborhub/features/data/models/paged_response_model.dart';
import 'package:laborhub/features/data/models/proposal_model.dart';
import 'package:laborhub/features/data/models/response_model.dart';

class ProposalDatasource extends BaseDatasource {
  ProposalDatasource();

  Future<PagedInternalModel<ProposalModel>> getProposals(ProposalFilterDto filterDto) async {
    final response = await post('/api/proposal/page', body: filterDto.toJson());

    if (response.statusCode == 200) {
      return PagedInternalModel.fromPagedResponseModel(
          PagedResponseModel<ProposalModel>.fromMap(response.data, ProposalModel.fromMap));
    } else {
      throw ServerException('${response.statusCode} ${response.statusMessage}');
    }
  }

  Future<ProposalModel> createProposal(ProposalDto proposal) async {
    var filePaths = proposal.files?.map((e) => e.path).toList();
    var mediaPaths = proposal.media?.map((e) => e.path).toList();

    List<MultipartFile> files = [];
    List<MultipartFile> media = [];

    if (filePaths != null && filePaths.isNotEmpty) {
      for (String filePath in filePaths) {
        String fileName = filePath.split('/').last;
        files.add(await MultipartFile.fromFile(filePath, filename: fileName));
      }
    }

    if (mediaPaths != null && mediaPaths.isNotEmpty) {
      for (String mediaPath in mediaPaths) {
        String fileName = mediaPath.split('/').last;
        media.add(await MultipartFile.fromFile(mediaPath, filename: fileName));
      }
    }

    FormData formData = FormData.fromMap({'Files': files, 'Media': media});

    final response = await post(
      '/api/proposal',
      body: formData,
      queryParameters: proposal.toMap(),
    );

    var responseModel = ResponseModel.fromMap(response.data);

    if (response.statusCode == 200) {
      return ProposalModel.fromMap(responseModel.data);
    } else {
      throw ServerException(
          '${response.statusCode} ${response.statusMessage} ${responseModel.userMessage}');
    }
  }

  Future<ProposalModel> updateProposal(String id, ProposalDto proposal) async {
    final response = await put(
      '/api/proposal/$id',
      body: proposal.toMap(),
    );

    var responseModel = ResponseModel.fromMap(response.data);

    if (response.statusCode == 200) {
      return ProposalModel.fromMap(responseModel.data);
    } else {
      throw ServerException(
          '${response.statusCode} ${response.statusMessage} ${responseModel.userMessage}');
    }
  }

  Future<bool> deleteProposal(String id) async {
    final response = await delete('/api/proposal/$id');
    var responseModel = ResponseModel.fromMap(response.data);

    if (response.statusCode == 200) {
      return responseModel.data as bool;
    } else {
      throw ServerException(
          '${response.statusCode} ${response.statusMessage} ${responseModel.userMessage}');
    }
  }
}
