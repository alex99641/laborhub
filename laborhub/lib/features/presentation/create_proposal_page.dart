// ignore_for_file: use_build_context_synchronously, invalid_use_of_protected_member, invalid_use_of_visible_for_testing_member

import 'package:easy_localization/easy_localization.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:get/route_manager.dart';
import 'package:laborhub/core/constants/constants.dart';

import 'package:laborhub/core/services/locator_service.dart';
import 'package:laborhub/core/ui/appbar.dart';
import 'package:laborhub/core/ui/drawer.dart';
import 'package:laborhub/core/ui/labor_text_field.dart';
import 'package:laborhub/core/ui/media_picking/media_picking_widget.dart';
import 'package:laborhub/core/ui/toasts.dart';
import 'package:laborhub/features/bussiness_logic/blocs/authentication/authentication_bloc.dart';
import 'package:laborhub/features/bussiness_logic/blocs/proposals/create_proposal_cubit.dart';
import 'package:laborhub/features/bussiness_logic/blocs/states.dart';
import 'package:laborhub/features/data/dtos/proposal_dto.dart';
import 'package:laborhub/features/data/models/advertisement_model.dart';
import 'package:laborhub/features/presentation/proposal_page.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

class CreateProposalPage extends StatelessWidget {
  CreateProposalPage({
    Key? key,
    required this.advertisementModel,
  }) : super(key: key);
  final _proposalFormKey = GlobalKey<FormBuilderState>();
  final AdvertisementModel advertisementModel;

  static Route route({required AdvertisementModel advertisementModel}) {
    return MaterialPageRoute<void>(
        builder: (_) => CreateProposalPage(advertisementModel: advertisementModel));
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<CreateProposalCubit>(
      create: (_) => sl<CreateProposalCubit>(),
      child: Scaffold(
        appBar: LaborHubAppBar(
          title: 'create-proposal-page.page-title'.tr(),
        ),
        drawer: LaborHubDrawer(),
        body: FormBuilder(
          key: _proposalFormKey,
          child: ListView(
            // mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Wrap(runSpacing: 6, children: [
                  Text(
                    'create-proposal-page.proposal-for',
                    style: TextStyle(fontSize: 17, fontWeight: FontWeight.w500),
                  ).tr(),
                  InkWell(
                    onTap: () {
                      Get.back();
                    },
                    child: Text(
                      advertisementModel.title,
                      style: TextStyle(
                          fontSize: 17, fontWeight: FontWeight.w500, color: Colors.blue[900]),
                    ),
                  ),
                ]),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: LaborTextField(
                    name: 'title',
                    minLines: 1,
                    maxLines: 2,
                    validators: [
                      FormBuilderValidators.required(),
                      FormBuilderValidators.minLength(2),
                    ],
                    labelText: 'create-proposal-page.title',
                    formKey: _proposalFormKey),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 8, 8, 24),
                child: LaborTextField(
                    name: 'description',
                    minLines: 1,
                    maxLines: 7,
                    validators: [
                      FormBuilderValidators.required(),
                      FormBuilderValidators.minLength(2),
                    ],
                    labelText: 'create-proposal-page.description',
                    formKey: _proposalFormKey),
              ),
              MediaPickingWidget<CreateProposalCubit>(),
              pickFileWidget(),
              submitButtonWidget(),
            ],
          ),
        ),
      ),
    );
  }

  Widget pickFileWidget() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Builder(builder: (context) {
          return InkWell(
            onTap: () => _pickFileCallback(context),
            child: Container(
              padding: const EdgeInsets.symmetric(
                horizontal: 20.0,
                vertical: 10.0,
              ),
              child: Row(
                children: <Widget>[
                  Container(
                    margin: const EdgeInsets.all(2.0),
                    width: 48,
                    height: 48,
                    child: Center(
                      child: Text(
                        '📁',
                        style: const TextStyle(fontSize: 28.0),
                      ),
                    ),
                  ),
                  const SizedBox(width: 12.0),
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "Attach files",
                          style: const TextStyle(
                            fontSize: 18.0,
                            fontWeight: FontWeight.bold,
                          ),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                        const SizedBox(height: 5),
                        Text(
                          "Choose any files",
                          style: Theme.of(context).textTheme.bodySmall,
                        ),
                      ],
                    ),
                  ),
                  const Icon(Icons.chevron_right, color: Colors.grey),
                ],
              ),
            ),
          );
        }),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Builder(builder: (context) {
            final state = context.watch<CreateProposalCubit>().state;
            if (state is LaborHubInitialState<ProposalDto> &&
                state.model?.files != null &&
                state.model!.files!.isNotEmpty) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  for (var element in state.model!.files!)
                    Padding(
                      padding: const EdgeInsets.all(4.0),
                      child: Row(
                        children: [
                          Icon(Icons.attach_file, color: Colors.amber),
                          Expanded(
                            child: Text(
                              element.path.split('/').last,
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                            ),
                          ),
                        ],
                      ),
                    )
                ],
              );
            } else {
              return Container();
            }
          }),
        )
      ],
    );
  }

  void _pickFileCallback(BuildContext context) async {
    var result = await FilePicker.platform.pickFiles(
      type: FileType.custom,
      allowedExtensions: FileConstants.supportedContentTypes,
      withReadStream: true,
      withData: true,
      allowMultiple: true,
    );

    if (result != null) {
      context.read<CreateProposalCubit>().setFiles(files: result.files);
    }
  }

  Builder submitButtonWidget() {
    return Builder(builder: (context) {
      return BlocConsumer<CreateProposalCubit, LaborHubBlocState>(
        listener: (context, state) {
          if (state is LaborHubSucceedState) {
            Get.off(() => ProposalPage(proposalModel: state.model), preventDuplicates: false);
          } else if (state is LaborHubFailureState) {
            LaborHubToast.showError(state.message);
            // context.read<CreateProposalCubit>().emit(LaborHubInitialState());
          }
        },
        builder: (context, state) {
          if (state is LaborHubLoadingState) {
            return Center(child: CircularProgressIndicator());
          } else {
            return Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: Wrap(
                crossAxisAlignment: WrapCrossAlignment.center,
                alignment: WrapAlignment.center,
                children: [
                  ElevatedButton(
                    onPressed: () {
                      // Обработка нажатия кнопки входа
                      var authState = context.read<AuthBloc>().state as AuthenticatedState;

                      _proposalFormKey.currentState?.saveAndValidate();

                      if (_proposalFormKey.currentState?.isValid == true) {
                        var formMap = {
                          ..._proposalFormKey.currentState!.value,
                          'authorId': authState.profile.nameid,
                          'advertisementId': advertisementModel.id,
                        };
                        var cubit = context.read<CreateProposalCubit>();
                        var currentState = cubit.state as LaborHubInitialState<ProposalDto>;
                        currentState.model ??= ProposalDto();
                        var completedDto =
                            currentState.model!.copyWith(proposalDto: ProposalDto.fromMap(formMap));
                        cubit.createProposal(proposalDto: completedDto);
                      }
                    },
                    child: Text('common.save').tr(),
                  ),
                ],
              ),
            );
          }
        },
      );
    });
  }

  Container mapWidget() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 8),
      height: 250,
      child: Material(
        elevation: 2,
        borderRadius: BorderRadius.circular(10),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(10),
          child: YandexMap(
            gestureRecognizers: <Factory<OneSequenceGestureRecognizer>>{
              Factory<OneSequenceGestureRecognizer>(
                () => EagerGestureRecognizer(),
              ),
            },
            rotateGesturesEnabled: false,
            focusRect: ScreenRect(
                topLeft: ScreenPoint(x: 10, y: 10), bottomRight: ScreenPoint(x: 20, y: 20)),
            mapObjects: [
              singleMapObject(),
            ],
          ),
        ),
      ),
    );
  }

  PlacemarkMapObject singleMapObject() {
    return PlacemarkMapObject(
        mapId: MapObjectId("advertisementModel.id"),
        point: Point(latitude: 59.945933, longitude: 30.320045),
        onTap: (PlacemarkMapObject self, Point point) => print('Tapped me at $point'),
        opacity: 0.7,
        direction: 0,
        isDraggable: true,
        onDragStart: (_) => print('Drag start'),
        onDrag: (_, Point point) => print('Drag at point $point'),
        onDragEnd: (_) => print('Drag end'),
        icon: PlacemarkIcon.single(PlacemarkIconStyle(
            image: BitmapDescriptor.fromAssetImage(IconPath.map_marker),
            rotationType: RotationType.rotate)));
  }
}
