import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:get/route_manager.dart';
import 'package:laborhub/features/bussiness_logic/blocs/authentication/authentication_bloc.dart';
import 'package:laborhub/features/bussiness_logic/enums/roles.dart';
import 'package:laborhub/features/presentation/signup_page.dart';

class LoginPage extends StatelessWidget {
  final _signInFormKey = GlobalKey<FormBuilderState>();
  static Route route() {
    return MaterialPageRoute<void>(builder: (_) => LoginPage());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('login-page.title').tr(),
      ),
      body: Padding(
        padding: EdgeInsets.all(16.0),
        child: FormBuilder(
          key: _signInFormKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              _textField(
                  name: 'userName',
                  validators: [
                    FormBuilderValidators.required(),
                    FormBuilderValidators.minLength(2),
                  ],
                  labelText: 'login-page.login',
                  formKey: _signInFormKey),
              SizedBox(height: 16.0),
              _textField(
                  name: 'password',
                  validators: [
                    FormBuilderValidators.required(),
                    FormBuilderValidators.minLength(2),
                  ],
                  labelText: 'login-page.password',
                  obscureText: true,
                  formKey: _signInFormKey),
              SizedBox(height: 24.0),
              Wrap(
                direction: Axis.vertical,
                alignment: WrapAlignment.center,
                crossAxisAlignment: WrapCrossAlignment.center,
                spacing: 20,
                children: [
                  ElevatedButton(
                    onPressed: () {
                      // Обработка нажатия кнопки входа
                      _signInFormKey.currentState?.saveAndValidate();
                      context.read<AuthBloc>().add(AuthenticationLoginEvent(
                          userName: _signInFormKey.currentState?.value['userName'],
                          password: _signInFormKey.currentState?.value['password']));
                    },
                    child: Text('login-page.signin').tr(),
                  ),
                  InkWell(
                    onTap: () => Get.to(() => SignUpPage(role: Roles.client)),
                    child: Text('login-page.signup-for-client').tr(),
                  ),
                  InkWell(
                    onTap: () => Get.to(() => SignUpPage(role: Roles.master)),
                    child: Text('login-page.signup-for-master').tr(),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  InputDecoration _inputDecoration({required String labelText}) {
    return InputDecoration(
      labelText: labelText,
    );
  }

  FormBuilderTextField _textField({
    required String name,
    required List<String? Function(String?)> validators,
    required String labelText,
    required GlobalKey<FormBuilderState> formKey,
    obscureText = false,
  }) {
    return FormBuilderTextField(
      name: name,
      obscureText: obscureText,
      style: const TextStyle(
        // color: Colors.white,
        fontSize: 15,
      ),
      autovalidateMode: AutovalidateMode.disabled,
      validator: FormBuilderValidators.compose(validators),
      onChanged: (_) {
        formKey.currentState?.fields[name]?.validate();
      },
      decoration: _inputDecoration(labelText: labelText.tr()),
    );
  }
}
