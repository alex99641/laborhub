// ignore_for_file: use_build_context_synchronously, invalid_use_of_protected_member, invalid_use_of_visible_for_testing_member

import 'package:easy_localization/easy_localization.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:get/route_manager.dart';
import 'package:laborhub/core/constants/constants.dart';

import 'package:laborhub/core/services/locator_service.dart';
import 'package:laborhub/core/ui/appbar.dart';
import 'package:laborhub/core/ui/drawer.dart';
import 'package:laborhub/core/ui/labor_text_field.dart';
import 'package:laborhub/core/ui/maps/map_static_widget.dart';
import 'package:laborhub/core/ui/media_picking/media_picking_widget.dart';
import 'package:laborhub/core/ui/toasts.dart';
import 'package:laborhub/features/bussiness_logic/blocs/authentication/authentication_bloc.dart';
import 'package:laborhub/features/bussiness_logic/blocs/create_advertisement/create_advertisement_cubit.dart';
import 'package:laborhub/features/bussiness_logic/blocs/states.dart';
import 'package:laborhub/features/data/dtos/advertisement_dto.dart';
import 'package:laborhub/features/presentation/advertisement_page.dart';

class CreateAdvertisementPage extends StatelessWidget {
  CreateAdvertisementPage({
    Key? key,
  }) : super(key: key);
  final _advertisementFormKey = GlobalKey<FormBuilderState>();
  static Route route() {
    return MaterialPageRoute<void>(builder: (_) => CreateAdvertisementPage());
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<CreateAdvertisementCubit>(
      create: (_) => sl<CreateAdvertisementCubit>(),
      child: Scaffold(
        appBar: LaborHubAppBar(
          title: 'create-advert-page.page-title'.tr(),
        ),
        drawer: LaborHubDrawer(),
        body: FormBuilder(
          key: _advertisementFormKey,
          child: ListView(
            // mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: LaborTextField(
                    name: 'title',
                    minLines: 1,
                    maxLines: 2,
                    validators: [
                      FormBuilderValidators.required(),
                      FormBuilderValidators.minLength(2),
                    ],
                    labelText: 'create-advert-page.title',
                    formKey: _advertisementFormKey),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 8, 8, 24),
                child: LaborTextField(
                    name: 'description',
                    minLines: 1,
                    maxLines: 7,
                    validators: [
                      FormBuilderValidators.required(),
                      FormBuilderValidators.minLength(2),
                    ],
                    labelText: 'create-advert-page.description',
                    formKey: _advertisementFormKey),
              ),
              MediaPickingWidget<CreateAdvertisementCubit>(),
              pickFileWidget(),
              MapStaticWidget<CreateAdvertisementCubit>(),
              submitButtonWidget(),
            ],
          ),
        ),
      ),
    );
  }

  Widget pickFileWidget() {
    return Column(
      children: [
        Builder(builder: (context) {
          return InkWell(
            onTap: () => _pickFileCallback(context),
            child: Container(
              padding: const EdgeInsets.symmetric(
                horizontal: 20.0,
                vertical: 10.0,
              ),
              child: Row(
                children: <Widget>[
                  Container(
                    margin: const EdgeInsets.all(2.0),
                    width: 48,
                    height: 48,
                    child: Center(
                      child: Text(
                        '📁',
                        style: const TextStyle(fontSize: 28.0),
                      ),
                    ),
                  ),
                  const SizedBox(width: 12.0),
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "Attach files",
                          style: const TextStyle(
                            fontSize: 18.0,
                            fontWeight: FontWeight.bold,
                          ),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                        const SizedBox(height: 5),
                        Text(
                          "Choose any files",
                          style: Theme.of(context).textTheme.bodySmall,
                        ),
                      ],
                    ),
                  ),
                  const Icon(Icons.chevron_right, color: Colors.grey),
                ],
              ),
            ),
          );
        }),
        Builder(builder: (context) {
          final state = context.watch<CreateAdvertisementCubit>().state;
          if (state is LaborHubInitialState<AdvertisementDto> &&
              state.model?.files != null &&
              state.model!.files!.isNotEmpty) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                for (var element in state.model!.files!)
                  Wrap(
                    children: [
                      Icon(Icons.attach_file, color: Colors.amber),
                      Text(element.path.split('/').last),
                    ],
                  )
              ],
            );
          } else {
            return Container();
          }
        })
      ],
    );
  }

  void _pickFileCallback(BuildContext context) async {
    var result = await FilePicker.platform.pickFiles(
      type: FileType.custom,
      allowedExtensions: FileConstants.supportedContentTypes,
      withReadStream: true,
      withData: true,
      allowMultiple: true,
    );

    if (result != null) {
      context.read<CreateAdvertisementCubit>().setFiles(files: result.files);
    }
  }

  Builder submitButtonWidget() {
    return Builder(builder: (context) {
      return BlocConsumer<CreateAdvertisementCubit, LaborHubBlocState>(
        listener: (context, state) {
          if (state is LaborHubSucceedState) {
            Get.off(() => AdvertisementPage(advertisementModel: state.model),
                preventDuplicates: false);
          } else if (state is LaborHubFailureState) {
            LaborHubToast.showError(state.message);
            // context.read<CreateAdvertisementCubit>().emit(CreateAdvertisementInitialState());
          }
        },
        builder: (context, state) {
          if (state is LaborHubLoadingState) {
            return Center(child: CircularProgressIndicator());
          } else {
            return Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: Wrap(
                crossAxisAlignment: WrapCrossAlignment.center,
                alignment: WrapAlignment.center,
                children: [
                  ElevatedButton(
                    onPressed: () {
                      // Обработка нажатия кнопки входа
                      var authState = context.read<AuthBloc>().state as AuthenticatedState;

                      _advertisementFormKey.currentState?.saveAndValidate();

                      if (_advertisementFormKey.currentState?.isValid == true) {
                        var formMap = {
                          ..._advertisementFormKey.currentState!.value,
                          'authorId': authState.profile.nameid,
                        };

                        var cubit = context.read<CreateAdvertisementCubit>();
                        var currentState = cubit.state as LaborHubInitialState<AdvertisementDto>;
                        currentState.model ??= AdvertisementDto();
                        var completedDto = currentState.model!
                            .copyWith(advertisementDto: AdvertisementDto.fromMap(formMap));
                        cubit.createAdvertisement(advertisementDto: completedDto);
                      }
                    },
                    child: Text('create-advert-page.save').tr(),
                  ),
                ],
              ),
            );
          }
        },
      );
    });
  }
}
