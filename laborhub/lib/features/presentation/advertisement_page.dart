import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/route_manager.dart';
import 'package:laborhub/core/ui/maps/map_static_widget.dart';
import 'package:laborhub/features/bussiness_logic/blocs/map_updatable_cubit.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

import 'package:laborhub/core/constants/constants.dart';
import 'package:laborhub/core/constants/settings.dart';
import 'package:laborhub/core/services/locator_service.dart';
import 'package:laborhub/core/ui/appbar.dart';
import 'package:laborhub/core/ui/drawer.dart';
import 'package:laborhub/core/ui/file_list.dart';
import 'package:laborhub/core/ui/image_gallery.dart';
import 'package:laborhub/features/bussiness_logic/blocs/create_advertisement/create_advertisement_cubit.dart';
import 'package:laborhub/features/data/models/advertisement_model.dart';
import 'package:laborhub/features/presentation/create_proposal_page.dart';

class AdvertisementPage extends StatelessWidget {
  AdvertisementPage({
    Key? key,
    required this.advertisementModel,
  }) : super(key: key);

  final AdvertisementModel advertisementModel;

  static Route route({required AdvertisementModel advertisementModel}) {
    return MaterialPageRoute<void>(
        builder: (_) => AdvertisementPage(advertisementModel: advertisementModel));
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<CreateAdvertisementCubit>(
      create: (_) => sl<CreateAdvertisementCubit>(),
      child: Scaffold(
        appBar: LaborHubAppBar(
          title: 'advertisement.page-title'.tr(),
        ),
        drawer: LaborHubDrawer(),
        body: ListView(children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: Text(advertisementModel.title,
                      style: TextStyle(fontSize: 22, fontWeight: FontWeight.w600)),
                ),
              ),
              IconButton(
                  onPressed: () {
                    Get.to(CreateProposalPage(advertisementModel: advertisementModel));
                  },
                  icon: Icon(Icons.create_new_folder_outlined)),
              IconButton(onPressed: () {}, icon: Icon(Icons.edit)),
            ],
          ),
          ImageGallery(
            imagesList: advertisementModel.medias
                .map((mediaName) => '${Settings.backendUrl}${Settings.fileEndpoint}$mediaName')
                .toList(),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 12.0, vertical: 12),
            child: Text(advertisementModel.description,
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500)),
          ),
          FileListWidget(model: advertisementModel),
          MapStaticWidget(
            location: Point(
                latitude: advertisementModel.latitude, longitude: advertisementModel.longitude),
          ),
        ]),
      ),
    );
  }
}
