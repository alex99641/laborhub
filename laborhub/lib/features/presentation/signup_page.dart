import 'package:easy_localization/easy_localization.dart';
import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:get/route_manager.dart';

import 'package:laborhub/core/services/locator_service.dart';
import 'package:laborhub/core/ui/toasts.dart';
import 'package:laborhub/features/bussiness_logic/blocs/authentication/authentication_bloc.dart';
import 'package:laborhub/features/bussiness_logic/blocs/registration/signup_cubit.dart';
import 'package:laborhub/features/bussiness_logic/enums/roles.dart';
import 'package:laborhub/features/data/dtos/signup_dto.dart';

class SignUpPage extends StatelessWidget {
  final Roles role;
  SignUpPage({
    Key? key,
    required this.role,
  }) : super(key: key);
  final _signUpFormKey = GlobalKey<FormBuilderState>();
  static Route route(Roles role) {
    return MaterialPageRoute<void>(builder: (_) => SignUpPage(role: role));
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<SignUpCubit>(
      create: (_) => sl<SignUpCubit>(),
      child: Scaffold(
        appBar: AppBar(
          title: Text('signup-page.title').tr(),
        ),
        body: FormBuilder(
          key: _signUpFormKey,
          child: ListView(
            // mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              _textField(
                  name: 'firstName',
                  validators: [
                    FormBuilderValidators.required(),
                    FormBuilderValidators.minLength(2),
                  ],
                  labelText: 'signup-page.firstName',
                  formKey: _signUpFormKey),
              SizedBox(height: 16.0),
              _textField(
                  name: 'lastName',
                  validators: [
                    FormBuilderValidators.required(),
                    FormBuilderValidators.minLength(2),
                  ],
                  labelText: 'signup-page.lastName',
                  formKey: _signUpFormKey),
              SizedBox(height: 16.0),
              _textField(
                  name: 'userName',
                  validators: [
                    FormBuilderValidators.required(),
                    FormBuilderValidators.minLength(2),
                  ],
                  labelText: 'signup-page.login',
                  formKey: _signUpFormKey),
              SizedBox(height: 16.0),
              _textField(
                  name: 'email',
                  validators: [
                    FormBuilderValidators.required(),
                    FormBuilderValidators.email(),
                  ],
                  labelText: 'signup-page.email',
                  formKey: _signUpFormKey),
              SizedBox(height: 16.0),
              _textField(
                  name: 'phoneNumber',
                  controller: MaskedTextController(mask: '+0 (000) 000-00-00'),
                  validators: [
                    FormBuilderValidators.required(),
                    FormBuilderValidators.match(r'^\+\d \(\d{3}\) \d{3}\-\d{2}\-\d{2}$'),
                  ],
                  labelText: 'signup-page.phoneNumber',
                  formKey: _signUpFormKey),
              SizedBox(height: 16.0),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                child: FormBuilderDateTimePicker(
                  inputType: InputType.date,
                  initialEntryMode: DatePickerEntryMode.input,
                  name: 'birthday',
                  decoration: _inputDecoration(labelText: 'signup-page.dateOfBirth'.tr()),
                ),
              ),
              SizedBox(height: 16.0),
              _textField(
                  name: 'password',
                  validators: [
                    FormBuilderValidators.required(),
                    FormBuilderValidators.minLength(2),
                  ],
                  labelText: 'signup-page.password',
                  obscureText: true,
                  formKey: _signUpFormKey),
              SizedBox(height: 16.0),
              _textField(
                  name: 'passwordConfirm',
                  validators: [
                    FormBuilderValidators.required(),
                    FormBuilderValidators.minLength(2),
                  ],
                  labelText: 'signup-page.passwordConfirm',
                  obscureText: true,
                  formKey: _signUpFormKey),
              SizedBox(height: 24.0),
              Builder(builder: (context) {
                return BlocConsumer<SignUpCubit, SignUpState>(listener: (context, state) {
                  if (state is SignUpSucceedState) {
                    context.read<AuthBloc>().setAuthorizedByToken(state.tokens);
                  } else if (state is SignUpFailureState) {
                    LaborHubToast.showError(state.message);
                  }
                }, builder: (context, state) {
                  if (state is SignUpLoadingState) {
                    return SizedBox(width: 15, height: 15, child: CircularProgressIndicator());
                  } else {
                    return Wrap(
                      crossAxisAlignment: WrapCrossAlignment.center,
                      alignment: WrapAlignment.center,
                      children: [
                        ElevatedButton(
                          onPressed: () {
                            // Обработка нажатия кнопки входа
                            _signUpFormKey.currentState?.saveAndValidate();
                            context.read<SignUpCubit>().register(
                                    signUpDto: SignUpDto.fromMap({
                                  ..._signUpFormKey.currentState!.value,
                                  'role': role,
                                }));

                            // add(AuthenticationLoginEvent(
                            //     userName: _signUpFormKey.currentState?.value['userName'],
                            //     password: _signUpFormKey.currentState?.value['password']));
                          },
                          child: Text('signup-page.signup').tr(),
                        ),
                        SizedBox(width: 24.0),
                        InkWell(
                          onTap: () => Get.back(),
                          child: Text('signup-page.signin').tr(),
                        ),
                      ],
                    );
                  }
                });
              }),
            ],
          ),
        ),
      ),
    );
  }

  InputDecoration _inputDecoration({required String labelText}) {
    return InputDecoration(
      labelText: labelText,
    );
  }

  Widget _textField({
    required String name,
    required List<String? Function(String?)> validators,
    required String labelText,
    required GlobalKey<FormBuilderState> formKey,
    obscureText = false,
    TextEditingController? controller,
  }) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 0),
      child: FormBuilderTextField(
        name: name,
        obscureText: obscureText,
        style: const TextStyle(
          // color: Colors.white,
          fontSize: 15,
        ),
        controller: controller,
        autovalidateMode: AutovalidateMode.disabled,
        validator: FormBuilderValidators.compose(validators),
        onChanged: (_) {
          formKey.currentState?.fields[name]?.validate();
        },
        decoration: _inputDecoration(labelText: labelText.tr()),
      ),
    );
  }
}
