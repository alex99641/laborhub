import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/route_manager.dart';

import 'package:laborhub/core/services/locator_service.dart';
import 'package:laborhub/core/ui/appbar.dart';
import 'package:laborhub/core/ui/drawer.dart';
import 'package:laborhub/features/bussiness_logic/blocs/get_advertisement_list/get_advertisement_list_cubit.dart';
import 'package:laborhub/features/bussiness_logic/blocs/states.dart';
import 'package:laborhub/features/bussiness_logic/internal_models/paged_internal_model.dart';
import 'package:laborhub/features/data/dtos/page_advert_dto.dart';
import 'package:laborhub/features/data/models/advertisement_model.dart';
import 'package:laborhub/features/presentation/advertisement_page.dart';
import 'package:number_paginator/number_paginator.dart';

class ListAdvertisementPage extends StatefulWidget {
  ListAdvertisementPage({Key? key}) : super(key: key);

  static Route route() {
    return MaterialPageRoute<void>(builder: (_) => ListAdvertisementPage());
  }

  @override
  State<ListAdvertisementPage> createState() => _ListAdvertisementPageState();
}

class _ListAdvertisementPageState extends State<ListAdvertisementPage> {
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
  int pageNumber = 1;

  @override
  Widget build(BuildContext context) {
    return BlocProvider<LoadAdvertisementListCubit>(
      create: (_) => sl<LoadAdvertisementListCubit>()
        ..getAdvertisementList(pageAdvertDto: PageAdvertDto.defaultModel()),
      child: Scaffold(
        appBar: LaborHubAppBar(
          title: 'Advertisement'.tr(),
        ),
        drawer: LaborHubDrawer(),
        body: advertisementList(),
        bottomNavigationBar: bottomNavigationBar(),
      ),
    );
  }

  BlocBuilder<LoadAdvertisementListCubit, LaborHubBlocState> advertisementList() {
    return BlocBuilder<LoadAdvertisementListCubit, LaborHubBlocState>(
      builder: (context, state) {
        if (state is LaborHubLoadingState) {
          return Column(
            // mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              RefreshIndicator(
                key: _refreshIndicatorKey,
                onRefresh: () async {
                  context.read<LoadAdvertisementListCubit>().getAdvertisementList(
                      pageAdvertDto: PageAdvertDto.defaultModel(page: pageNumber));
                },
                child: ListView(shrinkWrap: true),
              ),
              Center(child: CircularProgressIndicator()),
            ],
          );
        } else if (state is LaborHubSucceedState) {
          var pagedModel = state.model as PagedInternalModel<AdvertisementModel>;
          return RefreshIndicator(
            key: _refreshIndicatorKey,
            onRefresh: () async {
              context.read<LoadAdvertisementListCubit>().getAdvertisementList(
                  pageAdvertDto: PageAdvertDto.defaultModel(page: pageNumber));
            },
            child: ListView.builder(
              shrinkWrap: true,
              itemCount: pagedModel.totalReturned,
              itemBuilder: (context, index) {
                return ListTile(
                  title: Text(pagedModel.data[index].title),
                  subtitle: Text(pagedModel.data[index].description,
                      maxLines: 1, overflow: TextOverflow.ellipsis),
                  onTap: () {
                    Get.to(AdvertisementPage(advertisementModel: pagedModel.data[index]));
                  },
                );
              },
            ),
          );
        } else {
          return RefreshIndicator(
            key: _refreshIndicatorKey,
            onRefresh: () async {
              context.read<LoadAdvertisementListCubit>().getAdvertisementList(
                  pageAdvertDto: PageAdvertDto.defaultModel(page: pageNumber));
            },
            child: ListView(),
          );
        }
      },
    );
  }

  BlocBuilder<LoadAdvertisementListCubit, LaborHubBlocState> bottomNavigationBar() {
    return BlocBuilder<LoadAdvertisementListCubit, LaborHubBlocState>(
      buildWhen: (previous, current) {
        return current is LaborHubSucceedState;
      },
      builder: (context, state) {
        if (state is! LaborHubSucceedState) {
          return Container();
        }
        var currentState = state as LaborHubSucceedState<PagedInternalModel>;
        return Card(
          margin: EdgeInsets.zero,
          elevation: 4,
          child: NumberPaginator(
            // by default, the paginator shows numbers as center content
            numberPages: (currentState.model.total / currentState.model.pageSize).ceil(),
            onPageChange: (int index) {
              setState(() {
                pageNumber = index + 1;
              });
              context.read<LoadAdvertisementListCubit>().getAdvertisementList(
                  pageAdvertDto: PageAdvertDto.defaultModel(page: pageNumber));
            },
          ),
        );
      },
    );
  }
}
