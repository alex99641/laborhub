import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/route_manager.dart';

import 'package:laborhub/core/services/locator_service.dart';
import 'package:laborhub/core/ui/appbar.dart';
import 'package:laborhub/core/ui/drawer.dart';
import 'package:laborhub/features/bussiness_logic/blocs/proposals/proposal_list_cubit.dart';
import 'package:laborhub/features/bussiness_logic/blocs/states.dart';
import 'package:laborhub/features/bussiness_logic/internal_models/paged_internal_model.dart';
import 'package:laborhub/features/data/dtos/proposal_filter_dto.dart';
import 'package:laborhub/features/data/models/proposal_model.dart';
import 'package:laborhub/features/presentation/proposal_page.dart';
import 'package:number_paginator/number_paginator.dart';

class ListProposalPage extends StatefulWidget {
  ListProposalPage({Key? key}) : super(key: key);

  static Route route() {
    return MaterialPageRoute<void>(builder: (_) => ListProposalPage());
  }

  @override
  State<ListProposalPage> createState() => _ListProposalPageState();
}

class _ListProposalPageState extends State<ListProposalPage> {
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
  int pageNumber = 1;

  @override
  Widget build(BuildContext context) {
    return BlocProvider<ProposalListCubit>(
      create: (_) => sl<ProposalListCubit>()
        ..getProposalList(proposalFilterDto: ProposalFilterDto.defaultModel()),
      child: Scaffold(
        appBar: LaborHubAppBar(
          title: 'proposal.list-page-title'.tr(),
        ),
        drawer: LaborHubDrawer(),
        body: proposalList(),
        bottomNavigationBar: bottomNavigationBar(),
      ),
    );
  }

  BlocBuilder<ProposalListCubit, LaborHubBlocState> proposalList() {
    return BlocBuilder<ProposalListCubit, LaborHubBlocState>(
      builder: (context, state) {
        if (state is LaborHubLoadingState) {
          return Column(
            // mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              RefreshIndicator(
                key: _refreshIndicatorKey,
                onRefresh: () async {
                  context.read<ProposalListCubit>().getProposalList(
                      proposalFilterDto: ProposalFilterDto.defaultModel(page: pageNumber));
                },
                child: ListView(shrinkWrap: true),
              ),
              Center(child: CircularProgressIndicator()),
            ],
          );
        } else if (state is LaborHubSucceedState) {
          var pagedModel = state.model as PagedInternalModel<ProposalModel>;
          return RefreshIndicator(
            key: _refreshIndicatorKey,
            onRefresh: () async {
              context.read<ProposalListCubit>().getProposalList(
                  proposalFilterDto: ProposalFilterDto.defaultModel(page: pageNumber));
            },
            child: ListView.builder(
              shrinkWrap: true,
              itemCount: pagedModel.totalReturned,
              itemBuilder: (context, index) {
                if (index < 10) {
                  return ListTile(
                    title: Text(pagedModel.data[index].title),
                    subtitle: Text(pagedModel.data[index].description,
                        maxLines: 1, overflow: TextOverflow.ellipsis),
                    onTap: () {
                      Get.to(ProposalPage(proposalModel: pagedModel.data[index]));
                    },
                  );
                }
              },
            ),
          );
        } else {
          return RefreshIndicator(
            key: _refreshIndicatorKey,
            onRefresh: () async {
              context.read<ProposalListCubit>().getProposalList(
                  proposalFilterDto: ProposalFilterDto.defaultModel(page: pageNumber));
            },
            child: ListView(),
          );
        }
      },
    );
  }

  BlocBuilder<ProposalListCubit, LaborHubBlocState> bottomNavigationBar() {
    return BlocBuilder<ProposalListCubit, LaborHubBlocState>(
      buildWhen: (previous, current) {
        return current is LaborHubSucceedState;
      },
      builder: (context, state) {
        if (state is! LaborHubSucceedState) {
          return Container();
        }
        var currentState = state as LaborHubSucceedState<PagedInternalModel>;
        return Card(
          margin: EdgeInsets.zero,
          elevation: 4,
          child: NumberPaginator(
            // by default, the paginator shows numbers as center content
            numberPages: (currentState.model.total / currentState.model.pageSize).ceil(),
            onPageChange: (int index) {
              setState(() {
                pageNumber = index + 1;
              });
              context.read<ProposalListCubit>().getProposalList(
                  proposalFilterDto: ProposalFilterDto.defaultModel(page: pageNumber));
            },
          ),
        );
      },
    );
  }
}
