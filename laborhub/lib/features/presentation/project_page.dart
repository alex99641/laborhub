import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/route_manager.dart';
import 'package:laborhub/core/constants/settings.dart';

import 'package:laborhub/core/services/locator_service.dart';
import 'package:laborhub/core/ui/appbar.dart';
import 'package:laborhub/core/ui/drawer.dart';
import 'package:laborhub/core/ui/file_list.dart';
import 'package:laborhub/core/ui/image_gallery.dart';
import 'package:laborhub/core/ui/maps/map_static_widget.dart';
import 'package:laborhub/features/bussiness_logic/blocs/map_updatable_cubit.dart';
import 'package:laborhub/features/bussiness_logic/blocs/proposals/proposal_cubit.dart';
import 'package:laborhub/features/bussiness_logic/enums/project_status.dart';
import 'package:laborhub/features/data/dtos/project_dto.dart';
import 'package:laborhub/features/data/models/project_model.dart';
import 'package:lil_guid/lil_guid.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

class ProjectPage extends StatelessWidget {
  ProjectPage({
    Key? key,
    required this.projectModel,
  }) : super(key: key);

  final ProjectModel projectModel;

  static Route route({required ProjectModel proposalModel}) {
    return MaterialPageRoute<void>(builder: (_) => ProjectPage(projectModel: proposalModel));
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<ProposalCubit>(
      create: (_) => sl<ProposalCubit>(),
      child: Scaffold(
        // appBar: AppBar(
        //   title: Text('Advertisement'.tr()),
        // ),
        appBar: LaborHubAppBar(
          title: 'Project'.tr(),
        ),
        drawer: LaborHubDrawer(),
        body: ListView(children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: Text(projectModel.title,
                      style: TextStyle(fontSize: 22, fontWeight: FontWeight.w600)),
                ),
              ),
              IconButton(onPressed: () {}, icon: Icon(Icons.edit)),
            ],
          ),
          ImageGallery(
            imagesList: projectModel.medias
                .map((mediaName) => '${Settings.backendUrl}${Settings.fileEndpoint}$mediaName')
                .toList(),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 12),
            child: Text(projectModel.description,
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500)),
          ),
          FileListWidget(model: projectModel),
          MapStaticWidget(
            location: Point(
                latitude: projectModel.advertisement.latitude,
                longitude: projectModel.advertisement.longitude),
          ),
        ]),
      ),
    );
  }
}
