// ignore_for_file: use_build_context_synchronously

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/route_manager.dart';
import 'package:laborhub/core/constants/settings.dart';

import 'package:laborhub/core/services/locator_service.dart';
import 'package:laborhub/core/ui/appbar.dart';
import 'package:laborhub/core/ui/drawer.dart';
import 'package:laborhub/core/ui/file_list.dart';
import 'package:laborhub/core/ui/image_gallery.dart';
import 'package:laborhub/features/bussiness_logic/blocs/proposals/proposal_cubit.dart';
import 'package:laborhub/features/bussiness_logic/enums/project_status.dart';
import 'package:laborhub/features/data/dtos/project_dto.dart';
import 'package:laborhub/features/data/models/proposal_model.dart';
import 'package:laborhub/features/presentation/advertisement_page.dart';
import 'package:laborhub/features/presentation/project_page.dart';
import 'package:lil_guid/lil_guid.dart';

class ProposalPage extends StatelessWidget {
  ProposalPage({
    Key? key,
    required this.proposalModel,
  }) : super(key: key);

  final ProposalModel proposalModel;

  static Route route({required ProposalModel proposalModel}) {
    return MaterialPageRoute<void>(builder: (_) => ProposalPage(proposalModel: proposalModel));
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<ProposalCubit>(
      create: (_) => sl<ProposalCubit>(),
      child: Scaffold(
        // appBar: AppBar(
        //   title: Text('Advertisement'.tr()),
        // ),
        appBar: LaborHubAppBar(
          title: 'Proposal'.tr(),
        ),
        drawer: LaborHubDrawer(),
        body: ListView(children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: Text(proposalModel.title,
                      style: TextStyle(fontSize: 22, fontWeight: FontWeight.w600)),
                ),
              ),
              IconButton(onPressed: () => acceptProposalOnPress(context), icon: Icon(Icons.done)),
              IconButton(onPressed: () {}, icon: Icon(Icons.edit)),
            ],
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Wrap(runSpacing: 6, children: [
              Text(
                'create-proposal-page.proposal-for',
                style: TextStyle(fontSize: 17, fontWeight: FontWeight.w500),
              ).tr(),
              InkWell(
                onTap: () {
                  Get.to(AdvertisementPage(advertisementModel: proposalModel.advertisement));
                },
                child: Text(
                  proposalModel.advertisement.title,
                  style:
                      TextStyle(fontSize: 17, fontWeight: FontWeight.w500, color: Colors.blue[900]),
                ),
              ),
            ]),
          ),
          ImageGallery(
            imagesList: proposalModel.medias
                .map((mediaName) => '${Settings.backendUrl}${Settings.fileEndpoint}$mediaName')
                .toList(),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 12),
            child: Text(proposalModel.description,
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500)),
          ),
          FileListWidget(model: proposalModel),
        ]),
      ),
    );
  }

  acceptProposalOnPress(context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return BlocProvider<ProposalCubit>(
          create: (_) => sl<ProposalCubit>(),
          child: AlertDialog(
            title: Text('proposal.accept-proposal-dialog-title').tr(),
            content: Text('proposal.accept-proposal-dialog-question').tr(),
            actions: [
              TextButton(
                child: Text('common.cancel').tr(),
                onPressed: () {
                  Navigator.of(context).pop(); // Закрытие диалогового окна
                },
              ),
              Builder(builder: (context) {
                return TextButton(
                  child: Text('proposal.accept-proposal-dialog-confirm').tr(),
                  onPressed: () async {
                    var project = await context.read<ProposalCubit>().createProject(
                          projectDto: ProjectDto(
                            title: proposalModel.advertisement.title,
                            projectStatus: ProjectStatus.inProgress,
                            description:
                                "${proposalModel.advertisement.description}\n\n${proposalModel.description}",
                            advertisementId: Guid.parseString(proposalModel.advertisement.id),
                            proposalId: proposalModel.id,
                            clientId: proposalModel.advertisement.author.id,
                            masterId: proposalModel.author.id,
                          ),
                        );
                    Navigator.pop(context);
                    Get.to(ProjectPage(projectModel: project));
                  },
                );
              }),
            ],
          ),
        );
      },
    );
  }
}
