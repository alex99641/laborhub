import 'package:laborhub/core/ui/toasts.dart';

class ServerException extends LaborHubException {
  ServerException(String message) : super(message: message);
}

class CacheException extends LaborHubException {
  CacheException(String message) : super(message: message);
}

class LaborHubException implements Exception {
  final String message;
  LaborHubException({
    required this.message,
  }) : super() {
    print(message);
    LaborHubToast.showError(message);
  }
}
