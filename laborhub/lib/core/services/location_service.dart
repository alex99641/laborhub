import 'package:geolocator/geolocator.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

class LocationService {
  Future<Position> getCurrentLocation() async {
    await _requestPermission();
    await _requestService();
    return await Geolocator.getCurrentPosition();
  }

  Stream<Position> startLocationUpdates() {
    return Geolocator.getPositionStream();
  }

  Future<void> _requestPermission() async {
    // if (await Permission.locationWhenInUse.request().isGranted) {
    //   await Permission.locationAlways.request();
    // }
    // LocationPermission permission = await Geolocator.checkPermission();
    await Permission.locationWhenInUse.request();
    // if (permission == LocationPermission.denied || permission == LocationPermission.deniedForever) {
    if (await Permission.locationWhenInUse.request().isGranted) {
      // await _requestPermission();
      await Permission.locationAlways.request();
      if (!await Permission.locationAlways.request().isGranted) {
        await _requestPermission();
      }
    } else {
      await _requestPermission();
    }
  }

  Future<void> _requestService() async {
    bool serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      await Geolocator.openLocationSettings();
      await _requestService();
    }
  }
}

extension PositionConverter on Position {
  Point toPoint() => Point(latitude: latitude, longitude: longitude);
}
