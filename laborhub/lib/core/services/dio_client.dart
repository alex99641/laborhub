import 'dart:async';

import 'package:dio/dio.dart';
import 'package:laborhub/core/constants/settings.dart';

class DioClient {
  final Dio dio = Dio();

  static const baseUrl = Settings.backendUrl;

  DioClient() {
    dio.options
      ..baseUrl = baseUrl
      // ..connectTimeout = 5000 //5s
      // ..receiveTimeout = 5000
      ..validateStatus = (int? status) {
        return status != null && status > 0;
      }
      ..headers = {
        // HttpHeaders.userAgentHeader: 'dio',
        // 'common-header': 'xx',
      };

    // Token can be shared with different requests.
    var token = CancelToken();

    // In 10 minutes, we cancel!
    Timer(const Duration(minutes: 10), () {
      token.cancel('cancelled');
    });
  }
}
