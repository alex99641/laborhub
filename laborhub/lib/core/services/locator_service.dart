// ignore_for_file: cascade_invocations

import 'package:laborhub/core/interceptors/auth_interceptor.dart';
import 'package:laborhub/core/platform/network_info.dart';

import 'package:get_it/get_it.dart';
import 'package:laborhub/core/services/location_service.dart';
import 'package:laborhub/features/bussiness_logic/blocs/authentication/authentication_bloc.dart';
import 'package:laborhub/features/bussiness_logic/blocs/create_advertisement/create_advertisement_cubit.dart';
import 'package:laborhub/features/bussiness_logic/blocs/get_advertisement_list/get_advertisement_list_cubit.dart';
import 'package:laborhub/features/bussiness_logic/blocs/proposals/create_proposal_cubit.dart';
import 'package:laborhub/features/bussiness_logic/blocs/proposals/proposal_cubit.dart';
import 'package:laborhub/features/bussiness_logic/blocs/proposals/proposal_list_cubit.dart';
import 'package:laborhub/features/bussiness_logic/blocs/registration/signup_cubit.dart';
import 'package:laborhub/features/bussiness_logic/repositories/account_repository.dart';
import 'package:laborhub/features/bussiness_logic/repositories/advertisement_repository.dart';
import 'package:laborhub/features/bussiness_logic/repositories/authentication_repository.dart';
import 'package:laborhub/features/bussiness_logic/repositories/project_repository.dart';
import 'package:laborhub/features/bussiness_logic/repositories/proposal_repository.dart';
import 'package:laborhub/features/bussiness_logic/repositories/token_repository.dart';
import 'package:laborhub/features/data/sources/account_datasource.dart';
import 'package:laborhub/features/data/sources/advertisement_datasource.dart';
import 'package:laborhub/features/data/sources/auth_datasource.dart';
import 'package:laborhub/features/data/sources/file_datasource.dart';
import 'package:laborhub/features/data/sources/project_datasource.dart';
import 'package:laborhub/features/data/sources/proposal_datasource.dart';
import 'package:laborhub/features/data/sources/token_local_datasource.dart';

final sl = GetIt.instance;

Future<void> init() async {
  // BLoC / Cubit

  sl.registerLazySingleton(
    () => AuthBloc(authenticationRepository: sl(), tokenRepository: sl()),
  );
  sl.registerLazySingleton(
    () => SignUpCubit(accountRepository: sl()),
  );
  sl.registerFactory(
    () => CreateAdvertisementCubit(advertisementRepository: sl()),
  );
  sl.registerLazySingleton(() => LoadAdvertisementListCubit(advertisementRepository: sl()));
  sl.registerLazySingleton(() => CreateProposalCubit(proposalRepository: sl()));
  sl.registerLazySingleton(() => ProposalCubit(proposalRepository: sl(), projectRepository: sl()));
  sl.registerLazySingleton(() => ProposalListCubit(proposalRepository: sl()));

  // Repositories
  sl.registerLazySingleton<AuthenticationRepository>(
    () => AuthenticationRepository(authenticationDatasource: sl()),
  );
  sl.registerLazySingleton<TokenRepository>(
    () => TokenRepository(tokenLocalDatasource: sl()),
  );
  sl.registerLazySingleton<AccountRepository>(
    () => AccountRepository(accountDatasource: sl()),
  );
  sl.registerLazySingleton<AdvertisementRepository>(
    () => AdvertisementRepository(datasource: sl()),
  );
  sl.registerLazySingleton<ProposalRepository>(
    () => ProposalRepository(datasource: sl()),
  );
  sl.registerLazySingleton<ProjectRepository>(
    () => ProjectRepository(datasource: sl()),
  );

  // Datasources
  sl.registerLazySingleton<AuthenticationDatasource>(() => AuthenticationDatasource());
  sl.registerLazySingleton<TokenLocalDatasource>(() => TokenLocalDatasource());
  sl.registerLazySingleton<AccountDatasource>(() => AccountDatasource());
  sl.registerLazySingleton<AdvertisementDatasource>(() => AdvertisementDatasource());
  sl.registerLazySingleton<FileDatasource>(() => FileDatasource());
  sl.registerLazySingleton<ProposalDatasource>(() => ProposalDatasource());
  sl.registerLazySingleton<ProjectDatasource>(() => ProjectDatasource());

  // Core
  sl.registerLazySingleton<NetworkInfo>(
    () => NetworkInfoImp(sl()),
  );

  // External
  // final sharedPreferences = await SharedPreferences.getInstance();
  // sl.registerLazySingleton(() => sharedPreferences);
  // sl.registerLazySingleton(() => const FlutterSecureStorage());
  // sl.registerLazySingleton(() => http.Client());
  // sl.registerLazySingleton(() => InternetConnectionChecker());

  // Interceptor
  sl.registerSingleton<AuthInterceptor>(AuthInterceptor());

  // Service
  sl.registerLazySingleton<LocationService>(
    () => LocationService(),
  );
}
