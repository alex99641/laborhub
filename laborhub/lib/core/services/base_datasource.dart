import 'dart:io';

import 'package:dio/dio.dart';
import 'package:dio/io.dart';
import 'package:laborhub/core/constants/settings.dart';
import 'package:laborhub/core/errors/exception.dart';
import 'package:laborhub/core/interceptors/auth_interceptor.dart';
import 'package:laborhub/core/services/locator_service.dart';

class BaseDatasource {
  Dio _dio;

  BaseDatasource() : _dio = Dio() {
    // Создаем экземпляр Dio

    // Можно настроить базовые настройки для всех запросов
    // Например, добавить базовый URL или заголовки

    (_dio.httpClientAdapter as IOHttpClientAdapter).onHttpClientCreate = (HttpClient client) {
      client.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
      return client;
    };
    _dio.options.baseUrl = Settings.backendUrl;
    _dio.options.headers = {
      'Content-Type': 'application/json',
    };
  }

  // Метод для выполнения GET-запросов
  Future<Response> get(String path,
      {Map<String, dynamic>? queryParameters, Options? options}) async {
    try {
      final response = await _dio.get(path, queryParameters: queryParameters, options: options);
      return response;
    } catch (error) {
      // Обработка ошибок
      throw LaborHubException(message: 'Ошибка при выполнении GET-запроса: $error');
    }
  }

  // Метод для выполнения POST-запросов
  Future<Response> post(String path, {dynamic body, Map<String, dynamic>? queryParameters}) async {
    try {
      final response = await _dio.post(path, data: body, queryParameters: queryParameters);
      return response;
    } catch (error) {
      // Обработка ошибок
      throw LaborHubException(message: 'Ошибка при выполнении POST-запроса: $error');
    }
  }

  // Метод для выполнения PUT-запросов
  Future<Response> put(String path, {dynamic body, Map<String, dynamic>? queryParameters}) async {
    try {
      final response = await _dio.put(path, data: body, queryParameters: queryParameters);
      return response;
    } catch (error) {
      // Обработка ошибок
      throw LaborHubException(message: 'Ошибка при выполнении PUT-запроса: $error');
    }
  }

  // Метод для выполнения DELETE-запросов
  Future<Response> delete(String path,
      {dynamic body, Map<String, dynamic>? queryParameters}) async {
    try {
      final response = await _dio.delete(path, data: body, queryParameters: queryParameters);
      return response;
    } catch (error) {
      // Обработка ошибок
      throw LaborHubException(message: 'Ошибка при выполнении DELETE-запроса: $error');
    }
  }

  // Setter для включения/выключения AuthInterceptor
  set enableAuthInterceptor(bool enableAuthInterceptor) {
    enableAuthInterceptor;

    // Если значение true, добавляем AuthInterceptor, в противном случае удаляем его
    if (enableAuthInterceptor) {
      _dio.interceptors.add(sl<AuthInterceptor>());
    } else {
      _dio.interceptors.removeWhere((interceptor) => interceptor is AuthInterceptor);
    }
  }
}
