// ignore_for_file: constant_identifier_names
part of 'constants.dart';

class IconPath {
  static const String map_pin = 'assets/icons/map_pin.png';
  static const String map_marker = 'assets/icons/map_marker.png';
}
