part of 'constants.dart';

class Constants {
  //maybe for future, don't used now
  static const int appBarHeight = 115;
  static const int footerHeight = 130;
  static const int drawerWidth = 223;
}

class BackgroundColors {
  static const light = Color(0xFFffffff);
  static const main = Color.fromRGBO(255, 183, 139, 1);
  static const primary = Color.fromRGBO(255, 96, 0, 1);
  static const secondary = Color(0xFF000000);
  static const error = Color(0xFFd50909);
  static const warning = Color(0xFFd57209);
  static const info = Color(0xFF11cb5f);
  static const defaultColor = Color(0xFFc4c4c4);
  static const dark = Color.fromRGBO(35, 35, 35, 1);
  static const border = Color(0xffDDDDEE);

  static const Color vkColor = Color.fromRGBO(66, 103, 178, 1);
  static const Color inactiveGray = Color.fromARGB(97, 0, 0, 0);
}

class ButtonColors {
  static const primary = Color(0xffff6000);
  static const secondary = Color(0xffff6000);
  static const error = Color(0xffd50909);
  static const warning = Color(0xffd57209);
  static const info = Color(0xff11cb5f);
}

class RolesColors {
  static const admin = Color(0xFFFF0100);
  static const operator = Color(0xFF913303);
  static const user = Color(0xFFFFC01E);
}

class StateColors {
  static const active = Color(0xFF729f72);
  static const inactive = Color(0xFFff0100);
}

class DeviceStatusColor {
  static const online = Color(0xFF21AD15);
  static const offline = Color(0xFFDDDDEE);
}

class OrderStatusColor {
  static const cancelled = Color(0xFFEA3C3B);
  static const approved = Color(0xFF21AD15);
  static const onModeration = Color(0xFFFFC01E);
  static const currentlyPlaying = Color(0xFF41479B);
  static const completed = Color(0xFF7D96D4);
  static const nullified = Color(0xFFAE2B2B);
  static const onEditing = Color(0xFF7D96D4);
  static const onPayWaiting = Color(0xFFFFC01E);
}

class PaymentStatusColor {
  static const nonPaid = Color(0xFF21AD15);
  static const paid = Color(0xFF21AD15);
  static const paymentReturned = Color(0xFF0A5504);
  static const withoutPayment = Color(0xFF21AD15);
}

class OrderStatusLine {
  static const processing = Color(0xFFFFC01E);
  static const completed = Color(0xFFff6000);
}
