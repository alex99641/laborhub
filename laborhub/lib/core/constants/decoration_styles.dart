part of 'constants.dart';

BoxShadow shadow = const BoxShadow(
  blurRadius: 8,
  color: Color.fromARGB(255, 202, 200, 200),
);
