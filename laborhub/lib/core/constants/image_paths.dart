// ignore_for_file: constant_identifier_names

part of 'constants.dart';

class ImagePath {
  /// it doesn't exist right now
  static const String logo = 'assets/images/laborhub_logo.png';
  static const String remont = 'assets/images/remont.jpeg';
  static const String avatar_placeholder = 'assets/images/avatar_placeholder.jpg';
  static const String no_image = 'assets/images/no_image.png';
}
