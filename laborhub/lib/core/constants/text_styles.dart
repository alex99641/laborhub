// ignore_for_file: constant_identifier_names

part of 'constants.dart';

class TextStyles {
  static const TextStyle w700_18px_black =
      TextStyle(fontSize: 22.0, fontWeight: FontWeight.w700, color: Colors.black); //18px Bold black
  static const TextStyle w700_16px_black =
      TextStyle(fontSize: 20, fontWeight: FontWeight.w700, color: Colors.black); //16px Bold black
  static const TextStyle w700_16px_black_underlined = TextStyle(
      fontSize: 20,
      fontWeight: FontWeight.w700,
      decoration: TextDecoration.underline,
      color: Colors.black); //16px Bold black underline
  static const TextStyle w700_14px_black =
      TextStyle(fontSize: 17, fontWeight: FontWeight.w700, color: Colors.black); //16px Bold black

  static const TextStyle w700_12px_black = TextStyle(
      fontSize: 14.7,
      color: Colors.black,
      fontWeight: FontWeight.w700,
      height: 1.4); //12px Bold background: black;
  static const TextStyle w700_8px_black = TextStyle(
      fontSize: 10,
      color: Colors.black,
      fontWeight: FontWeight.w700,
      height: 1.3); //8px W700 background: black;

  static const TextStyle w600_14px_black = TextStyle(
      fontSize: 17,
      color: Colors.black,
      letterSpacing: 0.45,
      fontWeight: FontWeight.w600); //14px W600 background: black
  static const TextStyle w600_12px_black = TextStyle(
      fontSize: 14.7,
      color: Colors.black,
      fontWeight: FontWeight.w600); //14px W600 background: black
  static const TextStyle w600_12px_black_with_letter_spacing = TextStyle(
      fontSize: 14.7,
      color: Colors.black,
      letterSpacing: 0.45,
      fontWeight: FontWeight.w600); //14px W600 background: black
  static const TextStyle w600_12px_black_underlined = TextStyle(
      fontSize: 14.7,
      color: Colors.black,
      letterSpacing: 0.45,
      decoration: TextDecoration.underline,
      fontWeight: FontWeight.w600); //14px W600 background: black underlined
  static const TextStyle w600_10px_black = TextStyle(
      fontSize: 12.5,
      color: Colors.black,
      fontWeight: FontWeight.w600); //10px W600 background: black
  static const TextStyle w500_10px_black = TextStyle(
      fontSize: 12.5,
      color: Colors.black,
      fontWeight: FontWeight.w500); //10px W500 background: black;
  static const TextStyle w400_12px_black = TextStyle(
      fontSize: 14.7,
      color: Colors.black,
      height: 1.3,
      fontWeight: FontWeight.w400); //12px W400 background: black;

  static const TextStyle w400_10px_black = TextStyle(
      fontSize: 12.5,
      color: Colors.black,
      fontWeight: FontWeight.w400); //10px W400 background: black;

}
