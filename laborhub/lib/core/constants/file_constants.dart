part of 'constants.dart';

class FileConstants {
  static const supportedContentTypes = [
    'mp4',
    'webp',
    'mpeg',
    'mp2',
    'm4p',
    'mpg',
    '3gp',
    'jpeg',
    'jpg',
    'png',
    'pdf',
    'txt',
    'docx'
  ];
}
