import 'package:flutter/material.dart';

part 'colors.dart';
part 'decoration_styles.dart';
part 'text_styles.dart';
part 'icon_paths.dart';
part 'image_paths.dart';
part 'file_constants.dart';
