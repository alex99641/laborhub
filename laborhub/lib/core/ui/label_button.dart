import 'package:flutter/material.dart';

class LabelButton extends StatelessWidget {
  final Color color;
  final String text;
  final Widget icon;
  final VoidCallback onTap;

  const LabelButton(
      {Key? key, required this.color, required this.text, required this.icon, required this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            text,
            style: TextStyle(color: color, fontSize: 17),
          ),
          icon,
        ],
      ),
    );
  }
}
