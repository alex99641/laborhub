import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

typedef InputDecorationFunction = InputDecoration Function(
    {bool enabled, String? labelText, Widget? suffixIconWidget});

typedef OnChangedCallback = void Function({String? value});

class LaborTextField extends StatelessWidget {
  LaborTextField({
    Key? key,
    this.inputDecorationFunction,
    required this.name,
    required this.validators,
    required this.labelText,
    this.formKey,
    this.enabled = true,
    this.readOnly = false,
    this.initialValue,
    this.suffixIconWidget,
    this.onChangedCallback,
    this.obscureText = false,
    this.maxLines = 1,
    this.minLines,
  }) : super(key: key);

  final bool enabled;
  final bool readOnly;
  final GlobalKey<FormBuilderState>? formKey;
  final String? initialValue;
  final InputDecorationFunction? inputDecorationFunction;
  final String name;
  final String labelText;
  final OnChangedCallback? onChangedCallback;
  final bool obscureText;
  final Widget? suffixIconWidget;
  final List<String? Function(String?)> validators;
  final int maxLines;
  final int? minLines;

  @override
  Widget build(BuildContext context) {
    return FormBuilderTextField(
      minLines: minLines,
      maxLines: maxLines,
      autovalidateMode: AutovalidateMode.disabled,
      decoration: inputDecorationFunction != null
          ? inputDecorationFunction!(
              enabled: enabled, labelText: labelText.tr(), suffixIconWidget: suffixIconWidget)
          : InputDecoration(
              enabled: enabled, labelText: labelText.tr(), suffixIcon: suffixIconWidget),
      enabled: enabled,
      readOnly: readOnly,
      initialValue: initialValue,
      name: name,
      obscureText: obscureText,
      style: const TextStyle(
        fontSize: 17,
      ),
      validator: FormBuilderValidators.compose(validators),
      onChanged: (value) {
        if (onChangedCallback != null) {
          onChangedCallback!(value: value);
        } else {
          formKey?.currentState?.fields[name]?.validate();
        }
      },
    );
  }
}
