import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:laborhub/core/constants/constants.dart';
import 'package:flutter/material.dart';
import 'package:laborhub/features/bussiness_logic/blocs/authentication/authentication_bloc.dart';

class LaborHubAppBar extends StatelessWidget implements PreferredSizeWidget {
  final String title;
  const LaborHubAppBar({
    Key? key,
    this.title = 'title',
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthBloc, AuthenticationState>(builder: (context, state) {
      String? avatarImage;
      if (state is AuthenticatedState && state.profile.pic.isNotEmpty) {
        avatarImage = state.profile.pic;
      }
      return AppBar(
        leading: IconButton(
            onPressed: () {
              Scaffold.of(context).openDrawer();
            },
            icon: const Icon(
              Icons.menu,
              size: 30,
            )),
        title: Text(title).tr(),
        actions: [
          PopupMenuButton(
            itemBuilder: (context) => [
              PopupMenuItem(
                child: Text('appBar.profile').tr(),
                onTap: () {},
              ),
              PopupMenuItem(
                child: Text('appBar.logout').tr(),
                onTap: () => context.read<AuthBloc>().add(AuthenticationLogoutEvent()),
              ),
            ],
            icon: avatarImage != null
                ? Image.network(avatarImage)
                : ClipOval(child: Image.asset(ImagePath.avatar_placeholder)),
          ),
        ],
      );
    });
  }

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}
