import 'dart:io';

import 'package:flutter/material.dart';
import 'package:laborhub/core/ui/toasts.dart';
import 'package:laborhub/features/data/models/advertisement_model.dart';
import 'package:laborhub/features/data/models/file_media_model.dart';
import 'package:laborhub/features/data/sources/file_datasource.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';

class FileListWidget<T extends FileMediaModel> extends StatelessWidget {
  const FileListWidget({
    super.key,
    required this.model,
  });

  final T model;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: Future.wait(model.getFiles().map(
          (fileName) async {
            Directory directory = await getApplicationDocumentsDirectory();
            return FileItemWidget(
              fileName: fileName,
              directory: directory,
            );
          },
        ).toList()),
        builder: (context, snapshot) {
          return Column(
              crossAxisAlignment: CrossAxisAlignment.start, children: snapshot.data ?? []);
        });
  }
}

class FileItemWidget extends StatefulWidget {
  const FileItemWidget({
    super.key,
    required this.fileName,
    required this.directory,
  });
  final String fileName;
  final Directory directory;

  @override
  State<FileItemWidget> createState() => _FileItemWidgetState();
}

class _FileItemWidgetState extends State<FileItemWidget> {
  var exists = false;
  @override
  Widget build(BuildContext context) {
    var file = File('${widget.directory.absolute.path}/${widget.fileName}');
    setState(() {
      exists = file.existsSync();
    });
    return InkWell(
      onTap: () async {
        requestStoragePermission().then((_) => requestManageExternalStoragePermission());
        if (exists) {
          openFile(file);
        } else {
          FileDatasource().loadFile(widget.fileName).then((_) {
            setState(() {
              exists = file.existsSync();
            });
            return openFile(file);
          });
        }
      },
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 4),
        child: Wrap(
          children: [
            Icon(Icons.attach_file, color: exists ? Colors.amber : Colors.grey),
            Text(widget.fileName),
          ],
        ),
      ),
    );
  }

  // Запрос разрешения на запись во внешнее хранилище
  Future<void> requestStoragePermission() async {
    PermissionStatus status = await Permission.storage.request();
    if (status.isGranted) {
      // Разрешение получено, теперь вы можете выполнить операцию записи в файл
      print('Разрешение STORAGE получено');
    } else {
      // Разрешение не получено. Обработайте этот случай соответственно.
      print('Разрешение STORAGE не получено');
      LaborHubToast.showMessage("Приложению требуется разрешение на работу с файлами");
      requestStoragePermission();
    }
  }

  Future<void> requestManageExternalStoragePermission() async {
    final status = await Permission.manageExternalStorage.request();
    if (status.isGranted) {
      // Разрешение получено
      print('Разрешение MANAGE_EXTERNAL_STORAGE получено');
    } else {
      // Разрешение не получено
      print('Разрешение MANAGE_EXTERNAL_STORAGE не получено');
      LaborHubToast.showMessage("Приложению требуется разрешение на работу с файлами");
      requestManageExternalStoragePermission();
    }
  }

  Future<void> openFile(File file) async {
    try {
      final result = await OpenFile.open(file.path);

      // final result = await OpenFile.open(path, type: 'folder'); // Открытие папки с помощью open_file
      print("type=${result.type}  message=${result.message}");
    } catch (e) {
      print('Ошибка при открытии папки: $e');
    }
  }
}
