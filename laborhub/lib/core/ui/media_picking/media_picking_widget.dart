// ignore_for_file: use_build_context_synchronously

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:laborhub/core/ui/media_picking/selected_assets_list_view.dart';
import 'package:laborhub/features/bussiness_logic/blocs/file_media_cubit.dart';
import 'package:wechat_assets_picker/wechat_assets_picker.dart';
import 'package:wechat_camera_picker/wechat_camera_picker.dart';

class MediaPickingWidget<T extends FileMediaCubit> extends StatefulWidget {
  const MediaPickingWidget({
    super.key,
  });

  @override
  State<MediaPickingWidget> createState() => _MediaPickingWidgetState<T>();
}

class _MediaPickingWidgetState<T extends FileMediaCubit> extends State<MediaPickingWidget> {
  final ValueNotifier<bool> isDisplayingDetail = ValueNotifier<bool>(true);

  List<AssetEntity> assets = <AssetEntity>[];

  Future<void> selectAssets(PickMethod model, BuildContext context) async {
    final List<AssetEntity>? result = await model.method(context, assets);
    if (result != null) {
      assets = result.toList();
      context.read<T>().setMediaAssets(media: assets);
      isDisplayingDetail.value = true;
      if (mounted) {
        setState(() {});
      }
    }
  }

  void removeAsset(int index, BuildContext context) {
    assets.removeAt(index);
    context.read<T>().setMediaAssets(media: assets);
    if (assets.isEmpty) {
      isDisplayingDetail.value = false;
    }
    setState(() {});
  }

  void onResult(List<AssetEntity>? result, BuildContext context) {
    if (result != null && result != assets) {
      assets = result.toList();
      context.read<T>().setMediaAssets(media: assets);
      if (mounted) {
        setState(() {});
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Builder(builder: (context) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          methodItemBuilder(context),
          if (assets.isNotEmpty)
            SelectedAssetsListView(
              assets: assets,
              isDisplayingDetail: isDisplayingDetail,
              onResult: onResult,
              onRemoveAsset: removeAsset,
            ),
        ],
      );
    });
  }

  Widget methodItemBuilder(BuildContext context) {
    final PickMethod model = PickMethod.common(maxAssetsCount: 10);
    return InkWell(
      onTap: () => selectAssets(model, context),
      onLongPress: model.onLongPress,
      child: Container(
        padding: const EdgeInsets.symmetric(
          horizontal: 20.0,
          vertical: 10.0,
        ),
        child: Row(
          children: <Widget>[
            Container(
              margin: const EdgeInsets.all(2.0),
              width: 48,
              height: 48,
              child: Center(
                child: Text(
                  model.icon,
                  style: const TextStyle(fontSize: 28.0),
                ),
              ),
            ),
            const SizedBox(width: 12.0),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    model.name,
                    style: const TextStyle(
                      fontSize: 18.0,
                      fontWeight: FontWeight.bold,
                    ),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                  const SizedBox(height: 5),
                  Text(
                    model.description,
                    style: Theme.of(context).textTheme.bodySmall,
                  ),
                ],
              ),
            ),
            const Icon(Icons.chevron_right, color: Colors.grey),
          ],
        ),
      ),
    );
  }
}

class PickMethod {
  final String icon;
  final String name;
  final String description;

  /// The core function that defines how to use the picker.
  final Future<List<AssetEntity>?> Function(
    BuildContext context,
    List<AssetEntity> selectedAssets,
  ) method;

  final GestureLongPressCallback? onLongPress;

  const PickMethod({
    required this.icon,
    required this.name,
    required this.description,
    required this.method,
    this.onLongPress,
  });

  factory PickMethod.common({required int maxAssetsCount}) {
    return PickMethod(
      icon: '📷',
      name: 'Choose media',
      description: 'Pick images.',
      method: (BuildContext context, List<AssetEntity> assets) {
        return AssetPicker.pickAssets(
          context,
          pickerConfig: AssetPickerConfig(
            maxAssets: maxAssetsCount,
            selectedAssets: assets,
            requestType: RequestType.image,
            specialItemPosition: SpecialItemPosition.prepend,
            specialItemBuilder: (
              BuildContext context,
              AssetPathEntity? path,
              int length,
            ) {
              if (path?.isAll != true) {
                return null;
              }
              return GestureDetector(
                behavior: HitTestBehavior.opaque,
                onTap: () async {
                  Feedback.forTap(context);
                  final AssetEntity? result = await _pickFromCamera(context);
                  if (result == null) {
                    return;
                  }
                  final AssetPicker<AssetEntity, AssetPathEntity> picker =
                      context.findAncestorWidgetOfExactType()!;
                  final DefaultAssetPickerBuilderDelegate builder =
                      picker.builder as DefaultAssetPickerBuilderDelegate;
                  final DefaultAssetPickerProvider p = builder.provider;
                  await p.switchPath(
                    PathWrapper<AssetPathEntity>(
                      path: await p.currentPath!.path.obtainForNewProperties(),
                    ),
                  );
                  p.selectAsset(result);
                },
                child: const Center(
                  child: Icon(Icons.camera_enhance, size: 42.0),
                ),
              );
            },
          ),
        );
      },
    );
  }
}

Future<AssetEntity?> _pickFromCamera(
  BuildContext c, {
  CameraPickerTextDelegate textDelegate = const EnglishCameraPickerTextDelegate(),
}) {
  return CameraPicker.pickFromCamera(
    c,
    pickerConfig: CameraPickerConfig(
      textDelegate: textDelegate,
    ),
  );
}
