import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:laborhub/core/constants/constants.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class ImageGallery extends StatefulWidget {
  ImageGallery({
    Key? key,
    required this.imagesList,
  }) : super(key: key);

  final List<String> imagesList;

  @override
  State<ImageGallery> createState() => _ImageGalleryState();
}

class _ImageGalleryState extends State<ImageGallery> {
  int _currentIndex = 0;
  final controller = PageController(keepPage: true);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.amber,
      padding: const EdgeInsets.only(top: 20),
      child: Stack(
        alignment: Alignment.bottomCenter,
        children: [
          widget.imagesList.isNotEmpty
              ? CarouselSlider.builder(
                  options: CarouselOptions(
                      viewportFraction: 1,
                      enableInfiniteScroll: false,
                      onPageChanged: (index, reason) {
                        setState(() {
                          _currentIndex = index;
                        });
                      }),
                  itemCount: widget.imagesList.length,
                  itemBuilder: (context, index, realIndex) {
                    return buildImage(widget.imagesList[index], index);
                  },
                )
              : Image.asset(ImagePath.no_image),
          if (widget.imagesList.isNotEmpty) buildIndicator(),
        ],
      ),
    );
  }

  Widget buildImage(imageUrl, int index) {
    return Container(
      width: MediaQuery.of(context).size.width * 0.94,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
      ),
      clipBehavior: Clip.hardEdge,
      child: Image.network(
        imageUrl,
        fit: BoxFit.fitWidth,
      ),
    );
  }

  buildIndicator() {
    return Container(
      padding: const EdgeInsets.only(bottom: 10),
      alignment: Alignment.bottomCenter,
      child: AnimatedSmoothIndicator(
        activeIndex: _currentIndex,
        count: widget.imagesList.length,
        effect: const ScrollingDotsEffect(
            dotWidth: 7, dotHeight: 7, activeDotScale: 1, activeDotColor: Colors.white),
      ),
    );
  }
}
