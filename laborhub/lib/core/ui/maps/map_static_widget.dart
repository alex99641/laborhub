import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:geolocator/geolocator.dart';
import 'package:laborhub/core/constants/constants.dart';
import 'package:laborhub/core/services/location_service.dart';
import 'package:laborhub/core/services/locator_service.dart';
import 'package:laborhub/features/bussiness_logic/blocs/map_updatable_cubit.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

class MapStaticWidget<T extends MapUpdatableCubit> extends StatefulWidget {
  const MapStaticWidget({
    Key? key,
    this.location,
  }) : super(key: key);
  final Point? location;

  @override
  // ignore: no_logic_in_create_state
  State<MapStaticWidget> createState() => _MapStaticWidgetState<T>(location: location);
}

class _MapStaticWidgetState<T extends MapUpdatableCubit> extends State<MapStaticWidget> {
  _MapStaticWidgetState({this.location});

  final LocationService locationService = sl();
  late YandexMapController controller;
  Point? location;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 8),
      height: 250,
      child: Material(
        elevation: 2,
        borderRadius: BorderRadius.circular(10),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(10),
          child: YandexMap(
            gestureRecognizers: <Factory<OneSequenceGestureRecognizer>>{
              Factory<OneSequenceGestureRecognizer>(
                () => EagerGestureRecognizer(),
              ),
            },
            onMapCreated: (YandexMapController yandexMapController) async {
              controller = yandexMapController;
              if (widget.location != null) {
                controller.moveCamera(CameraUpdate.newCameraPosition(CameraPosition(
                    target: Point(latitude: location!.latitude, longitude: location!.longitude))));
              } else {
                var currentLocation =
                    await locationService.getCurrentLocation().then((value) => value.toPoint());

                if (widget.location == null) {
                  setState(() {
                    location = currentLocation;
                    context.read<T>().setPoint(point: location!);
                  });
                }

                controller.moveCamera(CameraUpdate.newCameraPosition(CameraPosition(
                    target: Point(latitude: location!.latitude, longitude: location!.longitude))));
              }
            },
            rotateGesturesEnabled: false,
            onCameraPositionChanged: (cameraPosition, reason, finished) {
              if (widget.location == null) {
                setState(() {
                  location = cameraPosition.target;
                  context.read<T>().setPoint(point: location!);
                });
              }
            },
            mapObjects: [
              if (location != null) singleMapObject(location!),
            ],
          ),
        ),
      ),
    );
  }

  PlacemarkMapObject singleMapObject(Point location) {
    return PlacemarkMapObject(
        mapId: MapObjectId("id"),
        point: Point(latitude: location.latitude, longitude: location.longitude),
        onTap: (PlacemarkMapObject self, Point point) => print('Tapped me at $point'),
        opacity: 0.7,
        direction: 0,
        isDraggable: true,
        onDragStart: (_) => print('Drag start'),
        onDrag: (_, Point point) => print('Drag at point $point'),
        onDragEnd: (_) => print('Drag end'),
        icon: PlacemarkIcon.single(PlacemarkIconStyle(
            image: BitmapDescriptor.fromAssetImage(IconPath.map_marker),
            rotationType: RotationType.rotate)));
  }
}
