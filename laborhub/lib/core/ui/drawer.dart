import 'package:easy_localization/easy_localization.dart' as easy;
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/route_manager.dart';

import 'package:laborhub/core/constants/constants.dart';
import 'package:laborhub/features/bussiness_logic/blocs/authentication/authentication_bloc.dart';
import 'package:laborhub/features/presentation/create_advertisement_page.dart';
import 'package:laborhub/features/presentation/list_advertisement_page.dart';
import 'package:laborhub/features/presentation/list_proposal_page.dart';

class LaborHubDrawer extends StatelessWidget {
  const LaborHubDrawer({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      // backgroundColor: BackgroundColors.dark,
      child: ListView(
        children: [
          closeButtonTile(),
          // profileTile(),
          buildTile(
            'create-advert-page.page-title',
            callback: () {
              Navigator.pop(context);
              return Navigator.of(context).push(CreateAdvertisementPage.route());
            },
          ),
          buildTile(
            'list-advert-page.page-title',
            callback: () {
              Navigator.pop(context);
              Navigator.of(context).push(ListAdvertisementPage.route());
            },
          ),
          buildTile(
            'proposal.list-page-title',
            callback: () {
              Navigator.pop(context);
              Navigator.of(context).push(ListProposalPage.route());
            },
          ),
          buildTile(
            'appBar.logout'.tr(),
            callback: () => context.read<AuthBloc>().add(AuthenticationLogoutEvent()),
          ),
        ],
      ),
    );
  }

  // Builder profileTile() {
  //   return Builder(
  //     builder: (context) {
  //       final profile = context.select(
  //         (AuthenticationBloc bloc) => bloc.state.profile,
  //       );
  //       return ListTile(
  //         leading: Avatar(networkInfo: sl<NetworkInfo>()),
  //         title: Text(
  //           '${profile?.givenName} ${profile?.familyName}',
  //           style: const TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.w500),
  //         ),
  //         onTap: () => Navigator.of(context).push(ProfilePage.route()),
  //       );
  //     },
  //   );
  // }

  Widget closeButtonTile() {
    return Builder(builder: (context) {
      return ListTile(
        trailing: IconButton(
          icon: const Icon(Icons.close),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        // iconColor: BackgroundColors.main,
      );
    });
  }

  Widget buildTile(String tileText, {Function? callback}) {
    return Builder(builder: (context) {
      return ListTile(
        title: Text(
          tileText,
          // style: const TextStyle(color: BackgroundColors.main, fontSize: 18),
        ).tr(),
        onTap: callback != null ? () => callback() : () {},
      );
    });
  }
}
