import 'package:laborhub/core/constants/constants.dart';
import 'package:flutter/material.dart';

class StepBar extends StatelessWidget {
  const StepBar({
    Key? key,
    required this.countOfItems,
    required this.activeItemIndex,
  }) : super(key: key);
  final int countOfItems;
  final int activeItemIndex;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          GradientText(
            activeItemIndex.toString(),
            style: const TextStyle(
                color: BackgroundColors.primary,
                fontWeight: FontWeight.w700,
                fontSize: 50,
                fontFamily: 'Magistral'),
            gradient: LinearGradient(
              colors: [
                BackgroundColors.primary,
                BackgroundColors.primary.withOpacity(0.6),
                Colors.white.withAlpha(0),
              ],
              stops: [0.0, 0.5, 1],
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
            ),
          ),
          Container(
            clipBehavior: Clip.hardEdge,
            decoration: BoxDecoration(borderRadius: BorderRadius.circular(10)),
            child: Row(
              children: [
                for (var i = 0; i < countOfItems; i++)
                  Expanded(
                    child: Container(
                      color: i != activeItemIndex - 1
                          ? BackgroundColors.defaultColor
                          : BackgroundColors.primary,
                      height: 6,
                    ),
                  ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class GradientText extends StatelessWidget {
  const GradientText(
    this.text, {
    required this.gradient,
    this.style,
  });

  final String text;
  final TextStyle? style;
  final Gradient gradient;

  @override
  Widget build(BuildContext context) {
    return ShaderMask(
      blendMode: BlendMode.srcIn,
      shaderCallback: (bounds) => gradient.createShader(
        Rect.fromLTWH(0, 0, bounds.width, bounds.height),
      ),
      child: Text(text, style: style),
    );
  }
}
