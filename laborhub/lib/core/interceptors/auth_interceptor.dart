import 'package:dio/dio.dart';
import 'package:laborhub/features/data/sources/token_local_datasource.dart';

class AuthInterceptor extends QueuedInterceptor {
  final _tokensService = TokenLocalDatasource();

  AuthInterceptor();

  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) async {
    var accessToken = await _tokensService.getAccessToken();
    options.headers.addAll({'Authorization': 'Bearer $accessToken'});
    print('REQUEST[${options.method}] => PATH: ${options.path}');
    return super.onRequest(options, handler);
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    print('''RESPONSE[${response.statusCode}] 
        => PATH: ${response.requestOptions.path}''');
    return super.onResponse(response, handler);
  }

  @override
  void onError(DioError err, ErrorInterceptorHandler handler) {
    print('''ERROR[${err.response?.statusCode}] 
        => PATH: ${err.requestOptions.path}''');
    return super.onError(err, handler);
  }
}
